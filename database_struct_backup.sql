-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.38 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных bro_data
CREATE DATABASE IF NOT EXISTS `bro_data` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bro_data`;

-- Дамп структуры для таблица bro_data.atoms
CREATE TABLE IF NOT EXISTS `atoms` (
  `atomid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ongui_name` varchar(45) NOT NULL,
  `in_stock` int(31) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`atomid`),
  UNIQUE KEY `atomid_UNIQUE` (`atomid`)
) ENGINE=InnoDB AUTO_INCREMENT=1025 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bro_data.atoms: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `atoms` DISABLE KEYS */;
INSERT INTO `atoms` (`atomid`, `ongui_name`, `in_stock`) VALUES
	(1, 'Разовые посещения', -1),
	(2, 'Дневные посещения', -1),
	(3, 'Коктейли', -1),
	(4, 'Вода 0.5л', 9996),
	(5, 'Вода 1.0л', 9997),
	(6, 'Вода 1.5л', 9998),
	(9, 'Персональные тренировки', -1),
	(1024, '[abstract]', -1);
/*!40000 ALTER TABLE `atoms` ENABLE KEYS */;

-- Дамп структуры для таблица bro_data.barcode_aliases
CREATE TABLE IF NOT EXISTS `barcode_aliases` (
  `barcode` int(17) unsigned NOT NULL,
  `alias` int(17) unsigned NOT NULL,
  PRIMARY KEY (`alias`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bro_data.barcode_aliases: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `barcode_aliases` DISABLE KEYS */;
/*!40000 ALTER TABLE `barcode_aliases` ENABLE KEYS */;

-- Дамп структуры для таблица bro_data.goods
CREATE TABLE IF NOT EXISTS `goods` (
  `goodid` tinyint(10) unsigned NOT NULL,
  `atomid` int(10) unsigned NOT NULL,
  `atom_count` int(8) unsigned DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`goodid`),
  UNIQUE KEY `goodid_UNIQUE` (`goodid`),
  KEY `atomkey_idx` (`atomid`),
  CONSTRAINT `atomkey` FOREIGN KEY (`atomid`) REFERENCES `atoms` (`atomid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bro_data.goods: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` (`goodid`, `atomid`, `atom_count`, `name`, `description`, `price`) VALUES
	(1, 1, 1, 'Разовое безлимитное посещение', 'С 17:00 до 23:00', 200),
	(2, 2, 1, 'Дневное посещение', 'С 09:00 до 17:00', 150),
	(3, 1024, 1, 'Абонемент дневной на месяц (с 9:00 до 17:00)', 'Абонемент дневной на месяц (с 9:00 до 17:00)', 1500),
	(4, 1024, 1, 'Абонемент безлимитный на месяц (с 9:00 до 23:', 'Клиент может приходить в зал в любой день, любое количество раз с 9:00 до 23:00', 2000),
	(5, 9, 1, 'Одна персональная тренировка', 'Одно занятие с тренером', 400),
	(6, 9, 12, '12 персональных тренировок', '', 3500),
	(187, 4, 1, '0.5л Вода TRI VI', '', 25),
	(194, 5, 1, '1л Вода TRI VI', '', 40),
	(200, 6, 1, '1.5л Вода TRI VI', '', 50),
	(202, 3, 1, 'Коктейль', 'Охуенный коктель банан+сок+протеин', 100),
	(255, 1024, 1, '[Abstract]', 'Произвольное изменение баланса BRO GYM', 100);
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;

-- Дамп структуры для представление bro_data.profit_by_days
-- Создание временной таблицы для обработки ошибок зависимостей представлений
CREATE TABLE `profit_by_days` (
	`when` VARCHAR(10) NULL COLLATE 'utf8mb4_general_ci',
	`Суммарная прибыль` DECIMAL(39,0) NULL,
	`Количество покупок` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- Дамп структуры для таблица bro_data.purchases
CREATE TABLE IF NOT EXISTS `purchases` (
  `purchaseid` int(11) NOT NULL AUTO_INCREMENT,
  `customer` int(11) NOT NULL,
  `purchtype` tinyint(6) unsigned NOT NULL DEFAULT '0',
  `purchcount` int(16) unsigned NOT NULL DEFAULT '1',
  `comment` text,
  `cashchange` int(18) NOT NULL DEFAULT '0',
  `cashafter` int(20) NOT NULL DEFAULT '0',
  `when` datetime NOT NULL,
  PRIMARY KEY (`purchaseid`),
  KEY `customer_idx` (`customer`),
  KEY `purchid_idx` (`purchtype`),
  CONSTRAINT `customer` FOREIGN KEY (`customer`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `purchid` FOREIGN KEY (`purchtype`) REFERENCES `goods` (`goodid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;

-- Дамп структуры для таблица bro_data.tickets
CREATE TABLE IF NOT EXISTS `tickets` (
  `ticketid` int(11) NOT NULL AUTO_INCREMENT,
  `buyer` int(11) NOT NULL,
  `bought` datetime NOT NULL,
  `expires` datetime NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`ticketid`),
  KEY `userid_idx` (`buyer`),
  CONSTRAINT `buyer` FOREIGN KEY (`buyer`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bro_data.tickets: ~1 rows (приблизительно)
INSERT INTO `tickets` (`ticketid`, `buyer`, `bought`, `expires`, `type`) VALUES
	(1, 777, '2018-04-05 00:00:00', '2020-05-25 00:00:00', 0)

-- Дамп структуры для представление bro_data.today_profit
-- Создание временной таблицы для обработки ошибок зависимостей представлений
CREATE TABLE `today_profit` (
	`Время` TIME NULL,
	`customer` VARCHAR(91) NOT NULL COLLATE 'utf8_general_ci',
	`name` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`purchcount` INT(16) UNSIGNED NOT NULL,
	`cashchange` INT(18) NOT NULL
) ENGINE=MyISAM;

-- Дамп структуры для таблица bro_data.unused
CREATE TABLE IF NOT EXISTS `unused` (
  `unusedid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ownerid` int(10) NOT NULL,
  `atomid` int(10) unsigned NOT NULL,
  `unused_count` int(16) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`unusedid`),
  UNIQUE KEY `uniq` (`ownerid`,`atomid`),
  KEY `atomid` (`atomid`),
  CONSTRAINT `atomid` FOREIGN KEY (`atomid`) REFERENCES `atoms` (`atomid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ownerid` FOREIGN KEY (`ownerid`) REFERENCES `users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='fuck';

-- Дамп данных таблицы bro_data.unused: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `unused` DISABLE KEYS */;
INSERT INTO `unused` (`unusedid`, `ownerid`, `atomid`, `unused_count`) VALUES
	(4, 777, 9, 100)
/*!40000 ALTER TABLE `unused` ENABLE KEYS */;

-- Дамп структуры для таблица bro_data.users
CREATE TABLE IF NOT EXISTS `users` (
  `userid` int(24) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `registred` datetime NOT NULL,
  `birthday` date NOT NULL,
  `itisbro` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `comment` text,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bro_data.users: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`userid`, `first_name`, `last_name`, `surname`, `registred`, `birthday`, `itisbro`, `comment`) VALUES
	(0, 'Superior', 'Anonymous', 'Anonymousovich', '2011-10-08 12:24:53', '1908-10-08', 1, ''),
	(777, 'Александр', 'Быков', 'Юрьевич', '2018-04-05 20:11:20', '1994-12-18', 1, '')
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дамп структуры для таблица bro_data.visits
CREATE TABLE IF NOT EXISTS `visits` (
  `visitid` int(11) NOT NULL AUTO_INCREMENT,
  `visitor` int(11) NOT NULL,
  `visitdate` datetime NOT NULL,
  `visittype` int(8) unsigned NOT NULL DEFAULT '0',
  `visitcomment` text,
  PRIMARY KEY (`visitid`),
  KEY `userid_idx` (`visitor`),
  CONSTRAINT `visitor` FOREIGN KEY (`visitor`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;

-- Дамп структуры для представление bro_data.profit_by_days
-- Удаление временной таблицы и создание окончательной структуры представления
DROP TABLE IF EXISTS `profit_by_days`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `profit_by_days` AS select date_format(cast(`purchases`.`when` as date),'%d.%m.%Y') AS `when`,sum(`purchases`.`cashchange`) AS `Суммарная прибыль`,count(`purchases`.`purchaseid`) AS `Количество покупок` from `purchases` group by cast(`purchases`.`when` as date) order by cast(`purchases`.`when` as date) desc;

-- Дамп структуры для представление bro_data.today_profit
-- Удаление временной таблицы и создание окончательной структуры представления
DROP TABLE IF EXISTS `today_profit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `today_profit` AS select cast(`P`.`when` as time) AS `Время`,concat(`U`.`first_name`,' ',`U`.`last_name`) AS `customer`,`G`.`name` AS `name`,`P`.`purchcount` AS `purchcount`,`P`.`cashchange` AS `cashchange` from ((`purchases` `P` join `users` `U`) join `goods` `G`) where ((`P`.`customer` = `U`.`userid`) and (`G`.`goodid` = `P`.`purchtype`) and (date_format(`P`.`when`,'%d%m%Y') = date_format(now(),'%d%m%Y'))) order by `P`.`when` WITH CASCADED CHECK OPTION;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
