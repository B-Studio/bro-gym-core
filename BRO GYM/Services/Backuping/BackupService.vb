﻿Public Class BackupService
    Implements IConnectable, IConfigurable(Of Boolean)

    Public ReadOnly Property IsConnected As Boolean Implements IConnectable.IsConnected
        Get
            Return True
        End Get
    End Property

    Public Sub Connect() Implements IConnectable.Connect
    End Sub

    Public Sub Disconnect() Implements IConnectable.Disconnect
    End Sub

    Public Sub Configurate(config As Boolean) Implements IConfigurable(Of Boolean).Configurate
    End Sub
End Class
