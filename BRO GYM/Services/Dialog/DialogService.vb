﻿Imports BRO_GYM

Public Class DialogService : Implements IConnectable

    Protected ReadOnly Property IsConnected As Boolean Implements IConnectable.IsConnected
        Get
            Return True
        End Get
    End Property
    Protected Sub Connect() Implements IConnectable.Connect : End Sub
    Protected Sub Disconnect() Implements IConnectable.Disconnect : End Sub

    Public Function Show(Text$, Optional Caption$ = "",
                                 Optional Type As MessageBoxIcon = MessageBoxIcon.Information,
                                 Optional Buttons As MessageBoxButtons = MessageBoxButtons.OK) As DialogResult
        Dim taskbarTitle = Caption
        If String.IsNullOrEmpty(Caption) Then _
            taskbarTitle = "BRO GYM Core"
        Dim D As New DialogForm With {
                                .TopMost = True,
                                .TopLevel = True,
                                .Content = Text$,
                                .Caption = Caption$,
                                .Text = taskbarTitle,
                                .Type = Type,
                                .Buttons = Buttons
        }
        AddHandler D.Shown, Sub(s As Object, e As EventArgs)
                                SetForegroundWindow(D.Handle)
                                SetActiveWindow(D.Handle)
                            End Sub

        Show = D.ShowDialog()

        If Not D.IsDisposed Then D.Dispose()
        D = Nothing : GC.Collect()
    End Function





End Class
