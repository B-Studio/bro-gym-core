﻿Partial Class DialogService

    Public Class DialogForm : Inherits Form

        Public Property Buttons As MessageBoxButtons = MessageBoxButtons.OK
        Public Property Type As MessageBoxIcon = MessageBoxIcon.Information

        Public WriteOnly Property Caption As String
            Set(value As String)
                lblCaption.Text = value
                Me.Text = value
            End Set
        End Property
        Public Shadows WriteOnly Property Content As String
            Set(value As String)
                lblText.Text = value
            End Set
        End Property

        Private Sub OK_Button_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAccept.Click
            If btnAccept.Text = "ОК" Then _
                 Me.DialogResult = DialogResult.OK _
            Else Me.DialogResult = DialogResult.Yes
            Me.Close()
        End Sub

        Private Sub Cancel_Button_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
            If btnCancel.Text = "ОТМЕНА" Then _
                 Me.DialogResult = DialogResult.Cancel _
            Else Me.DialogResult = DialogResult.No
            Me.Close()
        End Sub

        Private Sub Dialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            DragByAControl.AttachMove(lblCaption)
            If Me.Buttons = MessageBoxButtons.OK Then
                btnCancel.Hide()
                TableLayoutPanel1.SetColumn(btnAccept, 1)
            End If
            If Me.Buttons.ToString().ToLower.Contains("ok") Then btnAccept.Text = "ОК"
            If Me.Buttons.ToString().ToLower.Contains("cancel") Then btnCancel.Text = "ОТМЕНА"
            Select Case Type
                Case MessageBoxIcon.Information
                    lblText.ForeColor = Color.FromArgb(128, 255, 128)
                    lblCaption.BackColor = Color.FromArgb(12, 32, 12)
                Case MessageBoxIcon.Error
                    lblText.ForeColor = Color.FromArgb(255, 128, 128)
                    lblCaption.BackColor = Color.FromArgb(32, 12, 12)
                Case MessageBoxIcon.Question, MessageBoxIcon.None
                    lblText.ForeColor = Color.FromArgb(160, 220, 255)
                    lblCaption.BackColor = Color.FromArgb(24, 32, 24)
                Case MessageBoxIcon.Exclamation
                    lblText.ForeColor = Color.FromArgb(255, 255, 128)
                    lblCaption.BackColor = Color.FromArgb(25, 25, 0)
            End Select
        End Sub

        Private Sub FormShown(sender As Object, e As EventArgs) Handles MyBase.Shown
            SetForegroundWindow(Me.Handle)
            SetActiveWindow(Me.Handle)
            Me.Activate() : btnAccept.Focus()
        End Sub

#Region "DESIGN CODE"
        Public Sub New()
            InitializeComponent()
        End Sub
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub
        Private components As System.ComponentModel.IContainer
        Private Sub InitializeComponent()
            Me.TableLayoutPanel1.SuspendLayout()
            Me.SuspendLayout()
            Me.TableLayoutPanel1.Anchor = AnchorStyles.Bottom
            Me.TableLayoutPanel1.ColumnCount = 2
            Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 50.0!))
            Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 50.0!))
            Me.TableLayoutPanel1.Controls.Add(Me.btnAccept, 0, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.btnCancel, 1, 0)
            Me.TableLayoutPanel1.Font = New Font("Modern H Medium", 14.0!)
            Me.TableLayoutPanel1.Location = New Point(52, 189)
            Me.TableLayoutPanel1.Margin = New Padding(6)
            Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
            Me.TableLayoutPanel1.RowCount = 1
            Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
            Me.TableLayoutPanel1.Size = New Size(479, 54)
            Me.TableLayoutPanel1.TabIndex = 0
            Me.btnAccept.Anchor = AnchorStyles.None
            Me.btnAccept.DialogResult = DialogResult.Yes
            Me.btnAccept.FlatAppearance.MouseDownBackColor = Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.btnAccept.FlatAppearance.MouseOverBackColor = Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.btnAccept.FlatStyle = FlatStyle.Flat
            Me.btnAccept.ForeColor = Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(210, Byte), Integer))
            Me.btnAccept.Location = New Point(33, 3)
            Me.btnAccept.Name = "btnAccept"
            Me.btnAccept.Size = New Size(172, 48)
            Me.btnAccept.TabIndex = 0
            Me.btnAccept.Text = "ДА"
            Me.btnAccept.UseVisualStyleBackColor = True
            Me.btnCancel.Anchor = AnchorStyles.None
            Me.btnCancel.DialogResult = DialogResult.No
            Me.btnCancel.FlatAppearance.MouseDownBackColor = Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.btnCancel.FlatAppearance.MouseOverBackColor = Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
            Me.btnCancel.FlatStyle = FlatStyle.Flat
            Me.btnCancel.ForeColor = Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer))
            Me.btnCancel.Location = New Point(273, 3)
            Me.btnCancel.Name = "btnCancel"
            Me.btnCancel.Size = New Size(172, 48)
            Me.btnCancel.TabIndex = 1
            Me.btnCancel.Text = "НЕТ"
            Me.btnCancel.UseVisualStyleBackColor = True
            Me.lblText.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) Or AnchorStyles.Left) Or AnchorStyles.Right), AnchorStyles)
            Me.lblText.ForeColor = Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(190, Byte), Integer))
            Me.lblText.Location = New Point(25, 66)
            Me.lblText.Name = "lblText"
            Me.lblText.Padding = New Padding(10, 0, 10, 0)
            Me.lblText.Size = New Size(534, 108)
            Me.lblText.TabIndex = 1
            Me.lblText.Text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" &
    "ncididunt ut labore et dolore magna aliqua."
            Me.lblText.TextAlign = ContentAlignment.MiddleLeft
            Me.lblCaption.BackColor = Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(36, Byte), Integer))
            Me.lblCaption.Dock = DockStyle.Top
            Me.lblCaption.Font = New Font("Modern H Medium", 16.0!)
            Me.lblCaption.ForeColor = Color.White
            Me.lblCaption.Location = New Point(0, 0)
            Me.lblCaption.Name = "lblCaption"
            Me.lblCaption.Padding = New Padding(20, 0, 0, 0)
            Me.lblCaption.Size = New Size(583, 44)
            Me.lblCaption.TabIndex = 2
            Me.lblCaption.Text = "Ебать его в рот"
            Me.lblCaption.TextAlign = ContentAlignment.MiddleLeft
            Me.AcceptButton = Me.btnAccept
            Me.AutoScaleDimensions = New SizeF(11.0!, 24.0!)
            Me.AutoScaleMode = AutoScaleMode.Font
            Me.BackColor = Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer))
            Me.CancelButton = Me.btnCancel
            Me.ClientSize = New Size(583, 265)
            Me.Controls.Add(Me.lblCaption)
            Me.Controls.Add(Me.lblText)
            Me.Controls.Add(Me.TableLayoutPanel1)
            Me.Icon = My.Resources.dialog
            Me.Font = New Font("Modern H EcoLight", 15.75!, FontStyle.Regular, GraphicsUnit.Point, CType(204, Byte))
            Me.ForeColor = Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.FormBorderStyle = FormBorderStyle.None
            Me.Margin = New Padding(6)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "Dialog"
            Me.ShowIcon = True
            Me.StartPosition = FormStartPosition.CenterParent
            Me.Text = "Dialog"
            Me.TopMost = True
            Me.TableLayoutPanel1.ResumeLayout(False)
            Me.ResumeLayout(False)
        End Sub
        Friend WithEvents TableLayoutPanel1 As New TableLayoutPanel
        Friend WithEvents btnAccept As New Button
        Friend WithEvents btnCancel As New Button
        Friend WithEvents lblText As New Label
        Friend WithEvents lblCaption As New Label
#End Region

    End Class

End Class