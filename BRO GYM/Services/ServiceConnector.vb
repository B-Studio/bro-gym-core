﻿Module ServiceConnector

    Private ReadOnly connected_services As New List(Of Object)

    Private Function CheckIfConnected(ByVal serviceType As Type) As Object
        Dim service = connected_services.FirstOrDefault(Function(s) s.GetType() Is serviceType)
        If service Is Nothing Then _
            Throw New InvalidOperationException($"Сервис {service.GetType().Name} ещё не присоединён к приложению")

        Return service
    End Function

    Public Sub Connect(ByVal serviceType As Type)
        Dim service = Activator.CreateInstance(serviceType)

        If Not TypeOf service Is IConnectable Then _
            Throw New ArgumentException($"Тип {serviceType.Name} не является типом сервиса")

        If connected_services.Contains(service) Then Return
        connected_services.Add(service)

        CType(service, IConnectable).Connect()
    End Sub
    Public Sub Connect(Of TService)()
        Connect(GetType(TService))
    End Sub

    Public Sub Disconnect(ByVal serviceType As Type)
        Dim service As IConnectable = CheckIfConnected(serviceType)
        service.Disconnect()
        connected_services.Remove(service)
    End Sub
    Public Sub Disconnect(Of TService)()
        Disconnect(GetType(TService))
    End Sub

    Public Sub Configurate(Of T)(ByVal serviceType As Type, ByVal config As T)
        Dim service As IConfigurable(Of T) = CheckIfConnected(serviceType)
        service.Configurate(config)
    End Sub

    Public Sub Configurate(Of T1, T2)(ByVal serviceType As Type, ByVal config1 As T1, ByVal config2 As T2)
        Dim service As IConfigurable(Of T1, T2) = CheckIfConnected(serviceType)
        service.Configurate(config1, config2)
    End Sub

    Public Function Service(Of T)() As T
        Return connected_services.FirstOrDefault(Function(S) S.GetType() Is GetType(T))
    End Function

End Module