﻿Class UpdateService
    Implements IConnectable, IConfigurable(Of String, KeyValuePair(Of String, String))

    Private Property serverAddress As String
    Private Property login As String
    Private Property pass As String
    Protected Property IsConnected As Boolean Implements IConnectable.IsConnected

    Protected Sub Connect() Implements IConnectable.Connect
        Service(Of DialogService).Show("Connected!")
        IsConnected = True
    End Sub
    Protected Sub Disconnect() Implements IConnectable.Disconnect
        Service(Of DialogService).Show("Disconnected!")
        IsConnected = False
    End Sub

    Protected Sub Configurate(serverLocation As String, credentials As KeyValuePair(Of String, String)) _
        Implements IConfigurable(Of String, KeyValuePair(Of String, String)).Configurate
        serverAddress = serverLocation
        login = credentials.Value
        pass = credentials.Key

        Service(Of DialogService).Show("Server: " & serverAddress & Chr(10) & Chr(13) & login & " | " & pass)
    End Sub

End Class
