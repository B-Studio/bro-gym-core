﻿Imports System.Threading
Imports Gma.UserActivityMonitor

Public Class BarcodeService
    Implements IConnectable, IConfigurable(Of Dictionary(Of BarcodeEvent, Action(Of String)))

    Public Enum BarcodeEvent As Byte
        BarcodeCatched
        BarcodeError
    End Enum

    Public Enabled As Boolean = False
    Public Sub Start()
        Enabled = True : End Sub
    Public Sub [Stop]()
        Enabled = False : End Sub

    Private connected As Boolean = False
    Private sp As String = ""

    Private code$ = String.Empty
    Private dec_list As New List(Of Keys) From {
        Keys.D1, Keys.D2, Keys.D3,
        Keys.D4, Keys.D5, Keys.D6,
        Keys.D7, Keys.D8, Keys.D9,
        Keys.D0
        }

    Private provider As GlobalEventProvider
    Private Event BarcodeCatched(ByVal barcode As String)
    Private Event BarcodeError(ByVal read As String)
    Protected Property IsConnected As Boolean Implements IConnectable.IsConnected

    Protected Sub Configurate(config As Dictionary(Of BarcodeEvent, Action(Of String))) _
        Implements IConfigurable(Of Dictionary(Of BarcodeEvent, Action(Of String))).Configurate

        If (config.ContainsKey(BarcodeEvent.BarcodeCatched)) Then _
            AddHandler Me.BarcodeCatched, New BarcodeCatchedEventHandler(Sub(b) config(BarcodeEvent.BarcodeCatched)(b))

        If (config.ContainsKey(BarcodeEvent.BarcodeError)) Then _
            AddHandler Me.BarcodeError, New BarcodeErrorEventHandler(Sub(b) config(BarcodeEvent.BarcodeError)(b))

    End Sub

    Protected Sub Connect() Implements IConnectable.Connect

        If connected Then Throw New InvalidOperationException("BarcodeService has already connected!")
        provider = New GlobalEventProvider()
        AddHandler provider.KeyUp, AddressOf GlobalKeyPress
        connected = True

    End Sub

    Protected Sub Disconnect() Implements IConnectable.Disconnect

        If Not connected Then Throw New InvalidOperationException("BarcodeService hasnt connected!")
        RemoveHandler provider.KeyUp, AddressOf GlobalKeyPress
        connected = False

    End Sub

    Private Sub GlobalKeyPress(sender As Object, e As KeyEventArgs)
        If Not Enabled Then Return

        If e.KeyCode = Keys.B Then
            sp = "b"
            code = ""
            Exit Sub
        End If
        Select Case True
            Case (sp = "") : If e.KeyCode = Keys.B Then : sp = "b" : Exit Sub : End If
            Case (sp = "b") : If e.KeyCode = Keys.R Then : sp = "br" : Exit Sub : End If
            Case (sp = "br") : If e.KeyCode = Keys.O Then : sp = "bro" : Exit Sub : End If
            Case Else
                If e.KeyCode = Keys.Enter Then
                    If IsNumeric(code) Then
                        AppThreads.StartNew(Sub() RaiseEvent BarcodeCatched(code))
                        Exit Sub
                    Else
                        AppThreads.StartNew(Sub() RaiseEvent BarcodeError(code))
                        Exit Sub
                    End If
                End If
                If code.Length < 6 Then code &= e.KeyCode.ToString().Last()
                Exit Sub
        End Select
        sp = String.Empty : code = String.Empty
    End Sub

End Class
