﻿Public Interface IRoutable

    ReadOnly Property Id As String
    Function IsRoutableBy(Id As String) As Boolean

End Interface
