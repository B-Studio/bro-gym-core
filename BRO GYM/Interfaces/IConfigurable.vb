﻿Public Interface IConfigurable(Of TConfig)
    Inherits IConnectable

    Sub Configurate(ByVal config As TConfig)

End Interface

Public Interface IConfigurable(Of TConfig1, TConfig2)
    Inherits IConnectable

    Sub Configurate(ByVal config1 As TConfig1, ByVal config2 As TConfig2)

End Interface
