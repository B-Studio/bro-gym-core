﻿Public Interface IConnectable

    Sub Connect()
    Sub Disconnect()
    ReadOnly Property IsConnected As Boolean

End Interface
