﻿Module WinAPI
    Public Declare Auto Function SetActiveWindow Lib "user32.dll" (ByVal hwnd As IntPtr) As IntPtr
    Public Declare Ansi Function SetForegroundWindow Lib "user32.dll" Alias "SetForegroundWindow" (ByVal hwnd As IntPtr) As Boolean
    Public Declare Auto Sub SwitchToThisWindow Lib "user32.dll" (hWnd As IntPtr, fAltTab As Boolean)
    Public Declare Ansi Function GetKeyboardLayout Lib "user32.dll" (ByVal dwLayout As Integer) As UInt16
End Module