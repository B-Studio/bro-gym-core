﻿Public Class UserX

    Public Const AnonymousID As UInt32 = 0
    Public Shared RegistredIDs As New List(Of UInt32)
    Public Shared Sub LoadRegistred()
        Dim uids As SQL.Response = SQL.Query(SQL.SelectAllUserIds)
        If uids.KindIs = SQL.Response.ResponseKind.Table Then
            RegistredIDs.Clear()
            For Each Row As Row In uids.Table
                RegistredIDs.Add(Row("userid"))
            Next
        End If
    End Sub
    Public Shared Function AliasedGet(barcode As UInt32) As List(Of UInt32)
        Dim l As New List(Of UInt32)
        Dim a As SQL.Response = SQL.Query("SELECT alias FROM barcode_aliases WHERE barcode={0}", barcode)
        If a.KindIs = SQL.Response.ResponseKind.Table Then
            For Each Row As Row In a.Table
                l.Add(CUInt(Row("alias")))
            Next
        End If
        Return l
    End Function

    Public Enum TicketType As SByte
        Undefined = -1
        Unlimited = 0
        DayTime = 1
        OneDayLight = 2
        OneDayUnlim = 3
    End Enum

    Private _firstname, _lastname, _surname As String, _id, _ticketid As UInt32
    Private _birthday, _regdate, _expires As Date, _itisbro As Boolean
    Private _ticket_type As TicketType = TicketType.Undefined
    Private _loadedHave As New Dictionary(Of Atom, SByte)

    Public Sub LoadUserInv()
        Dim unusedResp As SQL.Response = SQL.Query(SQL.UnusedAll, Me._id)
        If unusedResp.KindIs = SQL.Response.ResponseKind.Table Then
            Me._loadedHave.Clear()
            For Each R As Row In unusedResp.Table
                If (Goods.HaveId(R("goodid"))) Then
                    Me._loadedHave.Add(Goods.ById(R("goodid")).Atom, R("unused"))
                End If
            Next
        End If
    End Sub

    Public Property Unused(Prod As Product) As SByte
        Get
            If Me._loadedHave.ContainsKey(Prod.Atom) Then
                Return Me._loadedHave(Prod.Atom)
            Else : Return 0
            End If
        End Get
        Set(value As SByte)
            SQL.Query(SQL.UpdateUnused, value, Me._id, Prod.Atom.Id)
            If Not Me._loadedHave.ContainsKey(Prod.Atom) Then _
            Me._loadedHave.Add(Prod.Atom, value)
            Me._loadedHave(Prod.Atom) = value
        End Set
    End Property

    Private _comment As String
    Public ReadOnly Property Comment As String
        Get
            Return Me._comment
        End Get
    End Property

    Public ReadOnly Property Have As Dictionary(Of Atom, SByte)
        Get
            Return _loadedHave
        End Get
    End Property

    Public ReadOnly Property IsAnonymous As Boolean
        Get
            Return Me._id = 0
        End Get
    End Property

    Public ReadOnly Property ID As UInt32
        Get
            Return Me._id
        End Get
    End Property

    Public ReadOnly Property TickType As TicketType
        Get
            Return _ticket_type
        End Get
    End Property

    Public ReadOnly Property IDStr As String
        Get
            Return Me._id.ToString("000000")
        End Get
    End Property

    Public ReadOnly Property IsBro As Boolean
        Get
            Return Me._itisbro
        End Get
    End Property

    Public ReadOnly Property FirstLastName As String
        Get
            Return Me._lastname & " " & Me._firstname
        End Get
    End Property

    Public ReadOnly Property FirstName As String
        Get
            Return Me._firstname
        End Get
    End Property
    Public ReadOnly Property LastName As String
        Get
            Return Me._lastname
        End Get
    End Property

    Public ReadOnly Property Surname As String
        Get
            Return Me._surname
        End Get
    End Property

    Public ReadOnly Property Birthday As String
        Get
            Return Me._birthday.ToString("dd.MM.yyyy")
        End Get
    End Property
    Public ReadOnly Property ExpiresStr As String
        Get
            If Me._expires <> Nothing Then
                Return Me._expires.ToString("dd.MM.yy")
            Else : Return String.Empty
            End If
        End Get
    End Property
    Public ReadOnly Property ExpiresAfter As String
        Get
            If Me._expires <> Nothing Then
                Return UserX.ExpiresAfter_(Me._expires, Now)
            Else : Return String.Empty
            End If
        End Get
    End Property
    Public ReadOnly Property TicketIsOK As Boolean
        Get
            If Me._ticket_type = TicketType.Undefined Then Return False
            Return (Me._ticket_type = TicketType.DayTime And Now.Hour < 17) OrElse
               (Me._ticket_type <> TicketType.DayTime And Now < Me._expires)
        End Get
    End Property
    Public ReadOnly Property HaveOneDayTickets As Boolean
        Get
            If Me.Have.ContainsKey(Goods.Dict(1).Atom) AndAlso Me.Have(Goods.Dict(1).Atom) > 0 Then ' разовый безлим 
            ElseIf Me.Have.ContainsKey(Goods.Dict(2).Atom) AndAlso Me.Have(Goods.Dict(2).Atom) > 0 _
            AndAlso Now.Hour < 17 Then ' утренний разовый (до 17:00)
            Else : Return False ' нихуя блять нет
            End If
            Return True
        End Get
    End Property
    Public ReadOnly Property RegDate As String
        Get
            Return Me._regdate.ToString("dd.MM.yy")
        End Get
    End Property
    Public ReadOnly Property RegOld As String
        Get
            Return RegistredOld_(Me._regdate, Date.Now)
        End Get
    End Property

    Public ReadOnly Property YearsOld As String
        Get
            Dim yrs As Byte = 21
            yrs = Date.Now.Year - Me._birthday.Year - 1
            If Date.Now.Month >= Me._birthday.Month AndAlso Date.Now.Day >= Me._birthday.Day Then yrs += 1
            Return Accusative(yrs, Val.Years)
        End Get
    End Property

    Public Function UpdateTicketInfo() As TicketType
        Dim expiresQueryRes, tickType As SQL.Response
        expiresQueryRes = SQL.Query("SELECT ticketid, expires, type FROM tickets " &
                                "WHERE tickets.buyer=" & ID & " AND " &
                                "tickets.expires=(SELECT MAX(expires) FROM tickets WHERE buyer=" & ID & ") LIMIT 1")
        tickType = SQL.Query("SELECT type FROM tickets WHERE buyer={0} AND (NOW() BETWEEN bought AND expires)", ID)
        Me._ticketid = 0
        If expiresQueryRes.KindIs = SQL.Response.ResponseKind.Table Then
            Me._ticketid = expiresQueryRes.Table("ticketid")
            Me._expires = CDate(expiresQueryRes.Table("expires"))
            If tickType.KindIs = SQL.Response.ResponseKind.Table AndAlso tickType.Table.Height > 0 Then
                Me._ticket_type = CSByte(tickType.Table("type"))
            ElseIf Me._expires.Subtract(Now).TotalMilliseconds > 0 Then : Me._ticket_type = CSByte(expiresQueryRes.Table("type"))
            Else : Me._ticket_type = TicketType.Undefined
            End If
        End If
        Return Me._ticket_type
    End Function

    Public ReadOnly Property MaxTicketID As UInt32
        Get
            Return Me._ticketid
        End Get
    End Property

    Public Property Expires As Date
        Get
            Return Me._expires
        End Get
        Set(value As Date)
            SQL.Query("UPDATE tickets SET expires={0} WHERE buyer={1} AND ticketid={2}",
                  value.ToString("yyyy-MM-dd"), Me._id, Me._ticketid)
        End Set
    End Property

    Public ReadOnly Property TicketEndsAfter As String
        Get
            Return ExpiresAfter_(Me._expires, DateTime.Now)
        End Get
    End Property

    Public Shared Function LoadByID(ByVal id As UInt32) As UserX
        Dim ret As UserX = New UserX
        Dim UserResp As SQL.Response = SQL.Query(String.Format("SELECT * FROM users WHERE `userid`='{0}' LIMIT 1", id))
        If UserResp.KindIs = SQL.Response.ResponseKind.Table Then
            ret._id = id
            ret._firstname = UserResp.Table("first_name")
            ret._lastname = UserResp.Table("last_name")
            ret._surname = UserResp.Table("surname")
            ret._birthday = UserResp.Table("birthday")
            ret._regdate = UserResp.Table("registred")
            ret._comment = UserResp.Table("comment")
            ret._itisbro = CBool(UserResp.Table("itisbro"))
            ret.UpdateTicketInfo()
            Return ret
        ElseIf UserResp.KindIs = SQL.Response.ResponseKind.Excep Then
            Throw UserResp.Excep
        End If
        Return Nothing
    End Function

#Region "    Localization RUS"
    Public Shared Function ExpiresAfterRu(dateExp As DateTime) As String
        Return UserX.ExpiresAfter_(dateExp, Now).Replace("закончится ", "").Replace("(закончился)", "")
    End Function
    Private Shared Function RegistredOld_(dateRegistred As DateTime, dateNow As DateTime) As String
        Dim yrs As Short = dateNow.Year - dateRegistred.Year
        Dim mnt As Short = dateNow.Month - dateRegistred.Month
        If mnt < 0 Then mnt = 0
        If yrs < 0 Then yrs = 0
        Dim tspn As TimeSpan = dateNow.Subtract(dateRegistred)
        Select Case True
            Case yrs > 0 AndAlso tspn.TotalDays > 360 : Return Accusative(yrs, Val.Years) & " назад"
            Case tspn.TotalDays >= 27 : Return Accusative(tspn.TotalDays \ 30, Val.Months) & " назад"
            Case tspn.TotalDays / 7 >= 1 AndAlso tspn.TotalDays < 27
                Return Accusative(tspn.TotalDays \ 7, Val.Weeks) & " назад"
            Case tspn.TotalHours > 0
                If tspn.TotalHours < 24 Then Return "сегодня"
                If tspn.TotalHours < 48 Then Return "вчера"
                If tspn.TotalHours < 72 Then Return "позавчера"
                Return Accusative(tspn.TotalDays, Val.Days) & " назад"
            Case Else : Return "?"
        End Select
        Return ""
    End Function
    Private Shared Function ExpiresAfter_(dateExpires As DateTime, dateNow As DateTime) As String
        Dim yrs As Short = dateExpires.Year - dateNow.Year
        Dim mnt As Short = dateExpires.Month - dateNow.Month
        If mnt < 0 Then mnt = 0
        If yrs < 0 Then yrs = 0
        Dim tspn As TimeSpan = dateExpires.Subtract(dateNow)
        Select Case True
            Case yrs > 0 : Return "закончится через " & Accusative(yrs, Val.Years)
            Case mnt >= 0 AndAlso tspn.TotalDays >= 27 : Return "закончится через " & Accusative(mnt, Val.Months)
            Case tspn.TotalDays > 6 AndAlso tspn.TotalDays < 27
                Return "закончится через " & Accusative(tspn.TotalDays \ 7, Val.Weeks)
            Case tspn.TotalHours > 0
                If tspn.TotalHours < 12 Then Return "закончится сегодня"
                If tspn.TotalHours < 24 Then Return "закончится завтра"
                If tspn.TotalHours < 48 Then Return "закончится послезавтра"
                Return "закончится через " & Accusative(tspn.TotalDays, Val.Days)
            Case Else : Return "(закончился)"
        End Select
        Return ""
    End Function
    Private Enum Val As Byte
        Days
        Weeks
        Months
        Years
    End Enum
    Private Shared Function Accusative(count As Byte, type As Val) As String
        Dim dick As New Dictionary(Of Val, String()) From {
        {Val.Days, {"день", "дня", "дней"}},
        {Val.Weeks, {"неделю", "недели", "недель"}},
        {Val.Months, {"месяц", "месяца", "месяцев"}},
        {Val.Years, {"год", "года", "лет"}}}
        If count = 1 Then Return dick(type)(0)
        Select Case True
            Case (count >= 5 AndAlso count <= 20) : Return count & " " & dick.Values(type)(2) ' лет месяцев дней
            Case (count Mod 10 = 1) : Return count & " " & dick.Values(type)(0) ' год месяц день
            Case (count Mod 10 > 1 AndAlso count Mod 10 < 5) : Return count & " " & dick.Values(type)(1) ' года месяца дня
            Case Else : Return count & " " & dick.Values(type)(2) ' лет месяцев дней
        End Select
    End Function
#End Region

End Class