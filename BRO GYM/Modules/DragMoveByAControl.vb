﻿Public Class DragByAControl

    Public Shared Sub AttachMove(ToControl As Control)
        attach(ToControl)
        AddHandler ToControl.MouseMove, AddressOf MouseMove
    End Sub
    Public Shared Sub DeattachMove(FromControl As Control)
        deattach(FromControl)
        RemoveHandler FromControl.MouseMove, AddressOf MouseMove
    End Sub
    Public Shared Sub AttachResize(ToControl As Control)
        attach(ToControl)
        AddHandler ToControl.MouseMove, AddressOf MouseResize
    End Sub
    Public Shared Sub DeattachResize(FromControl As Control)
        deattach(FromControl)
        RemoveHandler FromControl.MouseMove, AddressOf MouseResize
    End Sub
    Private Shared Sub attach(toControl As Control)
        AddHandler toControl.MouseDown, AddressOf MouseDown
        AddHandler toControl.MouseUp, AddressOf MouseUp
    End Sub
    Private Shared Sub deattach(fromControl As Control)
        RemoveHandler fromControl.MouseDown, AddressOf MouseDown
        RemoveHandler fromControl.MouseUp, AddressOf MouseUp
    End Sub


    Private Shared Function GetParentForm(ThatHaveAControl As Control) As Form
        If ThatHaveAControl.Parent IsNot Nothing Then
            Return GetParentForm(ThatHaveAControl.Parent)
        ElseIf TypeOf ThatHaveAControl Is Form Then
            Return CType(ThatHaveAControl, Form)
        Else : Return Nothing
        End If
    End Function

    Private Shared F As Form
    Private Shared C As Cursor
    Private Shared px%, py%

    Private Shared Sub MouseDown(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            Dim C As Control = CType(sender, Control)
            DragByAControl.C = C.Cursor
            C.Cursor = Cursors.SizeAll
            F = GetParentForm(C)
            px = e.X : py = e.Y
        End If
    End Sub
    Private Shared Sub MouseMove(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left AndAlso F IsNot Nothing Then _
           F.Location = New Point(F.Location.X - px + e.X, F.Location.Y - py + e.Y)
    End Sub
    Private Shared Sub MouseResize(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left AndAlso F IsNot Nothing Then _
           F.Size = New Size(F.Size.Width - px + e.X, F.Size.Height - py + e.Y)
    End Sub
    Private Shared Sub MouseUp(sender As Object, e As MouseEventArgs)
        CType(sender, Control).Cursor = C
    End Sub

End Class