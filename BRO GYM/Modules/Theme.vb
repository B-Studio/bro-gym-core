﻿Public Module Theme
    Public CurArrow As New Cursor(My.Resources.aero_arrow.Handle)
    Public CurPointer As New Cursor(My.Resources.aero_link.Handle)

    Public Enum States As Byte
        Normal
        Allowed
        Restricted
        Unknown
        Aqua
    End Enum

    ' BACKGROUND COLORS
    Private BG(5) As Color
    Private BG_Over(5) As Color
    Private BG_Press(5) As Color

    ' FOREGROUND COLORS
    Private FG(5) As Color
    Private FG_Inactive(5) As Color

    Public Sub Load()
        If My.Settings.theme = 0 Then ' DARK THEME
            BG(States.Normal) = Color.Black
            BG(States.Allowed) = Color.Black
            BG(States.Restricted) = Color.Black
            BG(States.Unknown) = Color.Black
            BG(States.Aqua) = Color.FromArgb(0, 20, 15)

            BG_Press(States.Normal) = Color.FromArgb(64, 64, 64)
            BG_Press(States.Allowed) = Color.FromArgb(0, 64, 0)
            BG_Press(States.Restricted) = Color.FromArgb(64, 0, 0)
            BG_Press(States.Unknown) = Color.FromArgb(64, 64, 0)
            BG_Press(States.Aqua) = Color.FromArgb(0, 56, 42)

            BG_Over(States.Normal) = Color.FromArgb(51, 51, 51)
            BG_Over(States.Allowed) = Color.FromArgb(0, 51, 0)
            BG_Over(States.Restricted) = Color.FromArgb(51, 0, 0)
            BG_Over(States.Unknown) = Color.FromArgb(51, 51, 0)
            BG_Over(States.Aqua) = Color.FromArgb(0, 46, 34)

            FG(States.Normal) = Color.FromArgb(230, 230, 213)
            FG(States.Allowed) = Color.Lime
            FG(States.Restricted) = Color.Red
            FG(States.Unknown) = Color.Yellow
            FG(States.Aqua) = Color.FromArgb(0, 255, 192)

            FG_Inactive(States.Normal) = Color.FromArgb(158, 158, 147)
            FG_Inactive(States.Allowed) = Color.FromArgb(0, 96, 0)
            FG_Inactive(States.Restricted) = Color.FromArgb(96, 0, 0)
            FG_Inactive(States.Unknown) = Color.FromArgb(96, 96, 0)
            FG_Inactive(States.Aqua) = Color.FromArgb(0, 128, 96)
        End If
    End Sub

    Public Sub Apply(ByRef Fr As Form, St As States)
        Fr.BackColor = BG(St)
        Fr.ForeColor = FG(St)
        ApplyToControlDeep(Fr, St)
    End Sub

    Public Sub ApplyButton(ByRef Ctrl As Button, St As States)
        CType(Ctrl, Button).FlatAppearance.BorderColor = FG_Inactive(St)
        CType(Ctrl, Button).FlatAppearance.MouseOverBackColor = BG_Over(St)
        CType(Ctrl, Button).FlatAppearance.MouseDownBackColor = BG_Press(St)
    End Sub

    Private Sub ApplyToControlDeep(C As Control, St As States)
        If C.HasChildren Then
            For Each ChildC As Control In C.Controls
                ApplyToControlDeep(ChildC, St)
            Next
        Else
            C.ForeColor = FG(St)
            If TypeOf C Is Button AndAlso C.Name <> "clos" AndAlso C.Name <> "exec" AndAlso C.Name <> "mini" Then
                Theme.ApplyButton(CType(C, Button), St)
            End If
        End If
        'C.BackColor = BG(St)
    End Sub

    <DebuggerStepThrough()>
    Public Sub AttachOpacityFeatures(ByRef Fr As Form)
        AddHandler Fr.Deactivate, AddressOf Form_Deactivated
        AddHandler Fr.Activated, AddressOf Form_Activated
    End Sub
    <DebuggerStepThrough()>
    Private Sub Form_Deactivated(sender As Object, e As EventArgs)
        CType(sender, Form).Opacity = 0.8
    End Sub
    <DebuggerStepThrough()>
    Private Sub Form_Activated(sender As Object, e As EventArgs)
        CType(sender, Form).Opacity = 1
    End Sub

End Module