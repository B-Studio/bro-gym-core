﻿Imports MySql.Data.MySqlClient
Imports System.Security.Cryptography
Imports System.Text

Public Class SQL

    Private Shared FieldNamesDictionary As New Dictionary(Of String, String) From {
        {"userid", "ID"}, {"first_name", "Имя"}, {"last_name", "Фамилия"}, {"surname", "Отчество"},
        {"registred", "Зарегистрирован"}, {"birthday", "Дата рождения"}, {"itisbro", "Это BRO"}, {"photo", "Фото"}, {"comment", "Комментарий"},
        {"ticketid", "Абон.ID"}, {"bought", "Куплен"}, {"expires", "Заканчивается"}, {"type", "Тип"}, {"unused", "Остаток"}, {"when", "Дата"},
        {"goodid", "Штрихкод"}, {"atomid", "ID Еденицы"}, {"atom_count", "Количество"}, {"name", "Название"}, {"description", "Описание"},
        {"visitor", "Посетитель"}, {"visitid", "ID"}, {"visittype", "Тип"}, {"visitdate", "Дата"}, {"visitcomment", "Комментарий"},
        {"buyer", "Покупатель"}, {"customer", "Покупатель"}, {"cashchange", "Прибыль"}, {"cashafter", "Баланс"}, {"purchaseid", "ID Покупки"},
        {"purchtype", "Шх-код товара"}, {"purchcount", "Количество"}}
    Public Shared Function RusColName(SourceColName$)
        If SQL.FieldNamesDictionary.ContainsKey(SourceColName.ToLower()) Then
            Return SQL.FieldNamesDictionary(SourceColName.ToLower())
        Else : Return SourceColName
        End If
    End Function

    ''' <summary>
    ''' Класс, представляющий исчерпывающие данные об ответе сервера на SQL-запрос
    ''' </summary>
    Public NotInheritable Class Response


        ''' <summary>
        ''' Представляет возможные типы ответов MySQL сервера
        ''' </summary>
        Public Enum ResponseKind As Byte
            ''' <summary> В результате запроса возникла ошибка и она записана в свойство 'Excep' </summary>
            Excep
            ''' <summary> В результате запроса ничего не произошло и сервер ничего не вернул в ответ </summary>
            Empty
            ''' <summary> 
            ''' <para> В результате запроса были изменены одна или несколько записей (строк) </para>
            ''' <para> и их количество записано в свойство 'Changed' </para> 
            ''' </summary>
            Changed
            ''' <summary>
            ''' <para>В результате запроса сервер вернул таблицу,</para>
            ''' <para>имена столбцов которой записаны в свойство 'Names',</para>
            ''' <para>все полученные строки записаны в свойство 'Rows',</para>
            ''' <para>а их количество в 'RowsCount'</para> 
            ''' </summary>
            Table
        End Enum

        ''' <summary>
        ''' Возвращает тип ответа MySQL сервера на отправленный ему запрос
        ''' </summary>
        Public ReadOnly Property KindIs As ResponseKind
            Get
                Return _KindIs
            End Get
        End Property
        Private _KindIs As ResponseKind = ResponseKind.Empty

        ''' <summary>
        ''' Возвращает экземпляр исключения, которое возникло в результате выполнения запроса
        ''' </summary>
        Public ReadOnly Property Excep As Exception
            Get
                Return _Excep
            End Get
        End Property
        Private _Excep As Exception

        ''' <summary> 
        ''' Возвращает количество строк, которые были изменены в результате выполнения запроса
        ''' </summary>
        Public ReadOnly Property Changed As UInt32
            Get
                Return _Changed
            End Get
        End Property
        Private _Changed As UInt32 = 0

        ''' <summary>
        ''' Возвращает экземпляр класса Table, представляющий таблицу, которая была получена в результате выполнения запроса.  
        ''' При обращении к экземпляру Table по числовому индексу или по строковому ключу, 
        ''' возвращает значения поля первой строки, взяв ключ как имя столбца.
        ''' </summary>
        Public ReadOnly Property Table As Table
            Get
                Return _Table
            End Get
        End Property
        Private _Table As Table

        Public Sub New()
            Me._KindIs = ResponseKind.Empty
        End Sub
        Public Sub New(Excep As Exception)
            Me._KindIs = ResponseKind.Excep
            Me._Excep = Excep
        End Sub
        Public Sub New(AffectedRows As UInt32)
            Me._KindIs = ResponseKind.Changed
            Me._Changed = AffectedRows
        End Sub
        Public Sub New(Table As Table, Optional AffectedRows As UInt32 = 0)
            Me._KindIs = ResponseKind.Table
            Me._Changed = AffectedRows
            Me._Table = Table
        End Sub

    End Class

#Region "Private"
    Private Shared Sub initialize(ByVal key As String)
        Try
            SQL.DatabaseName = Crypt(dbs, key)
            SQL.User = Crypt(usr, key)
            SQL.Password = Crypt(pas, key)
        Catch : SQL.DatabaseName = Nothing
        End Try
    End Sub

    Private Shared Function Crypt(sourceStr$, key$) As String
        Dim keyArray As Byte() = UTF8Encoding.UTF8.GetBytes(key)
        Dim kByte As New List(Of Byte)
        kByte.Capacity = 24
        For x As Byte = 0 To (24 - 1)
            If (x >= keyArray.Length) Then Exit For
            kByte.Add(keyArray(x))
        Next
        While kByte.Count < 24 : kByte.Add(0) : End While
        Dim toEncrypt As Byte()
        toEncrypt = Convert.FromBase64String(sourceStr$)
        Dim tdes As New TripleDESCryptoServiceProvider() _
            With {.Mode = CipherMode.ECB, .Padding = PaddingMode.PKCS7}
        tdes.KeySize = 24 * 8
        tdes.Key = kByte.ToArray()
        Dim icry As ICryptoTransform = tdes.CreateDecryptor()
        Dim result As Byte() = icry.TransformFinalBlock(toEncrypt, 0, toEncrypt.Length)
        tdes.Clear()
        Return UTF8Encoding.UTF8.GetString(result)
    End Function
#End Region

    Public Shared Address$, User$, Password$, DatabaseName$

    Private Const _
        host$ = "127.0.0.1",
        usr$ = "gceeujCmvos=",
        pas$ = "7FLK9WBo28uV7M9D0LYqbw==",
        dbs$ = "m9qaITCoJ/i76P0SqLshPw=="


    Public Const SelectAllUserIds$ = _
        "SELECT userid FROM users"

    ''' <summary>
    ''' {0} CustomerId, {1} GoodId (PurchType)
    ''' </summary>
    Public Const Unused$ = _
        "SELECT unused_count AS `unused` FROM unused WHERE ownerid={0} AND atomid=(SELECT atomid FROM goods WHERE goodid={1}) LIMIT 1"

    Public Const UpdateUnused$ = _
        "INSERT INTO unused (unused_count, ownerid, atomid) VALUES({0}, {1}, {2}) ON DUPLICATE KEY UPDATE unused_count={0}"
    '"UPDATE unused SET unused.unused_count={0} WHERE ownerid={1} AND atomid={2}"

    ''' <summary>
    ''' {0} CustomerId _ Returns Key-Value table where: column[0] - goodid, column[1] - unused atoms
    ''' </summary>
    ''' <remarks></remarks>
    Public Const UnusedAll$ = _
        "SELECT goodid, unused.unused_count AS `unused` " & _
        "FROM unused, goods " & _
        "WHERE goods.atomid=unused.atomid AND goods.atom_count=1 AND unused.ownerid={0} AND unused.unused_count>0"

    ''' <summary>
    ''' {0} VisitorID, {1} VisitDate "yyyy-MM-dd", {2} visitType As User.TicketType
    ''' </summary>
    Public Const VisitAdd$ = _
        "INSERT INTO visits (visitor,visitdate,visittype,visitcomment) VALUES ({0},'{1}',{2},'{3}')"

    ''' <summary>SELECT * FROM {0}</summary>
    Public Const SelectAll$ = _
        "SELECT * FROM {0}"

    ''' <summary>{0} Comment, {1} UserId</summary>
    Public Const UpdateComment$ = _
        "UPDATE users SET comment='{0}' WHERE userid={1}"

    Public Structure Special
        Public Shared Function AddPurchase(CustomerId As UInt32, GoodId As UInt16, GoodCount As UInt16,
                                           Optional Comment$ = "") As SQL.Response
            Dim CashChange_%, Clast_%
            CashChange_ = Goods.ById(GoodId).Price * GoodCount
            Clast_ = SQL.Special.GetRaisedCashState(CashChange_)
            AddPurchase = SQL.Query("INSERT INTO purchases (customer,purchtype,purchcount,comment,cashchange,cashafter,`when`) " & _
                             "VALUES ({0},{1},{2},'{3}',{4},{5},NOW())", _
                             CustomerId, GoodId, GoodCount, Comment, CashChange_, Clast_)
            'If IsShown(Form1.F5) Then Form1.F5.RefreshState("purchases") TODO
        End Function
        Public Shared Function AddAbstract(SummOfAddition As Int32, Comment$) As SQL.Response
            Dim Clast_% = SQL.Special.GetRaisedCashState(SummOfAddition)
            AddAbstract = SQL.Query("INSERT INTO purchases (customer,purchtype,purchcount,comment,cashchange,cashafter,`when`) " &
                             "VALUES ({0},{1},{2},'{3}',{4},{5},NOW())",
                             BRO_CORE.User.AnonymousID, Barcodes.Abstract, 1, Comment, SummOfAddition, Clast_)
            'If IsShown(Form1.F5) Then Form1.F5.RefreshState("purchases") TODO
        End Function
        Public Shared Function GetRaisedCashState(cashChange As Int32) As Int32
            Return OneNumberQuery("SELECT (cashafter+" & cashChange & ") as `cashaf` FROM purchases WHERE purchaseid=(SELECT max(purchaseid) FROM purchases)")
        End Function
        Public Shared Function GetLastCashState() As Int32
            Return OneNumberQuery("SELECT cashafter FROM purchases WHERE purchaseid=(SELECT max(purchaseid) FROM purchases)")
        End Function
        Public Shared Function AddTicket(UserId As UInt32, StartsAt As Date, TicketType As UserX.TicketType) As SQL.Response
            Dim Starts, Ends As Date
            Starts = Date.Parse(StartsAt.ToString("dd.MM.yyyy") & " 00:00:00")
            Select Case TicketType
                Case UserX.TicketType.Unlimited
                    If Starts.Month = 2 Then Ends = Starts.AddDays(30) Else Ends = Starts.AddMonths(1)
                Case UserX.TicketType.DayTime
                    If Starts.Month = 2 Then Ends = Starts.AddDays(29).AddHours(17) Else Ends = Starts.AddMonths(1)
                Case UserX.TicketType.OneDayUnlim : Ends = Starts.AddDays(1)
                Case UserX.TicketType.OneDayLight : Ends = Starts.AddHours(17)
            End Select
            AddTicket = SQL.Query("INSERT INTO tickets (buyer,bought,expires,type) VALUES ({0},'{1}','{2}',{3})",
                             UserId, Starts.ToString("yyyy-MM-dd 00:00:00"), Ends.ToString("yyyy-MM-dd HH:mm:ss"), CByte(TicketType))
            'If IsShown(Form1.F5) Then Form1.F5.RefreshState("tickets")  TODO
        End Function
        Public Shared Function AddVisit(VisitorId As UInt32, VisitType As SByte, Optional VisitComment As String = "") As SQL.Response
            AddVisit = SQL.Query(SQL.VisitAdd, VisitorId, Now.ToString("yyyy-MM-dd HH:mm:ss"), VisitType, VisitComment)
            'If IsShown(Form1.F5) Then Form1.F5.RefreshState("visits") TODO
        End Function
        Public Shared Function TotalVisits() As UInt32
            Return OneNumberQuery("select count(*) as visit_day from visits where DATE(visitdate) = DATE(NOW())")
        End Function
        Public Shared Function TotalPaid() As UInt32
            Return OneNumberQuery("select count(*) as visit_paid from visits where DATE(visitdate) = DATE(NOW()) and visittype in (2,3,102,103)")
        End Function
        Public Shared Function TotalProfit() As UInt32
            Return OneNumberQuery("select sum(cashchange) As profit_day from purchases where DATE(`when`) = DATE(NOW())")
        End Function
        Private Shared Function OneNumberQuery(query$) As UInt32
            Dim r As SQL.Response = SQL.Query(query)
            If r.KindIs = Response.ResponseKind.Table AndAlso IsNumeric(r.Table.CellFirst) Then
                Dim ret As UInt32 = 0
                UInt32.TryParse(r.Table.CellFirst, ret)
                Return ret
            Else : Return 0
            End If
        End Function
        Public Shared Function HaveATodayVisit(UserID%) As Boolean
            Return OneNumberQuery("SELECT COUNT(*) AS visits FROM visits WHERE visitor=" & UserID & _
                                  " AND DATE(NOW())=DATE(visitdate)") > 0
        End Function
        Public Shared Function LastVisitDate(UserID%) As Date
            Dim sr As SQL.Response
            sr = SQL.Query("SELECT visitdate FROM visits WHERE visitor={0} " & _
                           "AND visitdate=(SELECT MAX(visitdate) FROM visits WHERE visitor={0})", UserID)
            If sr.KindIs = Response.ResponseKind.Table AndAlso sr.Table.HaveCells Then
                Return CDate(sr.Table.CellFirst)
            Else : Return Nothing
            End If
        End Function
        Private Const FirstCell As Byte = 0, SecondCell As Byte = 1
    End Structure

    Private Shared connection As New MySqlConnection

    ''' <summary>
    ''' <para>Принимает на вход ключ для расшифровки учётных данных,</para>
    ''' <para>а возвращает имя базы данных или Nothing, если не расшифровано</para>
    ''' </summary>
    Public Shared Property Decrypted As String
        Get
            If SQL.DatabaseName IsNot Nothing _
                AndAlso SQL.DatabaseName.Length > 0 Then _
                Return SQL.DatabaseName _
            Else Return Nothing
        End Get
        Set(value As String)
            initialize(value)
        End Set
    End Property

    ''' <summary> Выполняет соединение с базой данных. Возвращает 'True', если успешно соединено. </summary>
    Public Shared Function Connect() As Boolean
        Try
            If SQL.Decrypted Is Nothing Then Throw New Exception("Данные для подключения к MySQL-серверу не расшифрованы ключом 3des")

            If connection.State <> ConnectionState.Open Then
                Dim con_string As New MySqlConnectionStringBuilder
                con_string.Server = host
                con_string.UserID = SQL.User
                con_string.Password = SQL.Password
                con_string.Database = SQL.DatabaseName
                connection.ConnectionString = con_string.ConnectionString
                connection.Open()
            End If
            Return True
        Catch ex As Exception
            connection.Dispose()
            connection = New MySqlConnection
            Throw ex
            Return False
        End Try
    End Function

    ''' <summary> Разрывает соединение с базой данных, если оно было установлено. Возвращает 'True', если соединение разорвано. </summary>
    Public Shared Function Disconnect() As Boolean
        Try
            connection.Close()
            Return connection.State = ConnectionState.Closed
        Catch : Return False
        End Try
    End Function

    ''' <summary> Возвращает 'True', если соединение с базой установлено, в противном случае - 'False' </summary>
    Public Shared ReadOnly Property Connected As Boolean
        Get
            Return connection.State = ConnectionState.Open
        End Get
    End Property

    ''' <summary>
    ''' <para>Преобразует входную строку SQL запроса посредством String.Format, после чего</para>
    ''' <para>выполняет запрос к базе данных, с которой, на данный момент, установлено соединение,</para>
    ''' <para>и возвращает экземпляр Response, содержащий результат запроса</para>
    ''' </summary>
    ''' <param name="String">Содержимое запроса на языке SQL, содержащее заменяемые фрагменты {0}, {1} и т.д.</param>
    ''' <param name="Variables">Объекты, которые поочерёдно заменят фрагменты {0}, {1} и т.п. в строке SQL-запроса</param>
    Public Shared Function Query(String$, ParamArray Variables As String()) As SQL.Response
        For Each S As String In Variables : S = S.Replace("'", "\'") : Next
        Return SQL.Query(String.Format(String$, Variables))
    End Function

    ''' <summary>
    ''' <para>Выполняет запрос к базе данных, с которой, на данный момент, установлено соединение,</para>
    ''' <para>и возвращает экземпляр Response, содержащий результат запроса</para>
    ''' </summary>
    ''' <param name="String">Содержимое запроса на языке SQL</param>
    Public Shared Function Query(String$) As SQL.Response
        Try
            connection.Ping()
            If connection.State <> ConnectionState.Open Then Connect()

            Dim response As Response,
                command As MySqlCommand,
                reader As MySqlDataReader

            command = New MySqlCommand(String$, connection)
            reader = command.ExecuteReader()

            If Not reader.HasRows Then
                If reader.RecordsAffected > 0 Then
                    response = New Response(CUInt(reader.RecordsAffected))
                Else : response = New Response()
                End If
            Else

                Dim cols As New List(Of String)
                Dim tabl As Table
                Dim max_idx As UInt16 = reader.FieldCount - 1
                For i% = 0 To max_idx
                    cols.Add(reader.GetName(i)) : Next
                tabl = New Table(cols)

                Do While reader.Read()
                    Dim row As New Row()
                    Dim val As Object
                    For i% = 0 To max_idx
                        val = reader.GetValue(i)
                        If TypeOf val Is DBNull Then val = Nothing
                        row.Add(cols(i), val)
                    Next
                    tabl.AddRow(row)
                Loop
                Dim affected As UInt32 = 0
                If reader.RecordsAffected > 0 Then affected = reader.RecordsAffected
                response = New Response(tabl, affected)
            End If
            reader.Close()
            reader.Dispose()
            command.Dispose()
            Return response
        Catch Ex As Exception
            Call Disconnect()
            Return New Response(Excep:=Ex)
        End Try
    End Function

End Class

Module Regex

    ''' <summary>
    ''' Проверяет строку на предмет соответствия регулярному выражению
    ''' </summary>
    ''' <param name="StrToCheck">Строка для проверки</param>
    ''' <param name="RegexStr">Строка регулярного выражение</param>
    ''' <param name="CaseInsensetive">Если передано True, то при сопоставлении 
    ''' не учитывается регистр символов проверяемой строки</param>
    Public Function Check(StrToCheck$, RegexStr$, Optional CaseInsensetive As Boolean = True) As Boolean
        Try
            Dim rx As New System.Text.RegularExpressions.Regex(RegexStr, RegularExpressions.RegexOptions.IgnoreCase And CaseInsensetive)
            Check = rx.IsMatch(StrToCheck)
            rx = Nothing
            GC.Collect()
        Catch ex As Exception : Throw ex
        End Try
    End Function

End Module