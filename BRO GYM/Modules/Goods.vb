﻿Public Class Barcodes
    Public Const OneUnlimVisit% = 1
    Public Const OneDaylightVisit% = 2
    Public Const OnePersTrain% = 5
    Public Const Abstract% = 255
End Class

Public Class Goods
    Public Shared Dict As Dictionary(Of UInt16, Product)
    Public Shared LblNamesById As Dictionary(Of UInt16, String)

    Public Shared Function ById(Id As UInt16) As Product
        If Dict.ContainsKey(Id) Then Return Dict(Id) Else Return Nothing
    End Function

    Public Shared Function HaveId(CheckId As UInt16) As Boolean
        If Dict Is Nothing OrElse Dict.Count = 0 Then Load()
        Return Dict.ContainsKey(CheckId)
    End Function

    Public Shared Sub Load()
        Dict = New Dictionary(Of UInt16, Product)()
        LblNamesById = New Dictionary(Of UShort, String)()
        Dim gResp As SQL.Response = SQL.Query( _
                                        "SELECT goodid, atoms.atomid AS atomid, atom_count, name, description, price, ongui_name " & _
                                        "FROM goods, atoms " & _
                                        "WHERE atoms.atomid=goods.atomid")
        If gResp.KindIs = SQL.Response.ResponseKind.Table Then
            For Each R As Row In gResp.Table
                Dict.Add(R("goodid"), New Product(R("goodid"), R("atomid"), R("atom_count"), R("name"), R("description"), R("price")))
                LblNamesById.Add(R("goodid"), R("ongui_name"))
            Next
        End If
    End Sub

End Class


Public Class Product

    Private _atom As Atom
    Private _id, _atomcount As UInt16
    ''' <summary>
    ''' Возвращает число, соответствующее трём последним цифрам штрих-кода, который принадлежит товару
    ''' </summary>
    Public ReadOnly Property Id As UInt16
        Get
            Return Me._id
        End Get
    End Property

    ''' <summary>
    ''' Возвращает Atom, из которого (которых) состоит товар
    ''' </summary>
    Public ReadOnly Property Atom As Atom
        Get
            Return Me._atom
        End Get
    End Property

    ''' <summary>
    ''' Возвращает количество едениц (атомов), которое содержит товар
    ''' </summary>
    Public ReadOnly Property AtomCount As UInt16
        Get
            Return Me._atomcount
        End Get
    End Property

    ''' <summary>Возвращает или задаёт понятное (friendly) имя товара</summary>
    Public Property Name As String
    ''' <summary>Возвращает или задаёт текст описания товара</summary>
    Public Property Description As String
    ''' <summary>Возвращает или задаёт стоимость одной еденицы товара</summary>
    Public Property Price As UInt16

    ''' <summary>
    ''' Обновляет все данные о текущем товаре, выполняя SQL-запрос к БД
    ''' </summary>
    Public Sub Reload()
        Dim m As SQL.Response = SQL.Query("SELECT * FROM goods WHERE goodid={0}", _id)
        If m.KindIs = SQL.Response.ResponseKind.Table Then
            Me._atom = Atoms.ById(m.Table("atomid"))
            Me._atomcount = m.Table("atom_count")
            Me.Name = m.Table("name")
            Me.Description = m.Table("description")
            Me.Price = CUShort(m.Table("price"))
        End If
    End Sub

    Public Function GetUnusedByUser(UserId As UInt32) As SByte
        Dim UnusedResp As SQL.Response = SQL.Query(SQL.Unused, UserId, Me._id)
        If UnusedResp.KindIs = SQL.Response.ResponseKind.Table Then
            GetUnusedByUser = UnusedResp.Table("unused")
        Else : GetUnusedByUser = 0
        End If
        UnusedResp = Nothing
    End Function

    ''' <summary>
    ''' Создаёт экземпляр класса Product, загружающий информацию о нём с помощью SQL-запроса к БД
    ''' </summary>
    ''' <param name="Id">Число, соответствующее трём последним цифрам штрих-кода, который принадлежит товару</param>
    Public Sub New(Id As UInt16)
        Me._id = Id
        Me.Reload()
    End Sub

    ''' <summary>
    ''' Создаёт экземпляр класса Product
    ''' </summary>
    ''' <param name="Id">Число, соответствующее трём последним цифрам штрих-кода, который принадлежит товару</param>
    ''' <param name="AtomId">ID atom'а, из которого (которых) состоит товар</param>
    ''' <param name="AtomCount"></param>
    ''' <param name="Name">Понятное (friendly) имя товара</param>
    ''' <param name="Description">Текст описания товара</param>
    ''' <param name="Price">Стоимость одной еденицы товара</param>
    Public Sub New(Id As UInt16, AtomId As UInt16, AtomCount As UInt16, Name$, Description$, Price As UInt16)
        Me._id = Id
        Me._atom = Atoms.ById(AtomId)
        Me._atomcount = AtomCount
        Me.Name = Name
        Me.Description = Description
        Me.Price = Price
    End Sub

    Public Overrides Function ToString() As String
        Return String.Format("{0} ({1} r)", Me.Name, Me.Price)
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean
        Return CType(obj, Product).Id = Me.Id
    End Function

End Class


Public Class Atoms

    Public Shared Dict As New Dictionary(Of UInt16, Atom)

    Public Shared Sub Load()
        Dim M As SQL.Response = SQL.Query("SELECT * FROM atoms")
        If M.KindIs = SQL.Response.ResponseKind.Table Then
            Atoms.Dict.Clear()
            For Each R As Row In M.Table
                Dict.Add(R("atomid"), New Atom(R("atomid"), R("in_stock"), R("ongui_name")))
            Next
        End If
    End Sub

    Public Shared Function ById(AtomId As UInt16) As Atom
        If Atoms.Dict.ContainsKey(AtomId) Then Return Atoms.Dict(AtomId) Else Return Nothing
    End Function

End Class
Public Class Atom

    Private _id As UInt16
    Public ReadOnly Property Id As UInt16
        Get
            Return Me._id
        End Get
    End Property

    ''' <summary>Возвращает True, если должно учитываться количество товара 'на складе'</summary>
    Public ReadOnly Property IsStackable As Boolean
        Get
            Return Me._instockcached > -1
        End Get
    End Property

    ''' <summary>
    ''' Возвращает или задаёт количество товара на складе, пользуясь SQL-запросом к БД
    ''' </summary>
    Public Property InStock As Int16
        Get
            Dim m As SQL.Response
            m = SQL.Query("SELECT * FROM atoms WHERE atomid = {0}", Me.Id)
            If m.KindIs = SQL.Response.ResponseKind.Table Then
                If IsNumeric(m.Table("in_stock")) Then _instockcached = CShort(m.Table("in_stock")) Else _instockcached = -1
                Return _instockcached
            Else : Return Nothing
            End If
        End Get
        Set(value As Int16)
            If value < 0 Then value = 0
            Dim m As SQL.Response
            m = SQL.Query("UPDATE atoms SET in_stock={0} WHERE atomid = {1}", value, Me.Id)
            If m.KindIs = SQL.Response.ResponseKind.Changed AndAlso m.Changed = 1 Then
                Me._instockcached = value
            End If
        End Set
    End Property

    Private _instockcached As Int16
    ''' <summary>
    ''' Возвращает последнее полученное с сервера значение, определяющее количество товара на складе. 
    ''' Если оно ещё не было загружено, выполняет запрос и возвращает актуальное значение.
    ''' </summary>
    Public ReadOnly Property InStockCached As Int16
        Get
            If Me._instockcached <> Nothing Then Return _instockcached Else Return Me.InStock
        End Get
    End Property

    Private _onguiname As String
    ''' <summary>
    ''' Возвращает имя атома, которое должно использоваться для отображения его количества в интерфейсе пользователя
    ''' </summary>
    Public ReadOnly Property OnGuiName As String
        Get
            Return Me._onguiname
        End Get
    End Property

    Public Sub New(AtomId As UInt16)
        Dim M As SQL.Response = SQL.Query("SELECT * FROM atoms WHERE atomid={0}", AtomId)
        If M.KindIs = SQL.Response.ResponseKind.Table AndAlso M.Table.Height = 1 Then
            Me._id = M.Table("atomid")
            Me._instockcached = M.Table("in_stock")
            Me._onguiname = M.Table("ongui_name")
        End If
    End Sub

    Public Sub New(AtomId As UInt16, ActualInStock As Int16, OnGuiName$)
        Me._id = AtomId
        Me._instockcached = ActualInStock
        Me._onguiname = OnGuiName
    End Sub

    Public Overrides Function ToString() As String
        Return String.Format("{0} #{1}", Me.OnGuiName, Me.Id.ToString("000"))
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean
        Return CType(obj, Atom).Id = Me.Id
    End Function

End Class