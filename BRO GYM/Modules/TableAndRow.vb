﻿''' <summary>Класс, представляющий таблицу, 
''' к полям строк которой можно обратится по именам столбцов или по индексам.</summary>
Public NotInheritable Class Table
    Implements IEnumerable(Of Row)
    ''' <summary>Возвращает одну ячейку строки</summary>
    ''' <param name="RowIndex">Индекс (начиная от нуля) строки, ячейку из которой необходимо получить</param>
    ''' <param name="ColIndex">Индекс (начиная от нуля) столбца, ячейку из которого необходимо получить</param>
    Default Public ReadOnly Property Item(ByVal RowIndex As ULong, ColIndex As Byte) As Object
        Get
            Return rows(RowIndex)(ColIndex)
        End Get
    End Property
    ''' <summary>Возвращает одну ячейку строки</summary>
    ''' <param name="RowIndex">Индекс (начиная от нуля) строки, ячейку из которой необходимо получить</param>
    ''' <param name="ColName">Имя столбца, ячейку из которого необходимо получить</param>
    Default Public ReadOnly Property Item(ByVal RowIndex As ULong, ColName As String) As Object
        Get
            Return rows(RowIndex)(ColName)
        End Get
    End Property

    ''' <summary>Возвращает одну ячейку первой [index = 0] строки</summary>
    ''' <param name="ColName">Имя столбца, ячейку из которого необходимо получить</param>
    Default Public ReadOnly Property Item(ByVal ColName As String) As Object
        Get
            Return rows(0)(ColName)
        End Get
    End Property
    ''' <summary>Возвращает одну ячейку первой [index = 0] строки</summary>
    ''' <param name="ColIndex">Имя столбца, ячейку из которого необходимо получить</param>
    Default Public ReadOnly Property Item(ByVal ColIndex As Byte) As Object
        Get
            Return rows(0)(ColIndex)
        End Get
    End Property

    ''' <summary>Возвращает ячейку таблицы из первой строки, находящейся в первом столбце</summary>
    Public ReadOnly Property CellFirst As Object
        Get
            Return rows(0)(0)
        End Get
    End Property
    ''' <summary>Возвращает ячейку таблицы из первой строки, находящейся во втором столбце</summary>
    Public ReadOnly Property CellSecond As Object
        Get
            Return rows(0)(1)
        End Get
    End Property
    ''' <summary>Возвращает ячейку таблицы из первой строки, находящейся во третьем столбце</summary>
    Public ReadOnly Property CellThird As Object
        Get
            Return rows(0)(2)
        End Get
    End Property

    ''' <summary>
    ''' Возвращает True, если таблица содержит хотя бы одну ячейку, в противном случае - False
    ''' </summary>
    Public ReadOnly Property HaveCells As Boolean
        Get
            Return rows.Count > 0 AndAlso Columns.Count > 0
        End Get
    End Property

    ''' <summary>Добавляет строку в конец таблицы</summary>
    ''' <param name="Row">Экземпляр класса Row, представляющий добавляемую строку</param>
    Public Sub AddRow(Row As Row)
        rows.Add(Row)
    End Sub

    ''' <summary>Удаляет строку из таблицы по указаному индексу (начиная с нуля)</summary>
    ''' <param name="Index">Индекс (от нуля) удаляемой строки</param>
    Public Sub RemoveRowAt(Index As ULong)
        rows.RemoveAt(Index)
    End Sub

    ''' <summary>Возвращает количество ячеек, которые содержит каждая строка таблицы</summary>
    Public ReadOnly Property Width As Byte
        Get
            Return colNames.Count
        End Get
    End Property

    ''' <summary>Возвращает количество строк, которые содержит таблица</summary>
    Public ReadOnly Property Height As ULong
        Get
            Return rows.Count
        End Get
    End Property

    ''' <summary>Возвращает List с именами столбцов, которые содержит таблица</summary>
    Public ReadOnly Property Columns As List(Of String)
        Get
            Return colNames
        End Get
    End Property

    Private rows As List(Of Row)
    Private colNames As List(Of String)

    ''' <summary>Создаёт новый экзепляр класса, представляющий таблицу, 
    ''' к полям которой можно обратится по именам столбцов или по индексам</summary>
    ''' <param name="ColumnNames">List с именами столбцов, которые представлены в таблице</param>
    Public Sub New(ColumnNames As List(Of String))
        colNames = ColumnNames
        rows = New List(Of Row)
    End Sub

#Region "Implementations"
    Public Function GetEnumerator() As IEnumerator(Of Row) Implements IEnumerable(Of Row).GetEnumerator
        Return Me.rows.AsEnumerable().GetEnumerator()
    End Function
    Private Function GetEnumerator1() As IEnumerator Implements IEnumerable.GetEnumerator
        Return GetEnumerator()
    End Function
#End Region
End Class

''' <summary>Слой абстракции для строки таблицы, который физически представляет Dictionary(Of String, Object)</summary>
Public NotInheritable Class Row
    Inherits Dictionary(Of String, Object)

    ''' <summary>Возвращает одну ячейку строки по индексу столбца</summary>
    ''' <param name="ColIndex">Индекс столбца, ячейку из которого необходимо получить</param>
    Default Public Overloads ReadOnly Property Item(ByVal ColIndex As Integer) As Object
        Get
            Dim ret = Me.Values(ColIndex)
            Try : If ret Is DBNull.Value Then ret = Nothing
            Catch
            End Try
            Return ret
        End Get
    End Property

    ''' <summary>Возвращает одну ячейку строки по строковому имени столбца</summary>
    ''' <param name="ColName">Имя столбца, ячейку из которого необходимо получить</param>
    Default Public Overloads ReadOnly Property Item(ByVal ColName As String) As Object
        Get
            Dim ret As Object = Nothing
            If Not Me.TryGetValue(ColName, ret) Then Return Nothing
            Try : If ret Is DBNull.Value Then ret = Nothing
            Catch
            End Try
            Return ret
        End Get
    End Property

End Class