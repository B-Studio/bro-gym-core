﻿Partial Public Class AppServices

    Public Shared Sub Connect_DialogService()
        ServiceConnector.Connect(Of DialogService)()
    End Sub

    Public Shared Sub Connect_UpdateService()
        Dim updateServer$ = "http://core.bro-gym.ru/update"
        Dim updateCredentials As New KeyValuePair(Of String, String)("loginTest", "passwordX12X34")

        ServiceConnector.Connect(Of UpdateService)()
        ServiceConnector.Configurate(GetType(UpdateService), updateServer, updateCredentials)
    End Sub

    Public Shared Sub Connect_BackupService()
        ServiceConnector.Connect(Of BackupService)()
    End Sub

    Public Shared Sub Connect_BarcodeService()
        Dim barcodeHandlers As New Dictionary(Of BarcodeService.BarcodeEvent, Action(Of String))

        barcodeHandlers.Add(BarcodeService.BarcodeEvent.BarcodeCatched, AddressOf AppRouting.ByBarcode)
        barcodeHandlers.Add(BarcodeService.BarcodeEvent.BarcodeError,
                                Sub(code)
                                    Service(Of DialogService).Show(
                                    "Некорректный BRO-код!" & Chr(10) & Chr(13) & "Прочитано: " & code,
                                    "Что ты наделал...",
                                    MessageBoxIcon.Error)
                                End Sub)

        ServiceConnector.Connect(GetType(BarcodeService))
        ServiceConnector.Configurate(GetType(BarcodeService), barcodeHandlers)
    End Sub

    Public Shared Sub Disconnect_All()
        ServiceConnector.Disconnect(Of BarcodeService)()
        ServiceConnector.Disconnect(Of BackupService)()
        ServiceConnector.Disconnect(Of UpdateService)()
        ServiceConnector.Disconnect(Of DialogService)()
    End Sub

End Class