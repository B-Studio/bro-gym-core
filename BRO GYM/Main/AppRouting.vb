﻿Imports System.Runtime.CompilerServices

Module AppRouting

    Private FormsCached As New Dictionary(Of Type, List(Of Form))

    <Extension()>
    Private Sub SetFocus(ByVal Form As Form)
        SetActiveWindow(Form.Handle)
        Form.Activate()
    End Sub

    Private Function IsShown(Win As Form) As Boolean
        If Win Is Nothing Then Return False
        If Win.IsDisposed Then Return False
        If Not Win.Visible Then Return False
        Return True
    End Function

    Private Function NeedRecreate(Win As Form) As Boolean
        Return Win Is Nothing OrElse Win.IsDisposed
    End Function

    Public Function Form(Of TForm)() As Form
        Return Form(GetType(TForm))
    End Function
    Public Function Form(ByVal FormType As Type) As Form
        Dim f = FormsCached.FirstOrDefault(Function(x) x.Key Is FormType)
        If f.Key Is Nothing Then Return Nothing
        Return f.Value.FirstOrDefault
    End Function

    Public Function Forms(Of TForm)() As Form()
        Return Forms(GetType(TForm))
    End Function
    Public Function Forms(ByVal FormType As Type) As Form()
        Dim f = FormsCached.FirstOrDefault(Function(x) x.Key Is FormType)
        If f.Key Is Nothing Then Return Nothing
        Return f.Value.ToArray()
    End Function

    Public Function ActWithForm(ByVal Form As Type, ByVal Action As Action(Of Form)) As Form
        Dim f = AppRouting.Form(Form)
        If f IsNot Nothing Then f.Invoke(Sub() Action(f))
        Return f
    End Function

    Public Sub Show(Proto As Type, Optional ItsSingle As Boolean = True)
        If Not GetType(Form).IsAssignableFrom(Proto) Then _
            Throw New ArgumentException($"Type '{Proto.Name}' is not assignable to System.Windows.Forms.Form type!")

        Dim f As Form = Form(Proto)
        If f Is Nothing OrElse Not ItsSingle Then
            If Not FormsCached.ContainsKey(Proto) Then _
                   FormsCached.Add(Proto, New List(Of Form))
            f = Activator.CreateInstance(Proto)
            FormsCached(Proto).Add(f)
        End If

        If Not IsShown(f) Then
            f.Show()
            f.SetFocus()
        End If
    End Sub

    ''' <summary>
    ''' Закрывает формы определённого типа по заголовку окна 
    ''' или закрывает все окна переданного типа, если заголовок не указан
    ''' </summary>
    ''' <param name="Proto">Тип формы, которую требуется закрыть</param>
    ''' <param name="Title">Заголовок формы</param>
    Public Sub Close(Proto As Type, Optional Title As String = Nothing)
        Dim closable = Forms(Proto)
        If Title IsNot Nothing Then closable = closable.Where(Function(f) f.Name Is Title)
        For Each f As Form In closable
            f.Close()
            f.Dispose()
            FormsCached(Proto).Remove(f)
            f = Nothing
        Next
        Call GC.Collect()
    End Sub

    Public Sub ShutdownApplication()
        Application.Exit()
    End Sub

    Public Sub RestartApplication()
        Application.Restart()
    End Sub

    Public Sub ByBarcode(Barcode As String)
        Dim dialog As DialogService = Service(Of DialogService)()

        Select Case Barcode
            Case "999990"
                If dialog.Show("Вы уверены, что хотите выйти из программы BRO GYM Core ?", "Уверен ???",
                               MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo) = DialogResult.Yes Then _
                    AppRouting.ShutdownApplication()
            Case "999991"
                If dialog.Show("Вы уверены, что хотите перезапустить BRO GYM Core ?", "Уверен ???",
                               MessageBoxIcon.Information, MessageBoxButtons.YesNo) = DialogResult.Yes Then _
                    AppRouting.RestartApplication()

        End Select
    End Sub

End Module
