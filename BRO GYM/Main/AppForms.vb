﻿Public Class AppForms

    Public Shared abstract_cashchange As Type = GetType(abstract_cashchange)
    Public Shared add_anon_visit As Type = GetType(add_anon_visit)
    Public Shared add_instock As Type = GetType(add_instock)
    Public Shared add_new As Type = GetType(add_new)
    Public Shared add_visit As Type = GetType(add_visit)
    Public Shared aliased_select As Type = GetType(aliased_select)
    Public Shared cart As Type = GetType(cart)
    Public Shared custom_queries As Type = GetType(custom_queries)
    Public Shared expires_change As Type = GetType(expires_change)
    Public Shared authorize_form As Type = GetType(authorize_form)
    Public Shared statistics As Type = GetType(statistics)
    Public Shared user_load As Type = GetType(user_load)

End Class
