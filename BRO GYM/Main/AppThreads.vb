﻿Imports System.Threading
Public Class AppThreads

    Public Shared ReadOnly Property Current As Thread
        Get
            Return Thread.CurrentThread
        End Get
    End Property

    Public Shared Function StartNew(action As Action) As Thread
        Dim t As New Thread(Sub() action()) With {.IsBackground = True}
        t.Start()
        Return t
    End Function

    Public Shared Function StartNewAfter(delayMs As Integer, action As Action) As Thread
        Return StartNew(Sub()
                            Thread.Sleep(delayMs)
                            action()
                        End Sub)
    End Function

    Public Shared Sub WaitAndDo(delayMs As Integer, action As Action)
        Dim t As New System.Windows.Forms.Timer With {.Interval = delayMs}
        AddHandler t.Tick, Sub()
                               action()
                               t.Dispose()
                               t = Nothing
                           End Sub
        t.Start()
    End Sub

End Class
