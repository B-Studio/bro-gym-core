﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class abstract_cashchange
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblCurrent = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.btnCommit = New System.Windows.Forms.Button()
        Me.clos = New System.Windows.Forms.Button()
        Me.lblRaised = New System.Windows.Forms.Label()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(651, 37)
        Me.lblCaption.TabIndex = 30
        Me.lblCaption.Text = "Добавление абстрактной оплаты"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComment
        '
        Me.txtComment.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComment.BackColor = System.Drawing.Color.FromArgb(CType(CType(18, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtComment.Font = New System.Drawing.Font("Modern H EcoLight", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtComment.ForeColor = System.Drawing.Color.Gainsboro
        Me.txtComment.Location = New System.Drawing.Point(225, 181)
        Me.txtComment.MaxLength = 2048
        Me.txtComment.Multiline = True
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(374, 63)
        Me.txtComment.TabIndex = 33
        Me.txtComment.Text = "Исправление баланса BRO GYM"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Modern H Medium", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label2.Location = New System.Drawing.Point(129, 68)
        Me.Label2.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(289, 31)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Текущий баланс BRO GYM:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label2.UseCompatibleTextRendering = True
        '
        'lblCurrent
        '
        Me.lblCurrent.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCurrent.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblCurrent.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblCurrent.Location = New System.Drawing.Point(413, 72)
        Me.lblCurrent.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCurrent.Name = "lblCurrent"
        Me.lblCurrent.Size = New System.Drawing.Size(150, 24)
        Me.lblCurrent.TabIndex = 32
        Me.lblCurrent.Text = "10000 руб"
        Me.lblCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCurrent.UseCompatibleTextRendering = True
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Modern H Medium", 14.25!)
        Me.Label4.Location = New System.Drawing.Point(51, 137)
        Me.Label4.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(171, 24)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Добавить сумму:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label4.UseCompatibleTextRendering = True
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Modern H Medium", 14.25!)
        Me.Label1.Location = New System.Drawing.Point(51, 181)
        Me.Label1.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 24)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Описание:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label1.UseCompatibleTextRendering = True
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.NumericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NumericUpDown1.ForeColor = System.Drawing.Color.White
        Me.NumericUpDown1.Increment = New Decimal(New Integer() {100, 0, 0, 0})
        Me.NumericUpDown1.Location = New System.Drawing.Point(225, 139)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {100000, 0, 0, -2147483648})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(144, 22)
        Me.NumericUpDown1.TabIndex = 34
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown1.ThousandsSeparator = True
        Me.NumericUpDown1.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'btnCommit
        '
        Me.btnCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCommit.BackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.btnCommit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCommit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCommit.Font = New System.Drawing.Font("Modern H Medium", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnCommit.Location = New System.Drawing.Point(225, 264)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(264, 48)
        Me.btnCommit.TabIndex = 35
        Me.btnCommit.Text = "Записать"
        Me.btnCommit.UseVisualStyleBackColor = False
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(589, -9)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(62, 46)
        Me.clos.TabIndex = 36
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'lblRaised
        '
        Me.lblRaised.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblRaised.BackColor = System.Drawing.Color.Transparent
        Me.lblRaised.Font = New System.Drawing.Font("Modern H EcoLight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblRaised.ForeColor = System.Drawing.Color.FromArgb(CType(CType(160, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblRaised.Location = New System.Drawing.Point(392, 137)
        Me.lblRaised.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblRaised.Name = "lblRaised"
        Me.lblRaised.Size = New System.Drawing.Size(207, 24)
        Me.lblRaised.TabIndex = 32
        Me.lblRaised.Text = "(будет 10100 руб)"
        Me.lblRaised.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblRaised.UseCompatibleTextRendering = True
        '
        'abstract_cashchange
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(8, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(651, 324)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.btnCommit)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblRaised)
        Me.Controls.Add(Me.lblCurrent)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblCaption)
        Me.Font = New System.Drawing.Font("Modern H EcoLight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "abstract_cashchange"
        Me.Text = "abstract_cashchange"
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblCurrent As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnCommit As System.Windows.Forms.Button
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents lblRaised As System.Windows.Forms.Label
End Class
