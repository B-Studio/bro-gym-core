﻿Imports AForge.Video
Imports AForge.Video.DirectShow

Public Class add_new

    Private Dialog As DialogService = Service(Of DialogService)()

    Private Sub add_new_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DragByAControl.AttachMove(ToControl:=lblCaption)
        Theme.AttachOpacityFeatures(Me)
        txtID.ValidatingType = GetType(System.UInt32)
        mtxBirthday.ValidatingType = GetType(System.DateTime)
        For Each C As Control In Me.Controls
            If TypeOf C Is Button OrElse TypeOf C Is PictureBox Then _
                C.Cursor = CurPointer
        Next
        MyBase.Cursor = CurArrow
        Theme.Load()
        Theme.Apply(Me, States.Normal)
        Theme.ApplyButton(btnAddUser, States.Restricted)
        Theme.ApplyButton(btnCancel, States.Allowed)
        Theme.ApplyButton(btnZoom, States.Allowed)
        btnZoom.ForeColor = Color.DimGray
        Me.BackColor = Color.FromArgb(16, 16, 16)
        AddHandler btnCancel.MouseLeave,
            Sub(s As Object, ev As EventArgs) btnCancel.Text = "Отмена"
        ControlsOrder = New List(Of Control) From {txtID, txtLastName, txtFirstName, txtSurname, mtxBirthday, fldGenderBin, btnZoom, btnAddUser}
    End Sub

    Private Sub mtxID_Focused(sender As Object, e As EventArgs) Handles txtID.GotFocus
        txtID.ForeColor = Color.Gainsboro
    End Sub

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        If Not btnZoom.Text = "( увеличить )" Then
            PictureBox1.Size = New Size(290, 271)
            btnZoom.Text = "( увеличить )"
        Else
            Me.Hide()
            Me.Close()
        End If
    End Sub
    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter
        sender.ForeColor = Color.Red
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave
        sender.ForeColor = Color.Maroon
    End Sub

    Private Sub mtxBirthday_Unfocused(sender As Object, e As EventArgs) Handles mtxBirthday.LostFocus
        If TryParseDate() Then mtxBirthday.ForeColor = Color.Lime Else mtxBirthday.ForeColor = Color.Red
        CorrectButton()
    End Sub

    Private Sub txtNames_Unfocused(sender As Object, e As EventArgs) Handles txtFirstName.LostFocus, txtLastName.LostFocus, txtSurname.LostFocus
        Dim sText As TextBox = CType(sender, TextBox)
        If Regex.Check(sText.Text, "[а-яё]{2,}") Then
            sText.ForeColor = Color.Lime
            sText.Text = (sText.Text.ToUpper()(0) & sText.Text.Remove(0, 1)).Trim()
        Else : CType(sender, Control).ForeColor = Color.Red
        End If
        CorrectButton()
    End Sub
    Private Sub txtID_Unfocused(sender As Object, e As EventArgs) Handles txtID.LostFocus
        If Regex.Check(txtID.Text, "^\d{1,6}$") AndAlso CUInt(txtID.Text) > 0 Then
            If Not UserX.RegistredIDs.Contains(CUInt(txtID.Text)) Then
                txtID.ForeColor = Color.Aqua
                CorrectButton()
                Exit Sub
            End If
        End If
        txtID.ForeColor = Color.Red
        CorrectButton()
    End Sub

    Private Sub CorrectButton()
        Dim correct As Boolean = True
        correct = correct AndAlso Regex.Check(txtFirstName.Text, "[a-zа-яё]{2,}") _
                          AndAlso Regex.Check(txtLastName.Text, "[a-zа-яё]{2,}") _
                          AndAlso Regex.Check(txtSurname.Text, "[a-zа-яё]{2,}") _
                          AndAlso Regex.Check(txtID.Text, "^\d{1,6}$") _
                          AndAlso (CInt(txtID.Text) > 0) _
                          AndAlso (LocalWebCam IsNot Nothing AndAlso Not LocalWebCam.IsRunning)
        If correct AndAlso mtxBirthday.ForeColor = Color.Lime Then
            Theme.ApplyButton(btnAddUser, States.Allowed)
        Else : Theme.ApplyButton(btnAddUser, States.Restricted)
        End If
    End Sub

    Private Function TryParseDate() As Boolean
        Try
            Dim d As Date = ParseDate()
            If Now.Subtract(d).TotalDays < 1095 Then : Return False
            ElseIf Now.Year - d.Year > 99 Then : Return False
            Else : Return True
            End If
        Catch : Return False
        End Try
    End Function
    Private Function ParseDate() As Date
        Try
            Dim dts() As String = mtxBirthday.Text.Split(".")
            If dts(2).Length < 4 Then dts(2) = "1999".Remove(4 - dts(2).Length) & dts(2)
            Return New Date(dts(2), dts(1), dts(0))
        Catch ex As Exception : Throw ex
        End Try
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAddUser.Click
        Dim dateStr$ = String.Empty
        Try
            Dim dt As Date = ParseDate()
            dateStr = String.Format("{0}-{1}-{2}", dt.Year, dt.Month, dt.Day)
            Dim errSub$ = String.Empty
            If Now.Subtract(dt).TotalDays < 1095 Then : errSub = "меньше 3-ёх"
            ElseIf Now.Year - dt.Year > 99 Then : errSub = "больше 100"
            End If
            If errSub <> String.Empty Then
                Dialog.Show("Некорректная Дата Рождения!" & Chr(13) & "Возраст посетителя не может быть" & Chr(13) & errSub & " лет!",
                            "Некорректно заполнили", MessageBoxIcon.Information, MessageBoxButtons.OK)
                Exit Sub
            End If
            Dim correct As Boolean = True
            correct = correct AndAlso Regex.Check(txtFirstName.Text, "[a-zа-яё]{2,}") _
                              AndAlso Regex.Check(txtLastName.Text, "[a-zа-яё]{2,}") _
                              AndAlso Regex.Check(txtSurname.Text, "[a-zа-яё]{2,}") _
                              AndAlso Regex.Check(txtID.Text, "^\d{1,6}$") _
                              AndAlso (CInt(txtID.Text) > 0)
            If Not correct Then
                Dialog.Show("Некорректно заполнены поля." & Chr(13) &
                            "P.S. Поля ФИО должны содержать не менее 2ух букв," &
                            "а ID должен быть больше нуля.",
                            "Некорректно заполнили", MessageBoxIcon.Error, MessageBoxButtons.OK)
                Exit Sub
            End If
            If PictureBox1.Image Is Nothing AndAlso
                Dialog.Show("Вы уверены, что хотите добавить пользователя без его фотографии?",
                            "Нет фотографии", MessageBoxIcon.Error, MessageBoxButtons.YesNo) = DialogResult.No Then
                Exit Sub
            End If
        Catch
            Exit Sub
        End Try
        ' добавление нового юзера
        Try
            If IO.Directory.Exists(Application.StartupPath & "/katherine") AndAlso
               IO.File.Exists(Application.StartupPath & "/katherine/" & UShort.Parse(txtID.Text).ToString("000000") & ".sys") Then
                Try : IO.File.Delete(Application.StartupPath & "/katherine/" & UShort.Parse(txtID.Text).ToString("000000") & ".sys")
                Catch : End Try
            End If
            Dim selp As SQL.Response = SQL.Query("SELECT userid FROM users WHERE first_name='{0}' AND last_name='{1}' AND surname='{2}' AND birthday=TIMESTAMP('{3}')",
                                                txtFirstName.Text,
                                                txtLastName.Text,
                                                txtSurname.Text,
                                                dateStr)
            If selp.KindIs = SQL.Response.ResponseKind.Table AndAlso selp.Table.Height > 0 Then
                If Dialog.Show("Такой пользователь уже зарегистрирован в базе и имеет ID" &
                             CInt(selp.Table("userid")).ToString().PadLeft(6, "0"c) & "!" &
                             "Вы точно ничего не напутали и хотите добавить этого посетителя в базу?", "Уже существует в базе",
                             MessageBoxIcon.Question, MessageBoxButtons.YesNo) = DialogResult.No Then Exit Sub
            End If
            Dim resp As SQL.Response = SQL.Query("INSERT INTO users (userid,first_name,last_name,surname,itisbro,birthday,registred) " &
                                              "VALUES ({0},'{1}','{2}','{3}','{4}','{5}', NOW())",
                UShort.Parse(txtID.Text),
                txtFirstName.Text,
                txtLastName.Text,
                txtSurname.Text,
                _gender.IndexOf(fldGenderBin.Text),
                dateStr)
            If resp.KindIs = SQL.Response.ResponseKind.Excep Then Throw resp.Excep

            ' получаем количество юзеров с ID, равным тока что добавленному
            resp = SQL.Query("SELECT COUNT(*) FROM users WHERE userid=" & txtID.Text)
            If resp.Table(CULng(0), CByte(0)).ToString() = "1" Then
                AppRouting.ActWithForm(
                    Form:=AppForms.statistics,
                    Action:=Sub(f As statistics) f.RefreshState("users"))

                'If IsShown(Form1.F5) Then Form1.F5.RefreshState("users") TODO
                'User.LoadRegistred()
                'Dim uid As UInt32 = CUInt(txtID.Text)
                'If PictureBox1.Image IsNot Nothing AndAlso LocalWebCam IsNot Nothing AndAlso Not LocalWebCam.IsRunning Then
                '    IO.Directory.CreateDirectory("katherine")
                '    PictureBox1.Image.Save(Application.StartupPath & "/katherine/" & UShort.Parse(txtID.Text).ToString("000000") & ".sys", Imaging.ImageFormat.Bmp)
                'End If
                'If Not Form1.UserWindows.ContainsKey(uid) _
                'OrElse Form1.UserWindows(uid).IsDisposed Then
                '    Form1.UserWindows(uid) = New user_load() With {.IDtoLoad = uid}
                'End If
                'Form1.UserWindows(uid).Show()
                'SwitchToThisWindow(Form1.UserWindows(uid).Handle, True)
                'Form1.UserWindows(uid).Refresh()
                'Me.Close()
            End If
            ' -----------------------
        Catch ex As Exception
            MessageBox.Show(ex.GetType().Name & "!" & Chr(13) & ex.Message, " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If btnCancel.Text = "ОТМЕНИТЬ?" Then : Me.Close()
        Else : btnCancel.Text = "ОТМЕНИТЬ?"
        End If
    End Sub

    Private _gender As New List(Of String) From {"Женский", "Мужской", "Женский"}
    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles fldGenderBin.Click
        fldGenderBin.Text = _gender(_gender.IndexOf(fldGenderBin.Text) + 1)
    End Sub

    Dim LocalWebCamsCollection As New FilterInfoCollection(FilterCategory.VideoInputDevice)
    Dim WithEvents LocalWebCam As VideoCaptureDevice
    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If LocalWebCam Is Nothing OrElse Not LocalWebCam.IsRunning Then
            If LocalWebCamsCollection.Count > 0 Then
                LocalWebCam = New VideoCaptureDevice(LocalWebCamsCollection(0).MonikerString)
                LocalWebCam.Start()
            Else
                LocalWebCamsCollection = New FilterInfoCollection(FilterCategory.VideoInputDevice)
                If LocalWebCamsCollection.Count = 0 Then
                    Dialog.Show("Ни одной подключенной веб-камеры не найдено!", "Где вебка?", MessageBoxIcon.Error, MessageBoxButtons.OK)
                Else
                    LocalWebCam = New VideoCaptureDevice(LocalWebCamsCollection(0).MonikerString)
                    LocalWebCam.Start()
                End If
            End If
        ElseIf LocalWebCam IsNot Nothing AndAlso LocalWebCam.IsRunning Then
            LocalWebCam.Stop()
            CorrectButton()
        End If
    End Sub
    Private Sub WebCam_NewFrame(sender As Object, e As NewFrameEventArgs) Handles LocalWebCam.NewFrame
        PictureBox1.Image = e.Frame.Clone()
        e.Frame.Dispose()
    End Sub
    Private Sub add_new_Deactivated(sender As Object, e As EventArgs) Handles MyBase.Deactivate
        If LocalWebCam IsNot Nothing AndAlso LocalWebCam.IsRunning Then
            LocalWebCam.Stop()
            CorrectButton()
        End If
    End Sub

    Private Sub btnZoom_Click(sender As Object, e As EventArgs) Handles btnZoom.Click
        If CType(sender, Button).Text = "( увеличить )" Then
            PictureBox1.Size = New Size(1004, 429)
            sender.Text = "( уменьшить )"
            If LocalWebCam Is Nothing OrElse Not LocalWebCam.IsRunning Then
                If LocalWebCamsCollection.Count > 0 Then
                    LocalWebCam = New VideoCaptureDevice(LocalWebCamsCollection(0).MonikerString)
                    LocalWebCam.Start()
                Else
                    LocalWebCamsCollection = New FilterInfoCollection(FilterCategory.VideoInputDevice)
                    If LocalWebCamsCollection.Count = 0 Then _
                    Dialog.Show("Ни одной подключенной веб-камеры не найдено!", "Где вебка?", MessageBoxIcon.Error, MessageBoxButtons.OK)
                End If
            End If
        Else
            If LocalWebCam IsNot Nothing AndAlso LocalWebCam.IsRunning Then
                LocalWebCam.Stop()
                CorrectButton()
            End If
            PictureBox1.Size = New Size(290, 271)
            sender.Text = "( увеличить )"
        End If
    End Sub

    Private ControlsOrder As List(Of Control)
    Private Sub btnZoom_KeyUp(s As Object, e As KeyEventArgs) Handles txtSurname.KeyUp, txtLastName.KeyUp, txtID.KeyUp, txtFirstName.KeyUp, mtxBirthday.KeyUp, fldGenderBin.KeyUp, btnZoom.KeyUp
        If e.KeyData = Keys.Up OrElse e.KeyData = Keys.Down Then
            Dim idxOf As SByte = -1, i As SByte = 0
            For Each C As Control In ControlsOrder
                If C.Name = CType(s, Control).Name Then Exit For
                i += 1
            Next
            If i < ControlsOrder.Count Then idxOf = i Else Exit Sub

            If idxOf < 0 Then Exit Sub
            If e.KeyData = Keys.Down Then
                If idxOf < ControlsOrder.Count - 1 Then
                    ControlsOrder(idxOf + 1).Focus()
                Else : ControlsOrder(0).Focus()
                End If
            ElseIf e.KeyData = Keys.Up Then
                If idxOf > 0 Then ControlsOrder(idxOf - 1).Focus()
            End If
        End If
    End Sub

    Private Sub fldGender_GotFocus(s As Object, e As EventArgs) Handles fldGenderBin.GotFocus, btnZoom.GotFocus
        CType(s, Button).FlatAppearance.BorderSize = 1
        CType(s, Button).FlatAppearance.BorderColor = Color.FromArgb(16, 32, 24)
    End Sub
    Private Sub fldGender_LostFocus(s As Object, e As EventArgs) Handles fldGenderBin.LostFocus, btnZoom.LostFocus
        CType(s, Button).FlatAppearance.BorderSize = 0
    End Sub

End Class