﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class statistics
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.clos = New System.Windows.Forms.Button()
        Me.lstQueryResult = New System.Windows.Forms.ListView()
        Me.cmbQueryType = New System.Windows.Forms.ComboBox()
        Me.btnCommit = New System.Windows.Forms.Button()
        Me.lblResize = New System.Windows.Forms.Label()
        Me.mini = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(806, 37)
        Me.lblCaption.TabIndex = 29
        Me.lblCaption.Text = "Просмотр результатов за сегодня"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(744, -9)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(62, 46)
        Me.clos.TabIndex = 30
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'lstQueryResult
        '
        Me.lstQueryResult.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstQueryResult.BackColor = System.Drawing.Color.FromArgb(CType(CType(18, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(22, Byte), Integer))
        Me.lstQueryResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstQueryResult.Font = New System.Drawing.Font("Modern H Medium", 12.0!)
        Me.lstQueryResult.ForeColor = System.Drawing.Color.White
        Me.lstQueryResult.FullRowSelect = True
        Me.lstQueryResult.Location = New System.Drawing.Point(12, 94)
        Me.lstQueryResult.Name = "lstQueryResult"
        Me.lstQueryResult.Size = New System.Drawing.Size(782, 374)
        Me.lstQueryResult.TabIndex = 31
        Me.lstQueryResult.UseCompatibleStateImageBehavior = False
        Me.lstQueryResult.View = System.Windows.Forms.View.Details
        '
        'cmbQueryType
        '
        Me.cmbQueryType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbQueryType.BackColor = System.Drawing.Color.FromArgb(CType(CType(18, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(22, Byte), Integer))
        Me.cmbQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbQueryType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbQueryType.Font = New System.Drawing.Font("Modern H EcoLight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.cmbQueryType.ForeColor = System.Drawing.Color.White
        Me.cmbQueryType.FormattingEnabled = True
        Me.cmbQueryType.Location = New System.Drawing.Point(12, 56)
        Me.cmbQueryType.Name = "cmbQueryType"
        Me.cmbQueryType.Size = New System.Drawing.Size(618, 27)
        Me.cmbQueryType.TabIndex = 32
        '
        'btnCommit
        '
        Me.btnCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCommit.BackColor = System.Drawing.Color.FromArgb(CType(CType(18, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(22, Byte), Integer))
        Me.btnCommit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(4, Byte), Integer), CType(CType(16, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(14, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCommit.Font = New System.Drawing.Font("Modern H EcoLight", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnCommit.Location = New System.Drawing.Point(636, 54)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(158, 31)
        Me.btnCommit.TabIndex = 33
        Me.btnCommit.Text = "Обновить"
        Me.btnCommit.UseVisualStyleBackColor = False
        '
        'lblResize
        '
        Me.lblResize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblResize.BackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.lblResize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblResize.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.lblResize.Location = New System.Drawing.Point(773, 471)
        Me.lblResize.Name = "lblResize"
        Me.lblResize.Size = New System.Drawing.Size(35, 11)
        Me.lblResize.TabIndex = 34
        '
        'mini
        '
        Me.mini.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.mini.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.mini.FlatAppearance.BorderSize = 0
        Me.mini.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.mini.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.mini.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.mini.ForeColor = System.Drawing.Color.Olive
        Me.mini.Location = New System.Drawing.Point(695, -10)
        Me.mini.Margin = New System.Windows.Forms.Padding(12)
        Me.mini.Name = "mini"
        Me.mini.Size = New System.Drawing.Size(46, 47)
        Me.mini.TabIndex = 35
        Me.mini.Text = "_"
        Me.mini.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.mini.UseVisualStyleBackColor = False
        '
        'today_profit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 23.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(806, 480)
        Me.Controls.Add(Me.mini)
        Me.Controls.Add(Me.btnCommit)
        Me.Controls.Add(Me.cmbQueryType)
        Me.Controls.Add(Me.lstQueryResult)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.lblResize)
        Me.Font = New System.Drawing.Font("Modern H EcoLight", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.MinimumSize = New System.Drawing.Size(516, 156)
        Me.Name = "today_profit"
        Me.Text = "today_profit"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents lstQueryResult As System.Windows.Forms.ListView
    Friend WithEvents cmbQueryType As System.Windows.Forms.ComboBox
    Friend WithEvents btnCommit As System.Windows.Forms.Button
    Friend WithEvents lblResize As System.Windows.Forms.Label
    Friend WithEvents mini As System.Windows.Forms.Button
End Class
