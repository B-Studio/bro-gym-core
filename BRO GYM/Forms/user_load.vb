﻿Public Class user_load
    Private Dialog As DialogService = Service(Of DialogService)()

    Public IDtoLoad As UInt32 = 0
    Private User As UserX

    Private Function CBoolean(s$) As Boolean
        If s Is Nothing Then Return False
        Return s.Length > 0
    End Function

    Private Sub FormLoaded(sender As Object, e As EventArgs) Handles MyBase.Load
        If sender IsNot Nothing Then
            Theme.AttachOpacityFeatures(Me)
            DragByAControl.AttachMove(Me.lblCaption)
            DragByAControl.AttachResize(lblResize)
            flowVariables.AutoScroll = True
            Theme.Load()
            Me.BackColor = Color.FromArgb(16, 16, 16)
            If My.Settings.user_load_size.Height > 0 AndAlso
               My.Settings.user_load_size.Width > 0 Then _
               Me.Size = My.Settings.user_load_size
            If My.Settings.user_load_location.X >= 0 AndAlso
               My.Settings.user_load_location.Y >= 0 Then _
               Me.Location = My.Settings.user_load_location
        End If
        Try : Me.User = UserX.LoadByID(Me.IDtoLoad)
        Catch ex As Exception : Throw ex
        End Try
        If Me.User IsNot Nothing Then
            If Me.User.IsAnonymous Then Me.Icon = My.Resources.anonymous_load
            flowVariables.Controls.Clear()
            fldFirstLastName.Text = Me.User.FirstLastName
            fldSurname.Text = Me.User.Surname
            fldBirthday.Text = Me.User.Birthday
            fldYearsOld.Text = Me.User.YearsOld
            fldRegistred.Text = Me.User.RegDate
            fldRegistredOld.Text = String.Format("({0})", Me.User.RegOld)
            fldID.Text = Me.User.IDStr
            _txtCommentCache = Me.User.Comment
            txtComment.Text = Me.User.Comment
            If sender IsNot Nothing Then Me.LoadAvatar(Me.User.ID)
            If Me.User.IsBro Then
                lblBirthday.Text = "Родился:"
                lblRegistred.Text = "Появился в базе:"
                lblNotVisited.Text = "сегодня не посещал"
            Else
                lblBirthday.Text = "Родилась:"
                lblRegistred.Text = "Появилась в базе:"
                lblNotVisited.Text = "сегодня не посещала"
            End If
            Me.LoadVariables()
            If Not Me.User.IsAnonymous Then
                lblCaption.Text = "Просмотр содержимого карты #" & Me.User.IDStr
                If Me.User.Birthday.StartsWith(Now.ToString("dd.MM.")) Then
                    Dim msgText$ = String.Empty
                    If Me.User.IsBro Then msgText = "У посетителя '{0}'" & Chr(10) & Chr(13) & "сегодня День Рожения!" & Chr(10) & Chr(13) & "Ему сегодня исполнилось {1}!" _
                                     Else msgText = "У посетительницы '{0}'" & Chr(10) & Chr(13) & "сегодня День Рождения!" & Chr(10) & Chr(13) & "Ей сегодня исполнилось {1}!"
                    Dialog.Show(String.Format(msgText, Me.User.FirstLastName, Me.User.YearsOld), "День Рождения",
                                 MessageBoxIcon.Information, MessageBoxButtons.OK)
                End If
            Else : lblCaption.Text = "Виртуальная карта анонимного посетителя BRO GYM"
            End If
        End If
        Me.Text = Me.User.FirstLastName & " :: BRO GYM"
    End Sub

    Dim AvatarImage As Image
    Private Sub LoadAvatar(UserID As UInt32)
        Dim avatarPath$ = String.Format("{0}/katherine/{1}.sys", Application.StartupPath, UserID.ToString("000000"))
        If IO.Directory.Exists("katherine") AndAlso IO.File.Exists(avatarPath) Then
            Try
                If AvatarImage IsNot Nothing Then AvatarImage.Dispose() : AvatarImage = Nothing
                AvatarImage = New Bitmap(Image.FromStream(New IO.FileStream(avatarPath, IO.FileMode.Open, IO.FileAccess.Read, IO.FileShare.ReadWrite)))
                PictureBox1.Image = AvatarImage
            Catch : End Try
        End If
    End Sub
    Private Sub LoadVariables()
        Me.UpdateTicketInfo()
        Me.UpdateUserInventory()
    End Sub

    Private Sub UpdateTicketInfo()
        Me.User.LoadUserInv()
        Me.User.UpdateTicketInfo()
        If Not Me.User.IsAnonymous Then
            fldEndsAt.Text = Me.User.ExpiresAfter
            fldTicketExpires.Text = Me.User.ExpiresStr
            Select Case Me.User.TickType
                Case UserX.TicketType.Unlimited
                    lblTicketExpAt.Text = "Абонемент до:"
                Case UserX.TicketType.DayTime
                    lblTicketExpAt.Text = "Дневной абонемент до:"
                Case UserX.TicketType.OneDayLight
                    lblTicketExpAt.Text = "Дневное посещение до:"
                Case UserX.TicketType.OneDayUnlim
                    lblTicketExpAt.Text = "Разовое посещение до:"
                Case UserX.TicketType.Undefined
                    lblTicketExpAt.Text = "Абонементов не имеет"
            End Select
        Else
            fldEndsAt.Text = ""
            fldTicketExpires.Text = ""
            lblTicketExpAt.Text = "Он вообще самый крутой в этом зале"
            lblTicketExpAt.BringToFront()
        End If
        tmrValidator.Start()
    End Sub

#Region "Pulse features"
    Private Sub btnWriteVisitPulse(Optional Pulse As Boolean = True)
        visitPulseTimer.Enabled = Pulse
        If Not Pulse Then btnWriteVisit.BackColor = Color.Black
    End Sub
    Private _vX As Int16 = -50,
            _sX As Int16 = -50
    Private WithEvents visitPulseTimer As New Timer() With {.Interval = 2},
                       savePulseTimer As New Timer() With {.Interval = 2}
    Private Sub visitPulseTimer_Tick(sender As Object, e As EventArgs) Handles visitPulseTimer.Tick
        If _vX < 50 Then _vX += 1 Else _vX = -50
        If ForeColor = Color.Lime Then _
             btnWriteVisit.BackColor = Color.FromArgb(0, SinColor(_vX), 0) _
        Else btnWriteVisit.BackColor = Color.FromArgb(SinColor(_vX) * 0.7, SinColor(_vX) * 0.7, 0)
    End Sub
    Private Sub btnSavePulse(Optional Pulse As Boolean = True)
        savePulseTimer.Enabled = Pulse
        If Not Pulse Then btnRefresh.BackColor = Color.Black
    End Sub
    Private Sub savePulseTimer_Tick(sender As Object, e As EventArgs) Handles savePulseTimer.Tick
        If _sX < 50 Then _sX += 1 Else _sX = -50
        If ForeColor = Color.Lime Then _
             btnRefresh.BackColor = Color.FromArgb(0, SinColor(_sX), 0) _
        Else btnRefresh.BackColor = Color.FromArgb(SinColor(_sX) * 0.7, SinColor(_sX) * 0.7, 0)
    End Sub
    Private Function SinColor(X) As Integer
        Return Math.Round(24 + 24 * Math.Sin(X * Math.PI * 0.02))
    End Function
#End Region

    Private fLoad As Boolean = True
    Private Sub UpdateUserInventory()
        If Not Me.User.IsAnonymous Then
            Me.User.LoadUserInv()
            For Each KV As KeyValuePair(Of Atom, SByte) In Me.User.Have
                If KV.Value > 0 Then
                    Dim name$ = String.Format("UnusedGoodId{0}", KV.Key.Id)
                    If Not Me.Controls.ContainsKey("fld" & name) Then
                        Dim flw As New FlowLayoutPanel() With {
                            .Anchor = AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top,
                            .Size = New Size(556, 36),
                            .Name = "flw" & name,
                            .Margin = New Padding(0)
                            }
                        Dim lbl As New Label() With {
                            .Anchor = AnchorStyles.Left,
                            .Font = New Font("Modern H EcoLight", 15.75!, FontStyle.Regular, GraphicsUnit.Point),
                            .TextAlign = ContentAlignment.MiddleLeft,
                            .Text = KV.Key.OnGuiName & ":",
                            .AutoSize = True,
                            .Name = "lbl" & name,
                            .Margin = New Padding(0, 0, 0, 0)
                        }
                        Dim fld As New Label() With {
                            .Font = New Font("Modern H Medium", 15.75!, FontStyle.Regular, GraphicsUnit.Point),
                            .Anchor = AnchorStyles.Left,
                            .AutoSize = True,
                            .Text = KV.Value,
                            .Name = "fld" & name,
                            .Margin = New Padding(2, 0, 0, 0)
                        }
                        flw.Controls.Add(lbl)
                        flw.Controls.Add(fld)
                        flowVariables.Controls.Add(flw)
                    End If
                End If
            Next
        Else
            lblNotVisited.Hide()
            Dim name$ = "UnusedGoodIdX"
            If Not Me.Controls.ContainsKey("fld" & name) Then
                Dim flw As New FlowLayoutPanel() With {
                    .Anchor = AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top,
                    .Size = New Size(556, 138),
                    .Padding = New Padding(0, 20, 0, 0),
                    .Name = "flw" & name
                    }
                Dim lblTotalVisits As New Label() With {
                    .Anchor = AnchorStyles.Left,
                    .Font = New Font("Modern H EcoLight", 15.75!, FontStyle.Regular, GraphicsUnit.Point),
                    .TextAlign = ContentAlignment.MiddleLeft,
                    .Text = "Всего посещений за сегодня: " & SQL.Special.TotalVisits(),
                    .AutoSize = True,
                    .Margin = New Padding(0, 0, 0, 0),
                    .Name = "lblTotalVisits"
                }
                Dim lblTotalPaid As New Label() With {
                    .Anchor = AnchorStyles.Left,
                    .Font = New Font("Modern H EcoLight", 15.75!, FontStyle.Regular, GraphicsUnit.Point),
                    .TextAlign = ContentAlignment.MiddleLeft,
                    .Text = "Оплаченных посещений за сегодня: " & SQL.Special.TotalPaid(),
                    .AutoSize = True,
                    .Margin = New Padding(0, 6, 0, 0),
                    .Name = "lblTotalPaidX1"
                }
                Dim lblTotalProfit As New Label() With {
                    .Anchor = AnchorStyles.Left,
                    .Font = New Font("Modern H EcoLight", 15.75!, FontStyle.Regular, GraphicsUnit.Point),
                    .TextAlign = ContentAlignment.MiddleLeft,
                    .Text = "Общая прибыль за сегодня: " & SQL.Special.TotalProfit & " руб",
                    .AutoSize = True,
                    .Margin = New Padding(0, 6, 0, 0),
                    .Name = "lblTotalProfitX1"
                }
                flw.Controls.Add(lblTotalVisits)
                flw.Controls.Add(lblTotalPaid)
                flw.Controls.Add(lblTotalProfit)
                flowVariables.Controls.Add(flw)
            End If
        End If
    End Sub

    Private WithEvents tmrValidator As New Timer With {.Interval = 500, .Enabled = False}
    Private Sub tmrValidator_Tick(sender As Object, e As EventArgs) Handles tmrValidator.Tick
        tmrValidator.Stop()
        btnWriteVisit.Show()
        If Me.User IsNot Nothing AndAlso Not Me.User.IsAnonymous Then
            If Me.User.TicketIsOK Then
                ' TICKET IS OKAY
                Theme.Apply(Me, States.Allowed)
                Dim hVis As Boolean = SQL.Special.HaveATodayVisit(Me.User.ID)
                If Not hVis Then
                    lblNotVisited.ForeColor = Color.FromArgb(154, 222, 0)
                    If Me.User.IsBro Then lblNotVisited.Text = "сегодня не посещал" _
                                 Else lblNotVisited.Text = "сегодня не посещала"
                Else
                    Dim vdate As Date = SQL.Special.LastVisitDate(Me.User.ID)
                    lblNotVisited.ForeColor = Color.FromArgb(0, 105, 38)
                    If Me.User.IsBro Then lblNotVisited.Text = "посещал сегодня в " & vdate.ToString("HH:mm") _
                                 Else lblNotVisited.Text = "посещала сегодня в " & vdate.ToString("HH:mm")
                End If
                btnWriteVisitPulse(Not hVis)
            ElseIf Me.User.HaveOneDayTickets Then
                ' HAVE ONE-DAY TICKETs
                Theme.Apply(Me, States.Unknown)
                btnWriteVisitPulse(True)
            Else 'ALL INCORRECT OR NOT VALID
                Theme.Apply(Me, States.Restricted)
                btnWriteVisit.Hide()
            End If
        ElseIf Me.User IsNot Nothing Then ' ITIS ANONYMOUS
            Theme.Apply(Me, States.Aqua)
        End If
        tmrValidator.Interval = 1
        lblCaption.ForeColor = Color.White
    End Sub
    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub
    Private Sub mini_Click(sender As Object, e As EventArgs) Handles mini.Click
        txtComment.Focus()
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter, mini.MouseEnter
        If sender Is Me.clos Then _
        sender.ForeColor = Color.Red _
        Else sender.ForeColor = Color.Yellow
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave, mini.MouseLeave
        If sender Is Me.clos Then _
        sender.ForeColor = Color.Maroon _
        Else sender.ForeColor = Color.Olive
    End Sub

    Private crt As cart, abstr As abstract_cashchange
    Private Sub brnCart_Click(sender As Object, e As EventArgs) Handles brnCart.Click
        If crt Is Nothing OrElse crt.IsDisposed Then crt = New cart()
        crt.USR = Me.User
        Dim kkode As String = code_queue.ToArray()
        code_queue.Clear()
        If Me.User.IsAnonymous AndAlso Regex.Check(kkode, "^\d{3}$") Then
            If kkode = "005" OrElse
               kkode = "001" OrElse
               kkode = "002" OrElse
               kkode = "003" OrElse
               kkode = "004" Then
                If vista Is Nothing OrElse vista.IsDisposed Then vista = New add_anon_visit()
                If kkode = "005" Then
                    vista.onePers = True
                    vista.lblPers.Image = My.Resources.checkbox_checked
                End If
                If kkode = "003" OrElse
                   kkode = "004" Then
                    vista.onePay = False
                    vista.lblOnePay.Image = My.Resources.checkbox
                    vista.onePers = False
                    vista.lblPers.Image = My.Resources.checkbox
                End If
                vista.ShowDialog()
                If Not vista.IsDisposed OrElse vista IsNot Nothing Then
                    vista.Dispose()
                    vista = Nothing
                End If
                Call FastRef()
                Exit Sub
            ElseIf kkode = "999" Then
                If abstr Is Nothing OrElse abstr.IsDisposed Then _
                    abstr = New abstract_cashchange()
                abstr.ShowDialog()
                If abstr IsNot Nothing OrElse Not abstr.IsDisposed Then
                    abstr.Dispose()
                    abstr = Nothing
                End If
                Call FastRef()
                Exit Sub
            End If
        End If
        crt.txtBarcode.Text = kkode
        kkode = String.Empty
        crt.ShowDialog()
        If crt IsNot Nothing OrElse Not crt.IsDisposed Then
            crt.Dispose()
            crt = Nothing
        End If
        Call FastRef()
    End Sub

    Private Sub FastRef(Optional Fast As Boolean = True)
        For Each C As Control In flowVariables.Controls
            C.Hide() : C.Dispose()
            flowVariables.Controls.Remove(C)
        Next
        If Not Fast Then Threading.Thread.Sleep(200)
        Me.FormLoaded(Nothing, Nothing)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        If btnRefresh.Text = "Сохранить" Then
            If LocalWebCam IsNot Nothing Then
                If Not LocalWebCam.IsRunning Then
                    If Dialog.Show("Сохранить новую фотографию посетителю #" & Me.User.ID.ToString("0000") &
                                    " (" & Me.User.FirstLastName & ")?", "Смена фотографии",
                                    MessageBoxIcon.Information, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                        Try
                            LocalWebCam = Nothing
                            Dim aPath$ = String.Format("{0}/katherine/{1}.sys", Application.StartupPath, Me.User.IDStr)
                            PictureBox1.Image.Save(aPath, Imaging.ImageFormat.Png)
                        Catch ex As Exception
                            Dialog.Show(ex.Message, "Возникла ошибочка.. Звони Сане", MessageBoxIcon.Error, MessageBoxButtons.OK)
                        End Try
                    End If
                Else : Exit Sub
                End If
            End If
            Dim m As SQL.Response = SQL.Query(SQL.UpdateComment, txtComment.Text, Me.User.ID)
            If m.KindIs = SQL.Response.ResponseKind.Changed Then
                txtComment.Text = String.Empty
                btnRefresh.Text = "Обновить данные"
                btnSavePulse(False)
            End If
        End If
        fldBirthday.Hide() : fldYearsOld.Hide()
        fldRegistred.Hide() : fldRegistredOld.Hide()
        fLoad = False
        FastRef(Fast:=False)
        fldBirthday.Show() : fldYearsOld.Show()
        fldRegistred.Show() : fldRegistredOld.Show()
    End Sub

    Private _txtCommentCache As String = String.Empty
    Private Sub txtComment_TextChanged(sender As Object, e As EventArgs) Handles txtComment.TextChanged
        If txtComment.Text <> Me._txtCommentCache OrElse
           LocalWebCam IsNot Nothing Then
            btnRefresh.Text = "Сохранить"
            btnSavePulse(Pulse:=(LocalWebCam Is Nothing OrElse Not LocalWebCam.IsRunning))
        Else
            btnRefresh.Text = "Обновить данные"
            btnSavePulse(False)
        End If
    End Sub

    Private vist As add_visit, vista As add_anon_visit
    Private Sub btnWriteVisit_Click(sender As Object, e As EventArgs) Handles btnWriteVisit.Click
        If Not Me.User.IsAnonymous Then
            If vist Is Nothing OrElse vist.IsDisposed Then _
                vist = New add_visit() With {.USR = Me.User}
            vist.ShowDialog()
            If Not vist.IsDisposed OrElse vist IsNot Nothing Then
                vist.Dispose()
                vist = Nothing
            End If
            FastRef()
        Else
            If vista Is Nothing OrElse vista.IsDisposed Then vista = New add_anon_visit()
            vista.onePers = False : vista.onePay = False
            vista.lblPers.Image = My.Resources.checkbox
            vista.lblOnePay.Image = My.Resources.checkbox
            vista.ShowDialog()
            If Not vista.IsDisposed OrElse vista IsNot Nothing Then
                vista.Dispose()
                vista = Nothing
            End If
        End If
        Call FastRef()
    End Sub

    Private Sub MeFormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        My.Settings.user_load_size = Me.Size
        My.Settings.user_load_location = Me.Location
        My.Settings.Save()
    End Sub

    Private code_queue As New Queue(Of Char)
    Private Sub brnCart_KeyPress(sender As Object, e As KeyPressEventArgs) _
        Handles brnCart.KeyPress, btnRefresh.KeyPress, btnWriteVisit.KeyPress,
                Me.KeyPress, txtComment.KeyPress
        If IsNumeric(e.KeyChar) Then
            code_queue.Enqueue(e.KeyChar)
            If code_queue.Count = 4 Then code_queue.Dequeue()
            If Not brnCart.Focused AndAlso sender IsNot txtComment Then brnCart.Focus()
        ElseIf sender Is txtComment And e.KeyChar = Chr(13) AndAlso code_queue.Count = 3 Then
            brnCart_Click(Nothing, Nothing)
        End If
    End Sub

    Private expires_cha As expires_change
    Private Sub fldTicketExpires_DoubleClick(sender As Object, e As EventArgs) Handles fldTicketExpires.DoubleClick
        If Not Me.User.IsAnonymous AndAlso (Me.User.TickType = UserX.TicketType.Unlimited OrElse
                                        Me.User.TickType = UserX.TicketType.DayTime) Then
            If expires_cha Is Nothing OrElse expires_cha.IsDisposed Then _
                expires_cha = New expires_change() With {
                    .UserID = Me.User.ID,
                    .TicketID = Me.User.MaxTicketID,
                    .ExpiresOld = Me.User.Expires
                    }
            expires_cha.ShowDialog()
            If Not expires_cha.IsDisposed OrElse expires_cha IsNot Nothing Then
                expires_cha.Dispose()
                expires_cha = Nothing
            End If
            FastRef()
        End If
    End Sub


    Dim LocalWebCamsCollection As New AForge.Video.DirectShow.FilterInfoCollection(AForge.Video.DirectShow.FilterCategory.VideoInputDevice)
    Dim WithEvents LocalWebCam As AForge.Video.DirectShow.VideoCaptureDevice
    Private Sub PB1_DoubleClick(sender As Object, e As EventArgs) Handles PictureBox1.DoubleClick
        If LocalWebCam Is Nothing OrElse Not LocalWebCam.IsRunning Then
            If LocalWebCamsCollection.Count > 0 Then
                LocalWebCam = New AForge.Video.DirectShow.VideoCaptureDevice(
                    LocalWebCamsCollection(0).MonikerString)
                LocalWebCam.Start()
            Else
                LocalWebCamsCollection = New AForge.Video.DirectShow.FilterInfoCollection(
                    AForge.Video.DirectShow.FilterCategory.VideoInputDevice)
                If LocalWebCamsCollection.Count = 0 Then
                    Dialog.Show("Ни одной подключенной веб-камеры не найдено!", "Где вебка?",
                                 MessageBoxIcon.Error, MessageBoxButtons.OK)
                Else
                    LocalWebCam = New AForge.Video.DirectShow.VideoCaptureDevice(LocalWebCamsCollection(0).MonikerString)
                    LocalWebCam.Start()
                End If
            End If
        End If
    End Sub
    Private Sub PB1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click, MyBase.Deactivate
        If LocalWebCam IsNot Nothing Then
            If LocalWebCam.IsRunning Then
                LocalWebCam.Stop()
                btnRefresh.Text = "Сохранить"
                btnSavePulse(True)
            ElseIf sender Is PictureBox1 Then
                btnSavePulse(False)
                LocalWebCam.Start()
            End If
        End If
    End Sub
    Private Sub WebCam_NewFrame(sender As Object, e As AForge.Video.NewFrameEventArgs) Handles LocalWebCam.NewFrame
        PictureBox1.Image = e.Frame.Clone()
        e.Frame.Dispose()
    End Sub

End Class
