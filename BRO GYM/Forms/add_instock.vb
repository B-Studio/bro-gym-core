﻿Public Class add_instock

    Private Sub add_instock_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Theme.ApplyButton(btnCommit, States.Restricted)
        Theme.AttachOpacityFeatures(Me)
        DragByAControl.AttachMove(ToControl:=lblCaption)
        Goods.Load()
    End Sub



    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Close()
    End Sub
    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter
        sender.ForeColor = Color.Red
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave
        sender.ForeColor = Color.Maroon
    End Sub

    Private GoodId As Short = -1
    Private CurrProd As Product
    Private Sub txtSurname_KeyUp(sender As Object, e As KeyEventArgs) Handles txtBarcode.KeyUp
        If e.KeyCode = Keys.Enter AndAlso txtBarcode.Text Like "*###" Then
            e.SuppressKeyPress = True
            txtBarcode.Text = txtBarcode.Text.Remove(0, txtBarcode.TextLength - 3)
            GoodId = CUShort(txtBarcode.Text)
            txtBarcode.SelectAll()
            If Goods.HaveId(GoodId) Then
                CurrProd = Goods.ById(GoodId)
                txtBarcode.Text = CurrProd.Name
                numInStock.Enabled = CurrProd.Atom.InStock >= 0
                numInStock.Value = CurrProd.Atom.InStockCached
                txtBarcode.SelectAll()
                If numInStock.Enabled Then
                    numInStock.Focus()
                    lblNowStock.Text = "сейчас на складе: " & numInStock.Value
                Else : lblNowStock.Text = "(не счётный товар)"
                End If
            End If ' this good exists
        End If ' key enter & text ok
    End Sub

    Private Sub numInStock_ValueChanged(sender As Object, e As EventArgs) Handles numInStock.ValueChanged, numInStock.KeyUp
        If numInStock.Value >= 0 AndAlso Me.CurrProd.Atom.InStockCached <> numInStock.Value Then
            If numInStock.Value > Me.CurrProd.Atom.InStockCached Then
                lblChangeVal.Text = "добавить "
            Else : lblChangeVal.Text = "убавить "
            End If
            lblChangeVal.Text &= Math.Abs(numInStock.Value - Me.CurrProd.Atom.InStockCached) & " ед."
            Theme.ApplyButton(btnCommit, States.Allowed)
            btnCommit.ForeColor = Color.White
        Else
            lblChangeVal.Text = ""
            Theme.ApplyButton(btnCommit, States.Restricted)
            btnCommit.ForeColor = Color.Gray
        End If
    End Sub

    Private Sub btnCommit_Click(sender As Object, e As EventArgs) Handles btnCommit.Click
        If numInStock.Value >= 0 AndAlso Me.CurrProd.Atom.InStockCached <> numInStock.Value Then
            Me.CurrProd.Atom.InStock = numInStock.Value
            If Me.CurrProd.Atom.InStockCached = numInStock.Value Then
                lblChangeVal.Text = ""
                lblChangeVal.Refresh()
                btnCommit.Text = "Сохранено !"
                btnCommit.ForeColor = Color.Lime
                btnCommit.Refresh()
                System.Threading.Thread.Sleep(1000)
                btnCommit.Text = "Сохранить"
                lblNowStock.Text = "сейчас на складе: " & numInStock.Value
                Theme.ApplyButton(btnCommit, States.Restricted)
                btnCommit.ForeColor = Color.Gray
                btnCommit.Refresh()
            End If
        End If
    End Sub

    Private Sub FormShown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Me.Activate()
        SetForegroundWindow(Me.Handle)
        SetActiveWindow(Me.Handle)
    End Sub
End Class