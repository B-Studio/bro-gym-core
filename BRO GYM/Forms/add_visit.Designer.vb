﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class add_visit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(add_visit))
        Me.lblText = New System.Windows.Forms.Label()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.clos = New System.Windows.Forms.Button()
        Me.btnCommit = New System.Windows.Forms.Button()
        Me.lblPers = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblText
        '
        Me.lblText.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblText.BackColor = System.Drawing.Color.Transparent
        Me.lblText.Font = New System.Drawing.Font("Modern H EcoLight", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblText.Location = New System.Drawing.Point(36, 50)
        Me.lblText.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(520, 146)
        Me.lblText.TabIndex = 23
        Me.lblText.Text = "Пользователь неизвестен и сейчас 16:00." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "В базу будет записано разовое дневное по" & _
    "сещение и оплата 150р."
        Me.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblText.UseCompatibleTextRendering = True
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(593, 37)
        Me.lblCaption.TabIndex = 28
        Me.lblCaption.Text = "Запись посещения в базу"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(531, -9)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(62, 46)
        Me.clos.TabIndex = 29
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'btnCommit
        '
        Me.btnCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCommit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCommit.Font = New System.Drawing.Font("Modern H Medium", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnCommit.Location = New System.Drawing.Point(163, 389)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(264, 73)
        Me.btnCommit.TabIndex = 30
        Me.btnCommit.Text = "Записать"
        Me.btnCommit.UseVisualStyleBackColor = True
        '
        'lblPers
        '
        Me.lblPers.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblPers.BackColor = System.Drawing.Color.Transparent
        Me.lblPers.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblPers.Font = New System.Drawing.Font("Modern H Medium", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblPers.Image = CType(resources.GetObject("lblPers.Image"), System.Drawing.Image)
        Me.lblPers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPers.Location = New System.Drawing.Point(58, 202)
        Me.lblPers.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblPers.Name = "lblPers"
        Me.lblPers.Size = New System.Drawing.Size(477, 32)
        Me.lblPers.TabIndex = 23
        Me.lblPers.Text = "        списать с карты персональную тренировку"
        Me.lblPers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPers.UseCompatibleTextRendering = True
        '
        'txtComment
        '
        Me.txtComment.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComment.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(18, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtComment.Font = New System.Drawing.Font("Modern H EcoLight", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtComment.ForeColor = System.Drawing.Color.Gainsboro
        Me.txtComment.Location = New System.Drawing.Point(46, 299)
        Me.txtComment.MaxLength = 2048
        Me.txtComment.Multiline = True
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(500, 63)
        Me.txtComment.TabIndex = 33
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Modern H EcoLight", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.Location = New System.Drawing.Point(46, 272)
        Me.Label1.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(500, 24)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Комментарий:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.UseCompatibleTextRendering = True
        '
        'add_visit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 24.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(593, 474)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCommit)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.lblPers)
        Me.Controls.Add(Me.lblText)
        Me.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "add_visit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "add_visit"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents btnCommit As System.Windows.Forms.Button
    Friend WithEvents lblPers As System.Windows.Forms.Label
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
