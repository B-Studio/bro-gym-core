﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cart))
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.txtBarcode = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblOverallPrice = New System.Windows.Forms.Label()
        Me.lblResize = New System.Windows.Forms.Label()
        Me.flowPurchases = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.clos = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(602, 44)
        Me.lblCaption.TabIndex = 16
        Me.lblCaption.Text = "Чек-ордер на карту #{0}"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBarcode
        '
        Me.txtBarcode.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtBarcode.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.txtBarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBarcode.Font = New System.Drawing.Font("Modern H EcoLight", 14.0!)
        Me.txtBarcode.ForeColor = System.Drawing.Color.White
        Me.txtBarcode.Location = New System.Drawing.Point(245, 62)
        Me.txtBarcode.MaxLength = 32
        Me.txtBarcode.Name = "txtBarcode"
        Me.txtBarcode.Size = New System.Drawing.Size(238, 30)
        Me.txtBarcode.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Modern H Medium", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 28)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "К оплате:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(18, Byte), Integer), CType(CType(22, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.lblOverallPrice)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.lblResize)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 505)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(602, 80)
        Me.Panel1.TabIndex = 20
        '
        'Button1
        '
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(26, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Modern H Medium", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button1.Location = New System.Drawing.Point(414, 14)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(159, 52)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "Оплачено!"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblOverallPrice
        '
        Me.lblOverallPrice.AutoSize = True
        Me.lblOverallPrice.Font = New System.Drawing.Font("Modern H EcoLight", 18.0!)
        Me.lblOverallPrice.Location = New System.Drawing.Point(150, 26)
        Me.lblOverallPrice.Name = "lblOverallPrice"
        Me.lblOverallPrice.Size = New System.Drawing.Size(71, 28)
        Me.lblOverallPrice.TabIndex = 19
        Me.lblOverallPrice.Text = "0 руб"
        '
        'lblResize
        '
        Me.lblResize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblResize.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(28, Byte), Integer))
        Me.lblResize.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.lblResize.Location = New System.Drawing.Point(573, 66)
        Me.lblResize.Name = "lblResize"
        Me.lblResize.Size = New System.Drawing.Size(36, 26)
        Me.lblResize.TabIndex = 1001
        '
        'flowPurchases
        '
        Me.flowPurchases.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.flowPurchases.AutoScroll = True
        Me.flowPurchases.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flowPurchases.Location = New System.Drawing.Point(20, 117)
        Me.flowPurchases.Name = "flowPurchases"
        Me.flowPurchases.Padding = New System.Windows.Forms.Padding(0, 8, 0, 0)
        Me.flowPurchases.Size = New System.Drawing.Size(563, 447)
        Me.flowPurchases.TabIndex = 21
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Modern H Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label4.Location = New System.Drawing.Point(114, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 24)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Штрих-код:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(559, -1)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(43, 45)
        Me.clos.TabIndex = 1000
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'cart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 23.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(602, 585)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txtBarcode)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.flowPurchases)
        Me.Font = New System.Drawing.Font("Modern H Medium", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.MinimumSize = New System.Drawing.Size(590, 420)
        Me.Name = "cart"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ID0 :: Чек-ордер"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents txtBarcode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblOverallPrice As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents flowPurchases As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents lblResize As System.Windows.Forms.Label
End Class
