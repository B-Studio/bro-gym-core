﻿Public Class add_anon_visit

    Private DefText$ = "Посетитель неизвестен и сейчас {0}." & Chr(10) & Chr(13) &
                       "В базу будет записано {1}{2}."

    Private VisTypes As New Dictionary(Of UserX.TicketType, String) From {
        {UserX.TicketType.Unlimited, "посещение по безлимитному абонементу"},
        {UserX.TicketType.DayTime, "посещение по дневному абонементу"},
        {UserX.TicketType.OneDayLight, "разовое дневное посещение"},
        {UserX.TicketType.OneDayUnlim, "разовое вечернее посещение"},
        {UserX.TicketType.Undefined, "(?)"}}
    Private TType As UserX.TicketType

    Private Sub add_anon_visit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DragByAControl.AttachMove(lblCaption)
        Goods.Load()
        If Now.Hour < 17 Then
            TType = UserX.TicketType.OneDayLight
            lblOnePay.Text = "       записать дневное посещение (" & Goods.ById(2).Price & " руб)"
        Else
            TType = UserX.TicketType.OneDayUnlim
            lblOnePay.Text = "       записать разовое посещение (" & Goods.ById(1).Price & " руб)"
        End If
        lblPers.Text = "       записать персональную тренировку (" & Goods.ById(5).Price & " руб)"

        lblOnePay_Click(lblOnePay, Nothing)
    End Sub

    Private Sub btnCommit_Click(sender As Object, e As EventArgs) Handles btnCommit.Click
        If onePay Then
            If TType = UserX.TicketType.OneDayLight Then
                SQL.Special.AddPurchase(UserX.AnonymousID, Barcodes.OneDaylightVisit, 1, txtComment.Text)
            Else : SQL.Special.AddPurchase(UserX.AnonymousID, Barcodes.OneUnlimVisit, 1, txtComment.Text)
            End If
        End If
        If onePers Then
            TType += 100
            SQL.Special.AddPurchase(0, Barcodes.OnePersTrain, 1, txtComment.Text & " (pers train)")
        End If
        SQL.Special.AddVisit(UserX.AnonymousID, TType, txtComment.Text)

        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub ' 

    Private Function OverallPrice() As UInt16
        Dim prc As UInt16 = 0
        If onePers Then prc += Goods.ById(Barcodes.OnePersTrain).Price
        If onePay Then
            If Now.Hour < 17 Then prc += Goods.ById(Barcodes.OneDaylightVisit).Price _
                             Else prc += Goods.ById(Barcodes.OneUnlimVisit).Price
        End If
        Return prc
    End Function
    Private Sub RefText()
        Dim andPay$ = " и оплата " & OverallPrice() & " руб"
        If OverallPrice() = 0 Then andPay = String.Empty
        lblText.Text = String.Format(DefText, Now.ToString("HH:mm"), VisTypes(TType), andPay)
    End Sub

    Public onePers, onePay As Boolean
    Private Sub lblOnePay_Click(sender As Object, e As EventArgs) Handles lblOnePay.Click, lblPers.Click
        If sender Is lblOnePay Then
            If onePay Then
                lblOnePay.Image = My.Resources.checkbox
                If TType = UserX.TicketType.OneDayLight Then _
                   TType = UserX.TicketType.DayTime _
              Else TType = UserX.TicketType.Unlimited
            Else
                lblOnePay.Image = My.Resources.checkbox_checked
                If Now.Hour < 17 Then TType = UserX.TicketType.OneDayLight _
                                 Else TType = UserX.TicketType.OneDayUnlim
            End If
            onePay = Not onePay
        Else
            If onePers Then
                lblPers.Image = My.Resources.checkbox
            Else : lblPers.Image = My.Resources.checkbox_checked
            End If
            onePers = Not onePers
        End If
        Call RefText()
    End Sub

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub
    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter
        sender.ForeColor = Color.Red
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave
        sender.ForeColor = Color.Maroon
    End Sub
End Class