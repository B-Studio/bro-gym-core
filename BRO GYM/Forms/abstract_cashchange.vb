﻿Public Class abstract_cashchange

    Private Dialog As DialogService = Service(Of DialogService)()

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub

    Private Sub abstract_cashchange_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DragByAControl.AttachMove(lblCaption)
        lblCurrent_Click(Nothing, Nothing)
        N_ValueChanged(Nothing, Nothing)
    End Sub

    Dim crnt% = 0
    Private Sub lblCurrent_Click(sender As Object, e As EventArgs) Handles lblCurrent.Click
        lblCurrent.Hide()
        lblCurrent.Refresh()
        crnt = SQL.Special.GetLastCashState()
        NumericUpDown1.Minimum = -crnt
        NumericUpDown1.Value = 0
        lblCurrent.Text = crnt & " руб"
        lblCurrent.Show()
    End Sub

    Private Sub N_ValueChanged(sender As Object, e As EventArgs) Handles NumericUpDown1.ValueChanged
        Dim raised% = crnt + NumericUpDown1.Value
        If raised < 0 Then raised = 0
        If NumericUpDown1.Value = 0 Then
            btnCommit.ForeColor = Color.DimGray
            btnCommit.FlatAppearance.BorderColor = Color.Black
        Else
            btnCommit.ForeColor = Color.White
            btnCommit.FlatAppearance.BorderColor = Color.FromArgb(48, 0, 0)
        End If
        lblRaised.Text = String.Format("(будет {0} руб)", raised)
    End Sub

    Private Sub btnCommit_Click(sender As Object, e As EventArgs) Handles btnCommit.Click
        If btnCommit.ForeColor = Color.White AndAlso
           Dialog.Show("Занести оплату с суммой " & NumericUpDown1.Value & "руб?", "Всё верно?",
                        MessageBoxIcon.Information, MessageBoxButtons.YesNo) = DialogResult.Yes Then
            SQL.Special.AddAbstract(NumericUpDown1.Value, txtComment.Text)
            abstract_cashchange_Load(Nothing, Nothing)
        End If
    End Sub

End Class