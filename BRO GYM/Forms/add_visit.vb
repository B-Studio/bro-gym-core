﻿Public Class add_visit

    Public Property USR As UserX

    Private DefText$ = "Посетитель - {0}." & Chr(10) & Chr(13) & "Сейчас {1}.{2}" &
                       "В базу будет записано {3}{4}."

    Private VisTypes As New Dictionary(Of UserX.TicketType, String) From {
        {UserX.TicketType.Unlimited, "посещение по безлимитному абонементу"},
        {UserX.TicketType.DayTime, "посещение по дневному абонементу"},
        {UserX.TicketType.OneDayLight, "разовое дневное посещение"},
        {UserX.TicketType.OneDayUnlim, "разовое вечернее посещение"},
        {UserX.TicketType.Undefined, "(?)"}}

    Private tType As UserX.TicketType = UserX.TicketType.Undefined
    Private Sub FormLoaded(s As Object, e As EventArgs) Handles MyBase.Load
        DragByAControl.AttachMove(lblCaption)
        If Me.USR IsNot Nothing Then
            If (Me.USR.TicketIsOK OrElse Me.USR.HaveOneDayTickets) Then
                Dim haventDayOneTick$ = Chr(10) & Chr(13)
                tType = Me.USR.TickType
                If tType = UserX.TicketType.Undefined OrElse
                    tType = UserX.TicketType.OneDayLight AndAlso Now.Hour >= 17 Then
                    tType = UserX.TicketType.OneDayUnlim
                    If Now.Hour < 17 Then
                        If Me.USR.Unused(Goods.ById(2)) > 0 Then tType = UserX.TicketType.OneDayLight _
                                                            Else haventDayOneTick = " Дневных посещений на карте нет. "
                    End If
                End If
                lblText.Text = String.Format(DefText, USR.FirstName & " " & USR.LastName, Now.ToString("HH:mm"), haventDayOneTick, VisTypes(tType), String.Empty)
                lblPers.Visible = Me.USR.Have.ContainsKey(Goods.ById(5).Atom) AndAlso Me.USR.Unused(Goods.ById(5)) > 0
                If lblPers.Visible Then
                    lblPers.Image = My.Resources.checkbox_checked
                    persChecked = True
                End If
                Exit Sub
            End If
        End If
        clos_Click(Nothing, Nothing)
    End Sub

    Private Sub btnCommit_Click(sender As Object, e As EventArgs) Handles btnCommit.Click
        If Not USR.TicketIsOK AndAlso USR.HaveOneDayTickets Then
            If tType = UserX.TicketType.OneDayUnlim Then _
                 Me.USR.Unused(Goods.ById(1)) -= 1 _
            Else Me.USR.Unused(Goods.ById(2)) -= 1
            SQL.Special.AddTicket(USR.ID, Now, tType)
        End If
        If lblPers.Visible AndAlso persChecked Then
            Me.USR.Unused(Goods.ById(5)) -= 1
            tType += 100
        End If
        SQL.Special.AddVisit(Me.USR.ID, tType, txtComment.Text)
        clos_Click(Nothing, Nothing)
    End Sub

    Private Sub FormShown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Me.Activate()
        SetForegroundWindow(Me.Handle)
    End Sub

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub
    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter
        sender.ForeColor = Color.Red
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave
        sender.ForeColor = Color.Maroon
    End Sub

    Private persChecked As Boolean = True
    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles lblPers.Click
        If persChecked Then lblPers.Image = My.Resources.checkbox _
                        Else lblPers.Image = My.Resources.checkbox_checked
        persChecked = Not persChecked
    End Sub

End Class