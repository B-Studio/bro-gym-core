﻿Public Class cart

    Public Property USR As UserX
    Private PurchCache As New Dictionary(Of UInt16, Purchase)
    Private Shared Dialog As DialogService = Service(Of DialogService)()

    Private Class Purchase
        Public Event PriceChanged As Action
        Public Event Disposed As Action(Of Purchase)
        Public IsInProcessing As Boolean = False
        Private _product As Product
        Public ReadOnly Property Product As Product
            Get
                Return Me._product
            End Get
        End Property

        Private _tocard As Int16
        Public Property ToCard As Int16
            Get
                Return Me._tocard
            End Get
            Set(value As Int16)
                Me._tocard = value
                TotalBuyRef()
            End Set
        End Property
        Private _touser As Int16
        Public Property ToUser As Int16
            Get
                Return Me._touser
            End Get
            Set(value As Int16)
                Me._touser = value
                TotalBuyRef()
            End Set
        End Property
        Private Property UserHave As Int16

        Public Sub Increment()
            If numToUserPurch.Visible AndAlso (numToUserPurch.Value < 1 OrElse Not numToCardPurch.Visible) Then
                If numToUserPurch.Value < numToUserPurch.Maximum Then numToUserPurch.Value += 1
            Else : If numToCardPurch.Value < numToCardPurch.Maximum Then numToCardPurch.Value += 1
            End If
        End Sub

        Public ReadOnly Property TotalBuy As Int16
            Get
                Dim res = Me.ToCard
                If Me.ToUser - Me.UserHave > 0 Then res += Me.ToUser * Me.Product.AtomCount - Me.UserHave
                If Me._product.Atom.IsStackable AndAlso res > Me._product.Atom.InStockCached Then
                    lblPriceResultPurch.ForeColor = Color.Red
                Else : lblPriceResultPurch.ForeColor = Color.White
                End If
                Return res
            End Get
        End Property
        Private Sub TotalBuyRef()
            If Me._touser > 0 OrElse Me._tocard > 0 Then
                If Me.Product.Id < 999 Then
                    Me.lblPriceResultPurch.Text = String.Format("{0}*{1} руб", Me.TotalBuy, Me._product.Price)
                End If
            Else
                Me.lblPriceResultPurch.Text = "0 руб"

                If Not IsInProcessing AndAlso Dialog.Show(
                    "Вы уверены, что хотите удалить товар" & Chr(13) & Me._product.Name & " ?",
                    "Удалить ???",
                    MessageBoxIcon.Error,
                    MessageBoxButtons.YesNo) = DialogResult.No Then

                    If numToUserPurch.Visible Then numToUserPurch.Value = 1 Else numToCardPurch.Value = 1

                Else
                    Me.panPurch.Hide()
                    For Each C As Control In Me.panPurch.Controls
                        C.Dispose() : Next
                    Me.panPurch.Dispose()
                    If Not IsInProcessing Then RaiseEvent Disposed(Me)
                End If
            End If
            RaiseEvent PriceChanged()
        End Sub

        Public ReadOnly Property OverallPrice As UInt16
            Get
                Return TotalBuy * Me.Product.Price
            End Get
        End Property

        Public Shared Function Create(GoodId As UInt16, User As UserX) As Purchase
            If User.IsAnonymous AndAlso GoodId = Barcodes.Abstract Then
                Return Nothing
            End If
            If Goods.HaveId(GoodId) Then
                Return New Purchase(GoodId) With {.UserHave = User.Unused(Goods.ById(GoodId))}
            Else : Return Nothing
            End If
        End Function

        Private Sub New(GoodId As UInt16)
            Me._product = Goods.Dict(GoodId)
        End Sub

        Private panPurch As New Panel(),
                lblOnCardPurch As New Label(),
                lblToUserPurch As New Label(),
                lblToCardPurch As New Label(),
                lblInStockPurch As New Label(),
                lblGoodNamePurch As New Label(),
                lblPriceResultPurch As New Label()
        Public numToUserPurch As New NumericUpDown(),
               numToCardPurch As New NumericUpDown(),
               lblNumPurch As New Label()
        Public Sub Draw(Canvas As cart, Num As Byte)
            '--------- panPurch
            Canvas.flowPurchases.Controls.Add(panPurch)
            panPurch.Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right
            panPurch.BackColor = Color.FromArgb(20, 17, 20)
            panPurch.Controls.Add(numToUserPurch)
            panPurch.Controls.Add(numToCardPurch)
            panPurch.Controls.Add(lblOnCardPurch)
            panPurch.Controls.Add(lblToUserPurch)
            panPurch.Controls.Add(lblToCardPurch)
            panPurch.Controls.Add(lblInStockPurch)
            panPurch.Controls.Add(lblGoodNamePurch)
            panPurch.Controls.Add(lblPriceResultPurch)
            panPurch.Controls.Add(lblNumPurch)
            panPurch.Location = New Point(0, 0)
            panPurch.Margin = New Padding(0, 0, 0, 8)
            panPurch.Padding = New Padding(10, 10, 5, 5)
            panPurch.Size = New Size(540, 115)
            panPurch.Name = "panPurch" & Num
            '--------- lblGoodNamePurch
            lblNumPurch.Name = "lblNumPurch" & Num
            lblNumPurch.Anchor = AnchorStyles.Top Or AnchorStyles.Bottom Or AnchorStyles.Left Or AnchorStyles.Right
            lblNumPurch.Font = New Font("Modern H Medium", 15.75!)
            lblNumPurch.Location = New Point(10, 9)
            lblNumPurch.Size = New Size(27, 49)
            lblNumPurch.TextAlign = ContentAlignment.TopLeft
            lblNumPurch.Text = Num & "."
            '--------- lblGoodNamePurch
            lblGoodNamePurch.Name = "lblGoodNamePurch" & Num
            lblGoodNamePurch.Anchor = AnchorStyles.Top Or AnchorStyles.Bottom Or AnchorStyles.Left Or AnchorStyles.Right
            lblGoodNamePurch.Font = New Font("Modern H EcoLight", 15.75!)
            lblGoodNamePurch.Location = New Point(47, 10)
            lblGoodNamePurch.Size = New Size(303, 49)
            lblGoodNamePurch.Text = Me._product.Name
            lblGoodNamePurch.SendToBack()
            '--------- lblInStockPurch
            lblInStockPurch.Name = "lblInStockPurch" & Num
            lblInStockPurch.Anchor = AnchorStyles.Top Or AnchorStyles.Right
            lblInStockPurch.BackColor = Color.Transparent
            lblInStockPurch.Font = New Font("Modern H EcoLight", 15.75!)
            lblInStockPurch.ForeColor = Color.DimGray
            lblInStockPurch.Location = New Point(356, 10)
            lblInStockPurch.Size = New Size(176, 24)
            lblInStockPurch.Text = "на складе: " & Me._product.Atom.InStockCached
            lblInStockPurch.TextAlign = ContentAlignment.MiddleRight
            '--------- lblPriceResultPurch
            lblPriceResultPurch.Name = "lblPriceResultPurch" & Num
            lblPriceResultPurch.Anchor = (AnchorStyles.Bottom Or AnchorStyles.Right)
            lblPriceResultPurch.BackColor = Color.Transparent
            lblPriceResultPurch.Font = New Font("Modern H Medium", 22.0!)
            lblPriceResultPurch.Location = New Point(360, 66)
            lblPriceResultPurch.Size = New Size(172, 44)
            lblPriceResultPurch.Text = String.Format("{0}×{1} руб", Me.TotalBuy, Me._product.Price)
            lblPriceResultPurch.TextAlign = ContentAlignment.MiddleRight
            '--------- lblOnCardPurch
            lblOnCardPurch.Name = "lblOnCardPurch" & Num
            lblOnCardPurch.Anchor = (AnchorStyles.Top Or AnchorStyles.Right)
            lblOnCardPurch.BackColor = Color.Transparent
            lblOnCardPurch.Font = New Font("Modern H EcoLight", 14.25!, FontStyle.Regular, GraphicsUnit.Point, 204)
            lblOnCardPurch.ForeColor = Color.DarkGray
            lblOnCardPurch.Location = New Point(332, 42)
            lblOnCardPurch.Size = New Size(200, 24)
            lblOnCardPurch.Text = "сейчас на карте: "
            If Canvas.USR.Have.ContainsKey(Me._product.Atom) Then
                lblOnCardPurch.Text &= Canvas.USR.Unused(Me._product)
            Else : lblOnCardPurch.Text &= 0
            End If
            lblOnCardPurch.TextAlign = ContentAlignment.MiddleRight
            '--------- lblToCartPurch
            lblToCardPurch.Name = "lblToCardPurch" & Num
            lblToCardPurch.Anchor = (AnchorStyles.Bottom Or AnchorStyles.Left)
            lblToCardPurch.Font = New Font("Modern H EcoLight", 14.25!, FontStyle.Regular, GraphicsUnit.Point, 204)
            lblToCardPurch.ForeColor = Color.DarkGray
            lblToCardPurch.Location = New Point(13, 79)
            lblToCardPurch.Size = New Size(81, 24)
            lblToCardPurch.Text = "в карту:"
            lblToCardPurch.TextAlign = ContentAlignment.MiddleLeft
            '--------- lblToUserPurch
            lblToUserPurch.Name = "lblToUserPurch" & Num
            lblToUserPurch.Anchor = (AnchorStyles.Bottom Or AnchorStyles.Left)
            lblToUserPurch.Font = New Font("Modern H EcoLight", 14.25!, FontStyle.Regular, GraphicsUnit.Point, 204)
            lblToUserPurch.ForeColor = Color.DarkGray
            lblToUserPurch.Location = New Point(161, 79)
            lblToUserPurch.Size = New Size(85, 24)
            lblToUserPurch.Text = "с собой:"
            lblToUserPurch.TextAlign = ContentAlignment.MiddleLeft
            '--------- numToCardPurch
            numToCardPurch.Name = "numToCardPurch" & Num
            numToCardPurch.Anchor = (AnchorStyles.Bottom Or AnchorStyles.Left)
            numToCardPurch.BackColor = Color.Black
            numToCardPurch.BorderStyle = BorderStyle.FixedSingle
            numToCardPurch.Font = New Font("Modern H Medium", 12.0!, FontStyle.Regular, GraphicsUnit.Point, 204)
            numToCardPurch.ForeColor = Color.White
            numToCardPurch.Location = New Point(91, 79)
            numToCardPurch.Margin = New Padding(0)
            numToCardPurch.Maximum = 99
            numToCardPurch.Size = New Size(59, 27)
            numToCardPurch.TextAlign = HorizontalAlignment.Center
            AddHandler numToCardPurch.ValueChanged, toCardNumCh
            AddHandler numToCardPurch.ValueChanged, Sub(s As Object, e As EventArgs)
                                                        Canvas.txtBarcode.Focus()
                                                        Canvas.txtBarcode.SelectAll()
                                                    End Sub
            numToCardPurch.Value = Me.ToCard
            '--------- numToUserPurch
            numToUserPurch.Name = "numToUserPurch" & Num
            numToUserPurch.Anchor = (AnchorStyles.Bottom Or AnchorStyles.Left)
            numToUserPurch.BackColor = Color.Black
            numToUserPurch.BorderStyle = BorderStyle.FixedSingle
            numToUserPurch.Font = New Font("Modern H Medium", 12.0!, FontStyle.Regular, GraphicsUnit.Point, 204)
            numToUserPurch.ForeColor = Color.White
            numToUserPurch.Location = New Point(242, 79)
            numToUserPurch.Margin = New Padding(0)
            numToUserPurch.Maximum = 99
            numToUserPurch.Size = New Size(59, 27)
            numToUserPurch.TextAlign = HorizontalAlignment.Center

            AddHandler numToUserPurch.ValueChanged, Me.toUserNumCh
            AddHandler numToUserPurch.ValueChanged, Sub(s As Object, e As EventArgs)
                                                        Canvas.txtBarcode.Focus()
                                                        Canvas.txtBarcode.SelectAll()
                                                    End Sub
            numToUserPurch.Value = Me.ToUser
            If Me.ToUser = 0 Then
                numToUserPurch.Hide()
                lblToUserPurch.Hide()
            End If
            If Me.Product.Name.Contains("Абонемент") Then
                numToCardPurch.Maximum = 1
            End If
            If Canvas.USR.IsAnonymous Then
                numToCardPurch.Value = 0
                numToUserPurch.Value = 1
                lblToUserPurch.Show()
                numToCardPurch.Hide()
                lblToCardPurch.Hide()
            End If
        End Sub

        Public toUserNumCh As EventHandler = Sub(o As Object, a As EventArgs) Me.ToUser = numToUserPurch.Value
        Public toCardNumCh As EventHandler = Sub(o As Object, a As EventArgs) Me.ToCard = numToCardPurch.Value
        Public Overrides Function Equals(obj As Object) As Boolean
            Return CType(obj, Purchase)._product.Id = Me._product.Id
        End Function
    End Class

    Private Sub cart_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DragByAControl.AttachMove(lblCaption)
        DragByAControl.AttachResize(lblResize)
        If My.Settings.cart_size.Height > 0 AndAlso
           My.Settings.cart_size.Width > 0 Then _
           Me.Size = My.Settings.cart_size
        If My.Settings.cart_location.X >= 0 AndAlso
           My.Settings.cart_location.Y >= 0 Then _
           Me.Location = My.Settings.cart_location
        lblCaption.Text = String.Format(lblCaption.Text, USR.ID)
        If Me.txtBarcode.Text Like "###" Then _
            txtBarcode_KeyUp(Nothing, New KeyEventArgs(Keys.Enter))
    End Sub

    Private purchCounter As Byte = 0
    Private Sub txtBarcode_KeyUp(sender As Object, e As KeyEventArgs) Handles txtBarcode.KeyUp
        If e.KeyCode = Keys.Enter AndAlso txtBarcode.Text Like "*###" Then
            e.SuppressKeyPress = True
            txtBarcode.Text = txtBarcode.Text.Remove(0, txtBarcode.TextLength - 3)
            Dim GoodId As UInt16 = CUShort(txtBarcode.Text)
            If Not Me.PurchCache.ContainsKey(GoodId) Then
                If Not USR.IsAnonymous Or GoodId > 100 Then
                    Dim Pu As Purchase = Purchase.Create(GoodId, USR)
                    If Pu IsNot Nothing Then
                        If Pu.Product.Atom.Id > 2 AndAlso
                           Pu.Product.Atom.Id < 9 Then
                            Pu.ToUser = 1
                            Pu.ToCard = 0
                        Else
                            Pu.ToCard = 1
                            Pu.ToUser = 0
                        End If
                        Me.PurchCache.Add(GoodId, Pu)
                        AddHandler Pu.PriceChanged, AddressOf lblRefOverallPrice
                        AddHandler Pu.Disposed, Sub(P As Purchase)
                                                    Me.PurchCache.Remove(P.Product.Id)
                                                    P = Nothing : GC.Collect()
                                                End Sub
                        purchCounter += 1
                        Pu.Draw(Me, purchCounter)
                        txtBarcode.Text = String.Empty
                    Else : txtBarcode.Text = "Не найдено!"
                    End If
                Else : txtBarcode.Text = "Не разрешено!"
                End If
                txtBarcode.Focus()
                txtBarcode.SelectAll()
            Else : Me.PurchCache(GoodId).Increment()
            End If
        End If
    End Sub

    Private Sub lblRefOverallPrice()
        Dim prc As UInt16 = 0, i As UInt16 = 1
        For Each P As Purchase In PurchCache.Values
            prc += P.OverallPrice
            P.lblNumPurch.Text = String.Format("{0}.", i) : i += 1
        Next
        lblOverallPrice.Text = String.Format("{0} руб", prc)
    End Sub

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        If PurchCache.Count > 0 Then
            If Dialog.Show(
                "В чеке присутствуют товары !" & Chr(13) & "При закрытии они пропадут." & Chr(13) & "Точно закрыть ?",
                "Непустой чек!",
                MessageBoxIcon.Error,
                MessageBoxButtons.YesNo
            ) = DialogResult.No Then Exit Sub
        End If
        DragByAControl.DeattachMove(lblCaption)
        For Each Pu As Purchase In Me.PurchCache.Values
            Pu.IsInProcessing = True
            Pu.ToUser = 0 : Pu.ToCard = 0
        Next
        PurchCache.Clear()
        GC.Collect()
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub
    Private Sub MeFormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        My.Settings.cart_size = Me.Size
        My.Settings.cart_location = Me.Location
        My.Settings.Save()
    End Sub
    Private Sub txtBarcode_GotFocus(s As Object, e As EventArgs) Handles txtBarcode.GotFocus
        txtBarcode.SelectAll()
    End Sub
    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter
        sender.ForeColor = Color.Red
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave
        sender.ForeColor = Color.Maroon
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If PurchCache.Count > 0 Then
            If (lblOverallPrice.Text <> "0 руб" AndAlso
                Dialog.Show("Ты получил эти денежки (" & lblOverallPrice.Text & ") в руки ?" & Chr(13) &
                            "Операции оплаты не так-то просто отменить...",
                            "Уверен ?", MessageBoxIcon.Information, MessageBoxButtons.YesNo) = DialogResult.No) _
                Then Exit Sub
            If (lblOverallPrice.Text = "0 руб" AndAlso Not USR.IsAnonymous AndAlso
                Dialog.Show("У пользователя будут списаны товары с карты, он знает об этом? =D", "Человек-то в курсе?",
                                MessageBoxIcon.Question, MessageBoxButtons.YesNo) = DialogResult.No) Then Exit Sub
            Dim price As UInt32 = 0
            For Each Pu As Purchase In Me.PurchCache.Values
                RemoveHandler Pu.numToCardPurch.ValueChanged, Pu.toCardNumCh
                RemoveHandler Pu.numToUserPurch.ValueChanged, Pu.toUserNumCh
                Pu.IsInProcessing = True
                If Not Pu.Product.Name.Contains("Абонемент") Then
                    If Pu.TotalBuy > 0 Then SQL.Special.AddPurchase(Me.USR.ID, Pu.Product.Id, Pu.TotalBuy)
                    If Not USR.IsAnonymous Then
                        Dim NewUnused As UInt16 = USR.Unused(Pu.Product)
                        SubstractAtoms(Balance:=NewUnused, Fine:=Pu.ToUser * Pu.Product.AtomCount)
                        USR.Unused(Pu.Product) = NewUnused + Pu.ToCard * Pu.Product.AtomCount
                    End If
                    If Pu.Product.Atom.IsStackable Then _
                        Pu.Product.Atom.InStock = Pu.Product.Atom.InStockCached - (Pu.ToUser + Pu.ToCard) * Pu.Product.AtomCount
                    Pu.ToUser = 0 : Pu.ToCard = 0
                Else ' TICKET BUYING '
                    Dim tType As UserX.TicketType = UserX.TicketType.Unlimited
                    If Pu.Product.Id = 3 Then tType = UserX.TicketType.DayTime
                    Dim StartsNewTick As Date
                    Me.USR.UpdateTicketInfo()
                    If Me.USR.TickType > UserX.TicketType.Undefined Then
                        If Me.USR.TickType < UserX.TicketType.OneDayLight AndAlso
                            Dialog.Show("У пользователя уже обнаружен существующий абонемент. " &
                                            "Если добавить новый, то его срок действия начнётся " &
                                            UserX.ExpiresAfterRu(Me.USR.Expires) & ". Добавить?",
                                            "Уже есть абонемент",
                                            MessageBoxIcon.Question, MessageBoxButtons.YesNo) _
                                            = Windows.Forms.DialogResult.No Then
                            Pu.ToCard = 0 : Pu.ToUser = 0
                            Continue For
                        End If
                        StartsNewTick = Me.USR.Expires.AddDays(1)
                    Else : StartsNewTick = Now
                    End If
                    SQL.Special.AddTicket(Me.USR.ID, StartsNewTick, tType)
                    SQL.Special.AddPurchase(Me.USR.ID, Pu.Product.Id, 1)
                    Pu.ToCard = 0 : Pu.ToUser = 0
                End If
            Next
            PurchCache.Clear()
            txtBarcode.Text = String.Empty
        End If
        GC.Collect()
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub

    Private Sub SubstractAtoms(ByRef Balance As Int64, ByRef Fine As Int64)
        If Balance > Fine Then
            Balance -= Fine
            Fine = 0
        Else
            Fine -= Balance
            Balance = 0
        End If
    End Sub
End Class