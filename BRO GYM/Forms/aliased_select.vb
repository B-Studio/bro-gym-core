﻿Public Class aliased_select

    Public Shared LastSelected As UInt32 = 0
    Public Property aliasedID As UInt32 = 0
    Public Property list As List(Of UInt32)

    Private Sub aliased_select_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DragByAControl.AttachMove(lblCaption)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        LastSelected = CUInt(CType(sender, Button).Text)
        Me.Close()
    End Sub

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub

    Private Sub aliased_select_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        If list IsNot Nothing AndAlso list.Count > 0 Then
            For x As Byte = 1 To list.Count
                Dim btn As Button = CType(Panel1.Controls("Button" & x), Button)
                btn.Text = list(x - 1).ToString("0000")
                btn.Visible = True
                AddHandler btn.Click, AddressOf Button1_Click
                If x = 9 Then Exit For
            Next
        End If
        lblText.Text = "Обнаружено несколько карт, соответствующих штрих-коду #" & aliasedID.ToString("0000") & ", пожалуйста, посмотрите номер на фронтальной стороне карты и выберите нужный"
    End Sub

End Class