﻿Imports MySql.Data.MySqlClient

Public Class custom_queries

    Private Shared Shadows resizeEnd As Size
    Private Sub custom_queries_Load(sender As Object, e As EventArgs) Handles MyBase.Shown
        DragByAControl.AttachMove(ToControl:=lblCaption)
        DragByAControl.AttachResize(ToControl:=lblResize)
        Theme.AttachOpacityFeatures(Me)
        Theme.Load()
        Theme.Apply(Me, States.Normal)
        Me.BackColor = Color.FromArgb(16, 16, 16)
        If My.Settings.custom_queries_size.Height > 0 AndAlso
           My.Settings.custom_queries_size.Width > 0 Then _
           Me.Size = My.Settings.custom_queries_size
        If My.Settings.custom_queries_location.X >= 0 AndAlso
           My.Settings.custom_queries_location.Y >= 0 Then _
           Me.Location = My.Settings.custom_queries_location
    End Sub

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Close()
    End Sub

    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter, mini.MouseEnter
        If sender Is Me.clos Then _
        sender.ForeColor = Color.Red _
        Else sender.ForeColor = Color.Yellow
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave, mini.MouseLeave
        If sender Is Me.clos Then _
        sender.ForeColor = Color.Maroon _
        Else sender.ForeColor = Color.Olive
    End Sub
    Private Sub Buttonx_MouseEnter(sender As Object, e As EventArgs) Handles exec.MouseEnter
        sender.ForeColor = Color.Lime
    End Sub
    Private Sub Buttonx_MouseLeave(sender As Object, e As EventArgs) Handles exec.MouseLeave
        sender.ForeColor = Color.Green
    End Sub

    Private Sub exec_Click(sender As Object, e As EventArgs) Handles exec.Click
        Dim query_TextBuffer$ = txtQuery.Text.Trim()
        txtQuery.Text = "Подожди ..."
        txtQuery.Enabled = False
        Dim resp As SQL.Response = SQL.Query(query_TextBuffer)

        Select Case resp.KindIs
            Case SQL.Response.ResponseKind.Table
                lstQueryResult.ListViewItemSorter = Nothing
                lstQueryResult.Clear()
                Dim g As Graphics = Graphics.FromImage(New Bitmap(Me.Width, Me.Height))
                Dim col_widths(resp.Table.Width) As UInt16
                Dim refWid = Sub(ByRef wid, nwid) If nwid > wid Then wid = nwid

                For i% = 0 To resp.Table.Width - 1
                    Dim gname$ = SQL.RusColName(resp.Table.Columns(i))
                    lstQueryResult.Columns.Add(gname)
                    refWid(col_widths(i), g.MeasureString(gname, lstQueryResult.Font).Width + 10)
                Next
                For Each row As Row In resp.Table
                    Dim strs(resp.Table.Width) As String
                    For i% = 0 To resp.Table.Width - 1
                        If Not IsDBNull(row(CByte(i))) AndAlso row(CByte(i)) IsNot Nothing Then _
                            strs(i) = row(CByte(i)).ToString() Else strs(i) = "(null)"
                        refWid(col_widths(i), g.MeasureString(strs(i), lstQueryResult.Font).Width + 10)
                    Next
                    lstQueryResult.Items.Add(New ListViewItem(strs))
                Next
                For i% = 0 To resp.Table.Width - 1
                    lstQueryResult.Columns(i).Width = col_widths(i)
                Next
                sortCol = -1
            Case SQL.Response.ResponseKind.Excep
                MessageBox.Show(resp.Excep.GetType().ToString() & ": " & resp.Excep.Message,
                                "Ошибка в результате запроса", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Case SQL.Response.ResponseKind.Changed
                MessageBox.Show("Ни одной строки не получено!" & Chr(13) &
                                "В результате запроса изменено строк: " & resp.Changed, " ",
                                MessageBoxButtons.OK, MessageBoxIcon.Information)
            Case SQL.Response.ResponseKind.Empty
                MessageBox.Show("Ни одной строки не получено и не изменено в результате запроса!", " ",
                                MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Select
        If resp.Changed > 0 Then
            fldAffected.Show()
            fldAffected.Text = "изменено строк - " & resp.Changed
            If query_TextBuffer.Contains("users") Then _
                UserX.LoadRegistred()
        Else : fldAffected.Hide()
        End If
        txtQuery.Enabled = True
        txtQuery.Text = query_TextBuffer.Trim()
        txtQuery.Focus()
        txtQuery.DeselectAll()
        txtQuery.SelectionStart = txtQuery.TextLength
    End Sub

    Private scripts_dir$ = Application.StartupPath & "\scripts"
    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles txtQuery.KeyUp
        If e.KeyCode = Keys.Enter Then
            If e.Control Then
                exec_Click(sender, Nothing) : e.Handled = False
            Else
                Dim scr$ = txtQuery.Text.Trim.ToLower.Replace("ыскшзе", "script")
                If Regex.Check(scr, "script\d{1,}") Then
                    If IO.Directory.Exists(scripts_dir$) Then
                        If IO.File.Exists(String.Format("{0}\{1}.ini", scripts_dir, scr)) Then
                            Dim script$ = String.Empty
                            Using SR As New IO.StreamReader(String.Format("{0}\{1}.ini", scripts_dir, scr), System.Text.Encoding.UTF8)
                                If Not SR.EndOfStream Then SR.ReadLine()
                                Do While Not SR.EndOfStream
                                    script &= SR.ReadLine() & Chr(10) & Chr(13)
                                Loop
                            End Using
                            txtQuery.Text = script
                            exec_Click(sender, Nothing)
                            txtQuery.SelectAll()
                        End If
                    Else : IO.Directory.CreateDirectory(scripts_dir$)
                    End If
                End If
            End If
        End If
    End Sub

    Private sortCol% = -1
    Private Sub lstColClick(sender As Object, e As ColumnClickEventArgs) Handles lstQueryResult.ColumnClick
        If sortCol <> e.Column Then _
        lstQueryResult.ListViewItemSorter = New ListViewItemComparer(e.Column)
        Select Case lstQueryResult.Sorting
            Case SortOrder.Descending : lstQueryResult.Sorting = SortOrder.Ascending
            Case SortOrder.Ascending : lstQueryResult.Sorting = SortOrder.Descending
            Case Else : lstQueryResult.Sorting = SortOrder.Ascending
        End Select
        lstQueryResult.Sort()
        lstQueryResult.Refresh()
        sortCol = e.Column
    End Sub

    Private Sub FormShown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Me.Activate()
        SetForegroundWindow(Me.Handle)
        SetActiveWindow(Me.Handle)
    End Sub

    Private Sub MeFormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        My.Settings.custom_queries_size = Me.Size
        My.Settings.custom_queries_location = Me.Location
        My.Settings.Save()
    End Sub

    Private Class ListViewItemComparer
        Implements IComparer
        Private col%
        Public Sub New()
            col = 0
        End Sub
        Public Sub New(colNum%)
            col = colNum
        End Sub
        Public Function Compare(x As Object, y As Object) As Integer Implements IComparer.Compare
            Return String.Compare(CType(x, ListViewItem).SubItems(col).Text,
                                  CType(y, ListViewItem).SubItems(col).Text)
        End Function
    End Class

    Private Sub mini_Click(sender As Object, e As EventArgs) Handles mini.Click
        Me.txtQuery.Focus()
        Me.WindowState = FormWindowState.Minimized
    End Sub
End Class