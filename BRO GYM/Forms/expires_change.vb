﻿Public Class expires_change

    Public Property TicketID As UInt32
    Public Property UserID As UInt32
    Public Property ExpiresOld As Date

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub

    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter
        sender.ForeColor = Color.Red
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave
        sender.ForeColor = Color.Maroon
    End Sub
    Private Sub btnCommit_Click(sender As Object, e As EventArgs) Handles btnCommit.Click
        mtxBirthday_Unfocused(Nothing, Nothing)
        If mtxNewExpires.ForeColor = Color.Aqua AndAlso
           Service(Of DialogService).Show("Вы уверены, что хотите изменить дату окончания абонмента владельцу карты #" & UserID.ToString("0000") & Chr(10) & Chr(13) & "с " &
                        ExpiresOld.ToString("d MMMM") & " на " & ParseDate().ToString("d MMMM") & "?", "Дата окончания у #" & UserID.ToString("0000"),
                        MessageBoxIcon.Question, MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim p As Date = ParseDate()
            Dim addt$ = ", bought='" & p.ToString("yyyy-MM-dd") & " 00:00:00' "
            If ParseDate.Subtract(Now).TotalMilliseconds > 0 Then addt = ""
            SQL.Query("UPDATE tickets SET expires='{0}'{1} WHERE ticketid={2} AND buyer={3}", p.ToString("yyyy-MM-dd HH:mm:ss"),
                      addt, Me.TicketID, Me.UserID)
            clos_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub mtxBirthday_Unfocused(sender As Object, e As EventArgs) Handles mtxNewExpires.LostFocus,
        MyBase.Click, lblCaption.Click, lblExpiresOld.Click, Label1.Click, Label5.Click
        If TryParseDate() Then mtxNewExpires.ForeColor = Color.Aqua Else mtxNewExpires.ForeColor = Color.Red
    End Sub

    Private Function TryParseDate() As Boolean
        Try
            Dim d As Date = ParseDate()
            Return d.ToString("ddMMyy") <> ExpiresOld.ToString("ddMMyy") AndAlso
                   d.Subtract(Now).TotalDays < 65
        Catch : Return False
        End Try
    End Function
    Private Function ParseDate() As Date
        Try
            Dim dts() As String = mtxNewExpires.Text.Split(".")
            If dts(2).Length < 4 Then dts(2) = "2000".Remove(4 - dts(2).Length) & dts(2)
            Return New Date(dts(2), dts(1), dts(0), ExpiresOld.Hour, 0, 0)
        Catch ex As Exception : Throw ex
        End Try
    End Function

    Private Sub expires_change_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblExpiresOld.Text = Me.ExpiresOld.ToString("d MMMM")
        mtxNewExpires.Text = Me.ExpiresOld.ToString("ddMMyyyy")
        DragByAControl.AttachMove(lblCaption)
    End Sub

End Class