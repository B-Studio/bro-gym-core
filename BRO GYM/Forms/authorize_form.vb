﻿
Imports SWF = System.Windows.Forms
Imports System.Threading

Public Class authorize_form
    Private Dialog As DialogService = Service(Of DialogService)()

    Private Sub Form1_Closing(s As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        TRAY_BRO.Visible = False
    End Sub

    Private Sub FLoad(s As Object, e As EventArgs) Handles MyBase.Load
        AddHandler btnCommit.Click, AddressOf btnCommit_Click
        kUp(Nothing, New KeyEventArgs(Keys.D0))
        If IO.File.Exists(Application.StartupPath.Remove(3) & "blocked") Then BlockedMsg()
    End Sub

    Private trysIncor As SByte = 4, dialShown As Boolean = False
    Private Sub btnCommit_Click(sender As Object, e As EventArgs)
        SQL.Decrypted = txtSurname.Text
        If SQL.Decrypted <> Nothing Then
            txtSurname.Enabled = False
            Do
                Try : SQL.Connect() : Catch : End Try
                If SQL.Connected Then
                    UserX.LoadRegistred()
                    Atoms.Load()
                    Goods.Load()
                    Me.Opacity = 0
                    Me.WindowState = FormWindowState.Minimized
                    Me.Hide()
                    Dialog.Show("Подключение к базе данных BRO GYM успешно установлено. Система управления полностью готова к работе.",
                                 "Успешная аутентификация", MessageBoxIcon.Information, MessageBoxButtons.OK)
                    Service(Of BarcodeService).Start()
                    Exit Sub
                End If
            Loop While Dialog.Show("Не удалость установить подключение к базе данных. Возможно локальный сервер ещё не запущен. Повторить попытку?",
                                    "Нет связи с базой данных", MessageBoxIcon.Error, MessageBoxButtons.YesNo) _
                         <> DialogResult.No
            TRAY_BRO.Visible = False
            Process.GetCurrentProcess().Kill()
        Else
            If trysIncor > 1 Then
                trysIncor -= 1
                Dim s$ = "попытки" : If trysIncor = 1 Then s = "попытка"
                dialShown = True
                Dialog.Show("Неверно введён ключ безопасности!" & Chr(13) & "До полной блокировки системы" & Chr(13) & "у вас осталось " & trysIncor & " " & s & "!",
                             "Поаккуратнее на поворотах", MessageBoxIcon.Exclamation, MessageBoxButtons.OK)
            Else
                Try : IO.File.Create(Application.StartupPath.Remove(3) & "blocked").Close()
                Catch
                End Try
                Call BlockedMsg()
            End If
        End If
    End Sub
    Private Sub BlockedMsg()
        Dialog.Show("База данных BRO GYM была полностью уничтожена. Система управления сейчас заблокирована. " &
                     "Для разрешения этой ситуации свяжитесь с системным администратором.",
                     "Полная блокировка системы", MessageBoxIcon.Error, MessageBoxButtons.OK)
        TRAY_BRO.Visible = False
        Application.ExitThread()
    End Sub

    Private Sub btnCommit_MouseEnter(sender As Object, e As EventArgs) Handles btnCommit.MouseEnter
        btnCommit.ForeColor = Color.FromArgb(204, 204, 255)
    End Sub
    Private Sub btnCommit_MouseLeave(sender As Object, e As EventArgs) Handles btnCommit.MouseLeave
        btnCommit.ForeColor = Color.FromArgb(192, 192, 255)
    End Sub
    Private Sub btnPass_MouseEnter(sender As Object, e As EventArgs) Handles btnPass.MouseEnter
        btnPass.Image = My.Resources.eye_aq
    End Sub
    Private Sub btnPass_MouseLeave(sender As Object, e As EventArgs) Handles btnPass.MouseLeave
        btnPass.Image = My.Resources.eye_aq_hov
    End Sub
    Private Sub btnPass_MouseDown(sender As Object, e As MouseEventArgs) Handles btnPass.MouseDown
        If e.Button = MouseButtons.Left Then
            btnPass.Image = My.Resources.eye_aq_dow
            txtSurname.PasswordChar = Nothing
        End If
    End Sub
    Private Sub btnPass_MouseUp(sender As Object, e As MouseEventArgs) Handles btnPass.MouseUp
        If e.Button = MouseButtons.Left Then
            btnPass.Image = My.Resources.eye_aq
            txtSurname.PasswordChar = "$"c
        End If
    End Sub
    Private Sub kUp(sender As Object, e As KeyEventArgs) Handles btnCommit.KeyUp, btnPass.KeyUp, Button1.KeyUp, txtSurname.KeyUp
        If Not dialShown AndAlso e.KeyData = Keys.Enter Then : btnCommit_Click(Nothing, Nothing)
        Else
            If WinAPI.GetKeyboardLayout(0) = 1049 Then Button1.Text = "RU" Else Button1.Text = "EN"
            dialShown = False
        End If
    End Sub
    Private Sub TrayUp(sender As Object, e As MouseEventArgs) Handles TRAY_BRO.MouseUp
        If e.Button = MouseButtons.Left Then
            AppRouting.Show(AppForms.statistics)
        Else : AppRouting.Show(AppForms.custom_queries)
        End If
    End Sub



End Class
