﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class add_instock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(add_instock))
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.clos = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.numInStock = New System.Windows.Forms.NumericUpDown()
        Me.txtBarcode = New System.Windows.Forms.TextBox()
        Me.btnCommit = New System.Windows.Forms.Button()
        Me.lblNowStock = New System.Windows.Forms.Label()
        Me.lblChangeVal = New System.Windows.Forms.Label()
        CType(Me.numInStock, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(493, 44)
        Me.lblCaption.TabIndex = 1
        Me.lblCaption.Text = "Добавление товаров на склад"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Modern H Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label2.Location = New System.Drawing.Point(66, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Штрих-код:"
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(16, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(451, -1)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(43, 45)
        Me.clos.TabIndex = 999
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Modern H Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label3.Location = New System.Drawing.Point(58, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(133, 24)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Количество:"
        '
        'numInStock
        '
        Me.numInStock.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.numInStock.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.numInStock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.numInStock.Font = New System.Drawing.Font("Modern H EcoLight", 20.25!)
        Me.numInStock.ForeColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.numInStock.Location = New System.Drawing.Point(197, 134)
        Me.numInStock.Maximum = New Decimal(New Integer() {32767, 0, 0, 0})
        Me.numInStock.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.numInStock.Name = "numInStock"
        Me.numInStock.Size = New System.Drawing.Size(238, 39)
        Me.numInStock.TabIndex = 1
        Me.numInStock.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtBarcode
        '
        Me.txtBarcode.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtBarcode.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtBarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBarcode.Font = New System.Drawing.Font("Modern H EcoLight", 14.0!)
        Me.txtBarcode.ForeColor = System.Drawing.Color.Gainsboro
        Me.txtBarcode.Location = New System.Drawing.Point(197, 84)
        Me.txtBarcode.MaxLength = 32
        Me.txtBarcode.Name = "txtBarcode"
        Me.txtBarcode.Size = New System.Drawing.Size(238, 30)
        Me.txtBarcode.TabIndex = 0
        '
        'btnCommit
        '
        Me.btnCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCommit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCommit.Font = New System.Drawing.Font("Modern H Medium", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnCommit.Location = New System.Drawing.Point(125, 239)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(242, 55)
        Me.btnCommit.TabIndex = 2
        Me.btnCommit.Text = "Сохранить"
        Me.btnCommit.UseVisualStyleBackColor = True
        '
        'lblNowStock
        '
        Me.lblNowStock.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblNowStock.AutoSize = True
        Me.lblNowStock.Font = New System.Drawing.Font("Modern H EcoLight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblNowStock.ForeColor = System.Drawing.Color.Gray
        Me.lblNowStock.Location = New System.Drawing.Point(193, 176)
        Me.lblNowStock.Name = "lblNowStock"
        Me.lblNowStock.Size = New System.Drawing.Size(171, 19)
        Me.lblNowStock.TabIndex = 2
        Me.lblNowStock.Text = "сейчас на складе: (?)"
        '
        'lblChangeVal
        '
        Me.lblChangeVal.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblChangeVal.Font = New System.Drawing.Font("Modern H EcoLight", 13.0!)
        Me.lblChangeVal.ForeColor = System.Drawing.Color.DimGray
        Me.lblChangeVal.Location = New System.Drawing.Point(125, 297)
        Me.lblChangeVal.Name = "lblChangeVal"
        Me.lblChangeVal.Size = New System.Drawing.Size(242, 27)
        Me.lblChangeVal.TabIndex = 2
        Me.lblChangeVal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'add_instock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 23.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(493, 344)
        Me.Controls.Add(Me.btnCommit)
        Me.Controls.Add(Me.txtBarcode)
        Me.Controls.Add(Me.numInStock)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.lblNowStock)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.lblChangeVal)
        Me.Font = New System.Drawing.Font("Modern H Medium", 14.25!)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "add_instock"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "add_instock"
        CType(Me.numInStock, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents numInStock As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtBarcode As System.Windows.Forms.TextBox
    Friend WithEvents btnCommit As System.Windows.Forms.Button
    Friend WithEvents lblNowStock As System.Windows.Forms.Label
    Friend WithEvents lblChangeVal As System.Windows.Forms.Label
End Class
