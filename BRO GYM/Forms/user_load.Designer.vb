﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class user_load
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(user_load))
        Me.clos = New System.Windows.Forms.Button()
        Me.fldID = New System.Windows.Forms.Label()
        Me.lblBirthday = New System.Windows.Forms.Label()
        Me.fldFirstLastName = New System.Windows.Forms.Label()
        Me.fldSurname = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.fldBirthday = New System.Windows.Forms.Label()
        Me.lblRegistred = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.flowVariables = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblTicketExpAt = New System.Windows.Forms.Label()
        Me.fldTicketExpires = New System.Windows.Forms.Label()
        Me.fldEndsAt = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.fldYearsOld = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.fldRegistred = New System.Windows.Forms.Label()
        Me.fldRegistredOld = New System.Windows.Forms.Label()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.brnCart = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.btnWriteVisit = New System.Windows.Forms.Button()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.lblNotVisited = New System.Windows.Forms.Label()
        Me.mini = New System.Windows.Forms.Button()
        Me.lblResize = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(1209, -10)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(62, 47)
        Me.clos.TabIndex = 7
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'fldID
        '
        Me.fldID.BackColor = System.Drawing.Color.Transparent
        Me.fldID.Font = New System.Drawing.Font("OCR A Extended", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fldID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(96, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.fldID.Location = New System.Drawing.Point(39, 325)
        Me.fldID.Margin = New System.Windows.Forms.Padding(0)
        Me.fldID.Name = "fldID"
        Me.fldID.Size = New System.Drawing.Size(290, 53)
        Me.fldID.TabIndex = 17
        Me.fldID.Text = "000000"
        Me.fldID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.fldID.UseCompatibleTextRendering = True
        '
        'lblBirthday
        '
        Me.lblBirthday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBirthday.AutoSize = True
        Me.lblBirthday.BackColor = System.Drawing.Color.Transparent
        Me.lblBirthday.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblBirthday.Location = New System.Drawing.Point(0, 0)
        Me.lblBirthday.Margin = New System.Windows.Forms.Padding(0)
        Me.lblBirthday.Name = "lblBirthday"
        Me.lblBirthday.Size = New System.Drawing.Size(101, 27)
        Me.lblBirthday.TabIndex = 21
        Me.lblBirthday.Text = "Родился:"
        Me.lblBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBirthday.UseCompatibleTextRendering = True
        '
        'fldFirstLastName
        '
        Me.fldFirstLastName.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldFirstLastName.AutoSize = True
        Me.fldFirstLastName.BackColor = System.Drawing.Color.Transparent
        Me.fldFirstLastName.Font = New System.Drawing.Font("Modern H Medium", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldFirstLastName.Location = New System.Drawing.Point(0, 0)
        Me.fldFirstLastName.Margin = New System.Windows.Forms.Padding(0)
        Me.fldFirstLastName.Name = "fldFirstLastName"
        Me.fldFirstLastName.Size = New System.Drawing.Size(883, 43)
        Me.fldFirstLastName.TabIndex = 18
        Me.fldFirstLastName.Text = "Anonymous Bodybuilder"
        Me.fldFirstLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldFirstLastName.UseCompatibleTextRendering = True
        '
        'fldSurname
        '
        Me.fldSurname.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.fldSurname.AutoSize = True
        Me.fldSurname.BackColor = System.Drawing.Color.Transparent
        Me.fldSurname.Font = New System.Drawing.Font("Modern H EcoLight", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldSurname.Location = New System.Drawing.Point(0, 43)
        Me.fldSurname.Margin = New System.Windows.Forms.Padding(0)
        Me.fldSurname.Name = "fldSurname"
        Me.fldSurname.Size = New System.Drawing.Size(188, 34)
        Me.fldSurname.TabIndex = 19
        Me.fldSurname.Text = "Anonymouvich"
        Me.fldSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldSurname.UseCompatibleTextRendering = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(39, 51)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(290, 271)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 15
        Me.PictureBox1.TabStop = False
        '
        'fldBirthday
        '
        Me.fldBirthday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.fldBirthday.AutoSize = True
        Me.fldBirthday.BackColor = System.Drawing.Color.Transparent
        Me.fldBirthday.Font = New System.Drawing.Font("Modern H Medium", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldBirthday.Location = New System.Drawing.Point(101, 0)
        Me.fldBirthday.Margin = New System.Windows.Forms.Padding(0)
        Me.fldBirthday.Name = "fldBirthday"
        Me.fldBirthday.Size = New System.Drawing.Size(124, 27)
        Me.fldBirthday.TabIndex = 22
        Me.fldBirthday.Text = "00.00.0000"
        Me.fldBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldBirthday.UseCompatibleTextRendering = True
        '
        'lblRegistred
        '
        Me.lblRegistred.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblRegistred.AutoSize = True
        Me.lblRegistred.BackColor = System.Drawing.Color.Transparent
        Me.lblRegistred.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblRegistred.Location = New System.Drawing.Point(0, 2)
        Me.lblRegistred.Margin = New System.Windows.Forms.Padding(0)
        Me.lblRegistred.Name = "lblRegistred"
        Me.lblRegistred.Size = New System.Drawing.Size(183, 31)
        Me.lblRegistred.TabIndex = 21
        Me.lblRegistred.Text = "Появился в базе:"
        Me.lblRegistred.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblRegistred.UseCompatibleTextRendering = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.flowVariables, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.fldFirstLastName, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.fldSurname, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(375, 51)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(883, 508)
        Me.TableLayoutPanel1.TabIndex = 23
        '
        'flowVariables
        '
        Me.flowVariables.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flowVariables.AutoScroll = True
        Me.flowVariables.Location = New System.Drawing.Point(3, 243)
        Me.flowVariables.Name = "flowVariables"
        Me.flowVariables.Size = New System.Drawing.Size(877, 214)
        Me.flowVariables.TabIndex = 26
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblTicketExpAt)
        Me.FlowLayoutPanel3.Controls.Add(Me.fldTicketExpires)
        Me.FlowLayoutPanel3.Controls.Add(Me.fldEndsAt)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 204)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(877, 33)
        Me.FlowLayoutPanel3.TabIndex = 24
        '
        'lblTicketExpAt
        '
        Me.lblTicketExpAt.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblTicketExpAt.AutoSize = True
        Me.lblTicketExpAt.BackColor = System.Drawing.Color.Transparent
        Me.lblTicketExpAt.Font = New System.Drawing.Font("Modern H EcoLight", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblTicketExpAt.Location = New System.Drawing.Point(0, 0)
        Me.lblTicketExpAt.Margin = New System.Windows.Forms.Padding(0)
        Me.lblTicketExpAt.Name = "lblTicketExpAt"
        Me.lblTicketExpAt.Size = New System.Drawing.Size(181, 33)
        Me.lblTicketExpAt.TabIndex = 21
        Me.lblTicketExpAt.Text = "Абонемент до:"
        Me.lblTicketExpAt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTicketExpAt.UseCompatibleTextRendering = True
        '
        'fldTicketExpires
        '
        Me.fldTicketExpires.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.fldTicketExpires.AutoSize = True
        Me.fldTicketExpires.BackColor = System.Drawing.Color.Transparent
        Me.fldTicketExpires.Font = New System.Drawing.Font("Modern H Medium", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldTicketExpires.Location = New System.Drawing.Point(181, 0)
        Me.fldTicketExpires.Margin = New System.Windows.Forms.Padding(0)
        Me.fldTicketExpires.Name = "fldTicketExpires"
        Me.fldTicketExpires.Size = New System.Drawing.Size(98, 33)
        Me.fldTicketExpires.TabIndex = 22
        Me.fldTicketExpires.Text = "00.00.00"
        Me.fldTicketExpires.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldTicketExpires.UseCompatibleTextRendering = True
        '
        'fldEndsAt
        '
        Me.fldEndsAt.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.fldEndsAt.AutoSize = True
        Me.fldEndsAt.BackColor = System.Drawing.Color.Transparent
        Me.fldEndsAt.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldEndsAt.Location = New System.Drawing.Point(299, 1)
        Me.fldEndsAt.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.fldEndsAt.Name = "fldEndsAt"
        Me.fldEndsAt.Size = New System.Drawing.Size(166, 31)
        Me.fldEndsAt.TabIndex = 22
        Me.fldEndsAt.Text = "через 2 недели"
        Me.fldEndsAt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldEndsAt.UseCompatibleTextRendering = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblBirthday)
        Me.FlowLayoutPanel1.Controls.Add(Me.fldBirthday)
        Me.FlowLayoutPanel1.Controls.Add(Me.fldYearsOld)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 110)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(877, 27)
        Me.FlowLayoutPanel1.TabIndex = 20
        '
        'fldYearsOld
        '
        Me.fldYearsOld.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.fldYearsOld.AutoSize = True
        Me.fldYearsOld.BackColor = System.Drawing.Color.Transparent
        Me.fldYearsOld.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldYearsOld.Location = New System.Drawing.Point(245, 0)
        Me.fldYearsOld.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.fldYearsOld.Name = "fldYearsOld"
        Me.fldYearsOld.Size = New System.Drawing.Size(72, 27)
        Me.fldYearsOld.TabIndex = 22
        Me.fldYearsOld.Text = "99 лет"
        Me.fldYearsOld.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldYearsOld.UseCompatibleTextRendering = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lblRegistred)
        Me.FlowLayoutPanel2.Controls.Add(Me.fldRegistred)
        Me.FlowLayoutPanel2.Controls.Add(Me.fldRegistredOld)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 143)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(877, 39)
        Me.FlowLayoutPanel2.TabIndex = 21
        '
        'fldRegistred
        '
        Me.fldRegistred.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.fldRegistred.AutoSize = True
        Me.fldRegistred.BackColor = System.Drawing.Color.Transparent
        Me.fldRegistred.Font = New System.Drawing.Font("Modern H Medium", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldRegistred.Location = New System.Drawing.Point(183, 0)
        Me.fldRegistred.Margin = New System.Windows.Forms.Padding(0)
        Me.fldRegistred.Name = "fldRegistred"
        Me.fldRegistred.Size = New System.Drawing.Size(98, 35)
        Me.fldRegistred.TabIndex = 22
        Me.fldRegistred.Text = "00.00.00"
        Me.fldRegistred.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldRegistred.UseCompatibleTextRendering = True
        '
        'fldRegistredOld
        '
        Me.fldRegistredOld.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.fldRegistredOld.AutoSize = True
        Me.fldRegistredOld.BackColor = System.Drawing.Color.Transparent
        Me.fldRegistredOld.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldRegistredOld.Location = New System.Drawing.Point(296, 2)
        Me.fldRegistredOld.Margin = New System.Windows.Forms.Padding(15, 0, 0, 0)
        Me.fldRegistredOld.Name = "fldRegistredOld"
        Me.fldRegistredOld.Size = New System.Drawing.Size(154, 31)
        Me.fldRegistredOld.TabIndex = 22
        Me.fldRegistredOld.Text = "(99 лет назад)"
        Me.fldRegistredOld.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.fldRegistredOld.UseCompatibleTextRendering = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer))
        Me.btnRefresh.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh.ForeColor = System.Drawing.Color.White
        Me.btnRefresh.Location = New System.Drawing.Point(39, 589)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(290, 39)
        Me.btnRefresh.TabIndex = 26
        Me.btnRefresh.Text = "Обновить данные"
        Me.btnRefresh.UseVisualStyleBackColor = False
        '
        'brnCart
        '
        Me.brnCart.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.brnCart.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer))
        Me.brnCart.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.brnCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.brnCart.ForeColor = System.Drawing.Color.White
        Me.brnCart.Location = New System.Drawing.Point(39, 544)
        Me.brnCart.Name = "brnCart"
        Me.brnCart.Size = New System.Drawing.Size(290, 39)
        Me.brnCart.TabIndex = 0
        Me.brnCart.Text = "Создать чек"
        Me.brnCart.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Modern H EcoLight", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label2.Location = New System.Drawing.Point(366, 483)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(266, 23)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Комментарий к посетителю:"
        '
        'txtComment
        '
        Me.txtComment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComment.BackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer))
        Me.txtComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtComment.Font = New System.Drawing.Font("Modern H EcoLight", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtComment.ForeColor = System.Drawing.Color.Gainsboro
        Me.txtComment.Location = New System.Drawing.Point(370, 511)
        Me.txtComment.MaxLength = 250
        Me.txtComment.Multiline = True
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(883, 117)
        Me.txtComment.TabIndex = 26
        '
        'btnWriteVisit
        '
        Me.btnWriteVisit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnWriteVisit.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer), CType(CType(16, Byte), Integer))
        Me.btnWriteVisit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnWriteVisit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWriteVisit.Font = New System.Drawing.Font("Modern H Medium", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnWriteVisit.ForeColor = System.Drawing.Color.White
        Me.btnWriteVisit.Location = New System.Drawing.Point(39, 469)
        Me.btnWriteVisit.Name = "btnWriteVisit"
        Me.btnWriteVisit.Size = New System.Drawing.Size(290, 53)
        Me.btnWriteVisit.TabIndex = 26
        Me.btnWriteVisit.Text = "Записать посещение"
        Me.btnWriteVisit.UseVisualStyleBackColor = False
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(1271, 37)
        Me.lblCaption.TabIndex = 27
        Me.lblCaption.Text = "Просмотр содержимого карты #000000"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNotVisited
        '
        Me.lblNotVisited.Font = New System.Drawing.Font("Modern H Medium", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblNotVisited.ForeColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(202, Byte), Integer))
        Me.lblNotVisited.Location = New System.Drawing.Point(39, 378)
        Me.lblNotVisited.Name = "lblNotVisited"
        Me.lblNotVisited.Size = New System.Drawing.Size(290, 23)
        Me.lblNotVisited.TabIndex = 25
        Me.lblNotVisited.Text = "сегодня не посещал"
        Me.lblNotVisited.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mini
        '
        Me.mini.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.mini.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.mini.FlatAppearance.BorderSize = 0
        Me.mini.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.mini.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.mini.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.mini.ForeColor = System.Drawing.Color.Olive
        Me.mini.Location = New System.Drawing.Point(1144, -10)
        Me.mini.Margin = New System.Windows.Forms.Padding(12)
        Me.mini.Name = "mini"
        Me.mini.Size = New System.Drawing.Size(62, 47)
        Me.mini.TabIndex = 7
        Me.mini.Text = "_"
        Me.mini.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.mini.UseVisualStyleBackColor = False
        '
        'lblResize
        '
        Me.lblResize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblResize.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.lblResize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblResize.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.lblResize.Location = New System.Drawing.Point(1238, 631)
        Me.lblResize.Name = "lblResize"
        Me.lblResize.Size = New System.Drawing.Size(35, 11)
        Me.lblResize.TabIndex = 28
        '
        'user_load
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 23.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1271, 640)
        Me.Controls.Add(Me.lblResize)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.btnWriteVisit)
        Me.Controls.Add(Me.brnCart)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.fldID)
        Me.Controls.Add(Me.mini)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.lblNotVisited)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.lblCaption)
        Me.Font = New System.Drawing.Font("Modern H Medium", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.MinimumSize = New System.Drawing.Size(1050, 640)
        Me.Name = "user_load"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents fldID As System.Windows.Forms.Label
    Friend WithEvents lblBirthday As System.Windows.Forms.Label
    Friend WithEvents fldFirstLastName As System.Windows.Forms.Label
    Friend WithEvents fldSurname As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents fldBirthday As System.Windows.Forms.Label
    Friend WithEvents lblRegistred As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents fldRegistred As System.Windows.Forms.Label
    Friend WithEvents fldYearsOld As System.Windows.Forms.Label
    Friend WithEvents fldRegistredOld As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblTicketExpAt As System.Windows.Forms.Label
    Friend WithEvents fldTicketExpires As System.Windows.Forms.Label
    Friend WithEvents fldEndsAt As System.Windows.Forms.Label
    Friend WithEvents flowVariables As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents brnCart As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtComment As System.Windows.Forms.TextBox
    Friend WithEvents btnWriteVisit As System.Windows.Forms.Button
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents lblNotVisited As System.Windows.Forms.Label
    Friend WithEvents mini As System.Windows.Forms.Button
    Friend WithEvents lblResize As System.Windows.Forms.Label
End Class
