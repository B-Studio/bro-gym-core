﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class custom_queries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(custom_queries))
        Me.clos = New System.Windows.Forms.Button()
        Me.txtQuery = New System.Windows.Forms.TextBox()
        Me.exec = New System.Windows.Forms.Button()
        Me.lstQueryResult = New System.Windows.Forms.ListView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.fldAffected = New System.Windows.Forms.Label()
        Me.mini = New System.Windows.Forms.Button()
        Me.lblResize = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(28, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(1082, -1)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(47, 45)
        Me.clos.TabIndex = 5
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'txtQuery
        '
        Me.txtQuery.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtQuery.BackColor = System.Drawing.Color.Black
        Me.txtQuery.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtQuery.Font = New System.Drawing.Font("Consolas", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtQuery.ForeColor = System.Drawing.Color.White
        Me.txtQuery.Location = New System.Drawing.Point(12, 68)
        Me.txtQuery.MaxLength = 2147483647
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.Size = New System.Drawing.Size(1039, 170)
        Me.txtQuery.TabIndex = 0
        '
        'exec
        '
        Me.exec.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.exec.FlatAppearance.BorderSize = 0
        Me.exec.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.exec.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.exec.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.exec.ForeColor = System.Drawing.Color.Green
        Me.exec.Location = New System.Drawing.Point(1066, 166)
        Me.exec.Margin = New System.Windows.Forms.Padding(12)
        Me.exec.Name = "exec"
        Me.exec.Size = New System.Drawing.Size(62, 58)
        Me.exec.TabIndex = 7
        Me.exec.Text = "->"
        Me.exec.UseVisualStyleBackColor = False
        '
        'lstQueryResult
        '
        Me.lstQueryResult.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstQueryResult.BackColor = System.Drawing.Color.Black
        Me.lstQueryResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstQueryResult.Font = New System.Drawing.Font("Modern H Medium", 12.0!)
        Me.lstQueryResult.ForeColor = System.Drawing.Color.White
        Me.lstQueryResult.FullRowSelect = True
        Me.lstQueryResult.Location = New System.Drawing.Point(12, 299)
        Me.lstQueryResult.Name = "lstQueryResult"
        Me.lstQueryResult.Size = New System.Drawing.Size(1105, 375)
        Me.lstQueryResult.TabIndex = 8
        Me.lstQueryResult.UseCompatibleStateImageBehavior = False
        Me.lstQueryResult.View = System.Windows.Forms.View.Details
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.Label1.Location = New System.Drawing.Point(12, 261)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 0, 3, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(215, 26)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Результат запроса:"
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(28, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(1129, 44)
        Me.lblCaption.TabIndex = 16
        Me.lblCaption.Text = "Произвольные запросы к базе данных"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fldAffected
        '
        Me.fldAffected.AutoSize = True
        Me.fldAffected.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.fldAffected.ForeColor = System.Drawing.Color.DimGray
        Me.fldAffected.Location = New System.Drawing.Point(285, 262)
        Me.fldAffected.Margin = New System.Windows.Forms.Padding(3, 0, 3, 9)
        Me.fldAffected.Name = "fldAffected"
        Me.fldAffected.Size = New System.Drawing.Size(201, 24)
        Me.fldAffected.TabIndex = 9
        Me.fldAffected.Text = "изменено строк - 0"
        Me.fldAffected.Visible = False
        '
        'mini
        '
        Me.mini.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.mini.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(28, Byte), Integer))
        Me.mini.FlatAppearance.BorderSize = 0
        Me.mini.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.mini.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.mini.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.mini.ForeColor = System.Drawing.Color.Olive
        Me.mini.Location = New System.Drawing.Point(1030, -3)
        Me.mini.Margin = New System.Windows.Forms.Padding(12)
        Me.mini.Name = "mini"
        Me.mini.Size = New System.Drawing.Size(49, 47)
        Me.mini.TabIndex = 17
        Me.mini.Text = "_"
        Me.mini.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.mini.UseVisualStyleBackColor = False
        '
        'lblResize
        '
        Me.lblResize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblResize.BackColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.lblResize.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.lblResize.Location = New System.Drawing.Point(1095, 674)
        Me.lblResize.Name = "lblResize"
        Me.lblResize.Size = New System.Drawing.Size(35, 16)
        Me.lblResize.TabIndex = 29
        '
        'custom_queries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(18.0!, 38.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1129, 686)
        Me.Controls.Add(Me.lblResize)
        Me.Controls.Add(Me.mini)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.fldAffected)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstQueryResult)
        Me.Controls.Add(Me.exec)
        Me.Controls.Add(Me.txtQuery)
        Me.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.ForeColor = System.Drawing.Color.LightGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(9)
        Me.Name = "custom_queries"
        Me.Text = "custom_queries"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents exec As System.Windows.Forms.Button
    Friend WithEvents lstQueryResult As System.Windows.Forms.ListView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents fldAffected As System.Windows.Forms.Label
    Friend WithEvents mini As System.Windows.Forms.Button
    Friend WithEvents lblResize As System.Windows.Forms.Label
End Class
