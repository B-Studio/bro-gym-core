﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class expires_change
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(expires_change))
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.clos = New System.Windows.Forms.Button()
        Me.btnCommit = New System.Windows.Forms.Button()
        Me.mtxNewExpires = New System.Windows.Forms.MaskedTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblExpiresOld = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Modern H Medium", 16.0!)
        Me.lblCaption.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
        Me.lblCaption.Size = New System.Drawing.Size(593, 37)
        Me.lblCaption.TabIndex = 28
        Me.lblCaption.Text = "Изменение даты окончания абонемента"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'clos
        '
        Me.clos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clos.BackColor = System.Drawing.Color.FromArgb(CType(CType(41, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.clos.FlatAppearance.BorderSize = 0
        Me.clos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.clos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.clos.Font = New System.Drawing.Font("Modern H Medium", 24.0!)
        Me.clos.ForeColor = System.Drawing.Color.Maroon
        Me.clos.Location = New System.Drawing.Point(531, -9)
        Me.clos.Margin = New System.Windows.Forms.Padding(12)
        Me.clos.Name = "clos"
        Me.clos.Size = New System.Drawing.Size(62, 46)
        Me.clos.TabIndex = 29
        Me.clos.Text = "x"
        Me.clos.UseVisualStyleBackColor = False
        '
        'btnCommit
        '
        Me.btnCommit.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnCommit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCommit.Font = New System.Drawing.Font("Modern H Medium", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnCommit.Location = New System.Drawing.Point(167, 353)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(264, 73)
        Me.btnCommit.TabIndex = 30
        Me.btnCommit.Text = "Сохранить"
        Me.btnCommit.UseVisualStyleBackColor = True
        '
        'mtxNewExpires
        '
        Me.mtxNewExpires.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.mtxNewExpires.AsciiOnly = True
        Me.mtxNewExpires.BackColor = System.Drawing.Color.Black
        Me.mtxNewExpires.BeepOnError = True
        Me.mtxNewExpires.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtxNewExpires.Font = New System.Drawing.Font("Modern H EcoLight", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.mtxNewExpires.ForeColor = System.Drawing.Color.Gainsboro
        Me.mtxNewExpires.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Insert
        Me.mtxNewExpires.Location = New System.Drawing.Point(146, 236)
        Me.mtxNewExpires.Mask = "00\.00\.00##"
        Me.mtxNewExpires.Name = "mtxNewExpires"
        Me.mtxNewExpires.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.mtxNewExpires.Size = New System.Drawing.Size(300, 45)
        Me.mtxNewExpires.TabIndex = 31
        Me.mtxNewExpires.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.Font = New System.Drawing.Font("Modern H EcoLight", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label5.Location = New System.Drawing.Point(77, 195)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(438, 28)
        Me.Label5.TabIndex = 32
        Me.Label5.Text = "Новая дата окончания абонемента:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.Location = New System.Drawing.Point(65, 91)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(462, 28)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Текущая дата окончания абонемента:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblExpiresOld
        '
        Me.lblExpiresOld.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblExpiresOld.Font = New System.Drawing.Font("Modern H Medium", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblExpiresOld.Location = New System.Drawing.Point(146, 118)
        Me.lblExpiresOld.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.lblExpiresOld.Name = "lblExpiresOld"
        Me.lblExpiresOld.Size = New System.Drawing.Size(300, 43)
        Me.lblExpiresOld.TabIndex = 33
        Me.lblExpiresOld.Text = "16 января"
        Me.lblExpiresOld.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'expires_change
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 24.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(16, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(593, 490)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.mtxNewExpires)
        Me.Controls.Add(Me.btnCommit)
        Me.Controls.Add(Me.clos)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.lblExpiresOld)
        Me.Font = New System.Drawing.Font("Modern H EcoLight", 15.75!)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "expires_change"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "add_anon_visit"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents clos As System.Windows.Forms.Button
    Friend WithEvents btnCommit As System.Windows.Forms.Button
    Friend WithEvents mtxNewExpires As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblExpiresOld As System.Windows.Forms.Label
End Class
