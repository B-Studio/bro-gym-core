﻿Imports System.IO
Public Class statistics
    Private Dialog As DialogService = Service(Of DialogService)()

    Public Sub RefreshState(IfTableNameInQuery$)
        If scripts(cmbQueryType.SelectedIndex).Contains(IfTableNameInQuery) Then
            exec_Click(Nothing, Nothing)
        End If
    End Sub

    Dim scriptsDir$ = Application.StartupPath & "\profits"
    Dim scripts As New List(Of String)
    Private Sub FormLoaded(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            scripts.Clear()
            cmbQueryType.Items.Clear()
            If Not Directory.Exists(scriptsDir) Then Directory.CreateDirectory(scriptsDir)
            For Each s As String In Directory.GetFiles(scriptsDir)
                Using SR As New StreamReader(s, System.Text.Encoding.UTF8)
                    Dim scriptName$ = String.Empty
                    s = String.Empty
                    If Not SR.EndOfStream Then _
                        scriptName = SR.ReadLine().Trim()
                    Do While Not SR.EndOfStream
                        s &= SR.ReadLine() & Chr(10) & Chr(13)
                    Loop
                    If s.Trim().Length > 0 Then
                        cmbQueryType.Items.Add(scriptName)
                        scripts.Add(s)
                    End If
                End Using
                If My.Settings.tprofit_size.Height > 0 AndAlso
                   My.Settings.tprofit_size.Width > 0 Then _
                   Me.Size = My.Settings.tprofit_size
                If My.Settings.tprofit_location.X >= 0 AndAlso
                   My.Settings.tprofit_location.Y >= 0 Then _
                   Me.Location = My.Settings.tprofit_location
            Next
        Catch Ex As Exception
            Dialog.Show("Возникла ошибка при попытке прочитать шаблоны из папки. Стек: " & Ex.Message,
                         "Покажи это Сане", MessageBoxIcon.Error, MessageBoxButtons.OK)
        End Try
        If cmbQueryType.Items.Count > 0 Then
            If My.Settings.tprofit_last_sel > -1 AndAlso
               My.Settings.tprofit_last_sel < cmbQueryType.Items.Count Then
                cmbQueryType.SelectedIndex = My.Settings.tprofit_last_sel
            Else : cmbQueryType.SelectedIndex = 0
            End If
            DragByAControl.AttachResize(lblResize)
            DragByAControl.AttachMove(lblCaption)
        Else
            Dialog.Show("Не найдено ни одного корректного шаблона в папке!", "Ошибочка", MessageBoxIcon.Error, MessageBoxButtons.OK)
            Me.Close()
        End If
    End Sub

    Private Sub clos_Click(sender As Object, e As EventArgs) Handles clos.Click
        Me.Opacity = 0.01
        Me.Refresh()
        Me.Hide()
        Me.Close()
    End Sub
    Private Sub mini_Click(sender As Object, e As EventArgs) Handles mini.Click
        btnCommit.Focus()
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Private Sub Button5_MouseEnter(sender As Object, e As EventArgs) Handles clos.MouseEnter, mini.MouseEnter
        If sender Is Me.clos Then _
        sender.ForeColor = Color.Red _
        Else sender.ForeColor = Color.Yellow
    End Sub
    Private Sub Button5_MouseLeave(sender As Object, e As EventArgs) Handles clos.MouseLeave, mini.MouseLeave
        If sender Is Me.clos Then _
        sender.ForeColor = Color.Maroon _
        Else sender.ForeColor = Color.Olive
    End Sub

    Private Sub MEClose(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        My.Settings.tprofit_last_sel = cmbQueryType.SelectedIndex
        My.Settings.tprofit_location = Me.Location
        My.Settings.tprofit_size = Me.Size
        My.Settings.Save()
        Me.Opacity = 0.01
        Me.Refresh()
        e.Cancel = False
    End Sub

    Private Sub exec_Click(sender As Object, e As EventArgs) Handles btnCommit.Click
        If sender IsNot Nothing Then : FormLoaded(Nothing, Nothing)
        Else
            Dim resp As SQL.Response = SQL.Query(scripts(cmbQueryType.SelectedIndex))
            lstQueryResult.ListViewItemSorter = Nothing
            lstQueryResult.Clear()
            Select Case resp.KindIs
                Case SQL.Response.ResponseKind.Table
                    Dim g As Graphics = Graphics.FromImage(New Bitmap(Me.Width, Me.Height))
                    Dim col_widths(resp.Table.Width) As UInt16
                    Dim refWid = Sub(ByRef wid, nwid) If nwid > wid Then wid = nwid
                    lstQueryResult.Columns.Add("#")
                    For i% = 0 To resp.Table.Width - 1
                        Dim gname$ = SQL.RusColName(resp.Table.Columns(i))
                        lstQueryResult.Columns.Add(gname)
                        refWid(col_widths(i), g.MeasureString(gname, lstQueryResult.Font).Width + 10)
                    Next
                    Dim rowCounter% = 1
                    For Each row As Row In resp.Table
                        Dim strs(resp.Table.Width + 1) As String
                        strs(0) = rowCounter : rowCounter += 1
                        For i% = 1 To resp.Table.Width
                            If Not IsDBNull(row(CByte(i - 1))) AndAlso row(CByte(i - 1)) IsNot Nothing Then _
                                strs(i) = row(CByte(i - 1)).ToString() Else strs(i) = "(null)"
                            refWid(col_widths(i - 1), g.MeasureString(strs(i), lstQueryResult.Font).Width + 10)
                        Next
                        lstQueryResult.Items.Add(New ListViewItem(strs))
                    Next
                    lstQueryResult.Columns(0).Width = 32
                    For i% = 1 To resp.Table.Width
                        lstQueryResult.Columns(i).Width = col_widths(i - 1)
                    Next
                Case SQL.Response.ResponseKind.Excep
                    Dialog.Show(resp.Excep.GetType().ToString() & ": " & resp.Excep.Message,
                                    "Ошибка в результате запроса", MessageBoxIcon.Error, MessageBoxButtons.OK)
                Case SQL.Response.ResponseKind.Empty
                    lstQueryResult.Columns.Add("Результат по запросу «" & cmbQueryType.Text & "»")
                    lstQueryResult.Columns(0).Width = lstQueryResult.Width - 10
                    lstQueryResult.Items.Add(New ListViewItem("Ни одной строки не получено!"))
            End Select
            lstQueryResult.Refresh()
            btnCommit.Focus()
        End If
        My.Settings.tprofit_last_sel = cmbQueryType.SelectedIndex
    End Sub

    Private Sub cmbQueryType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbQueryType.SelectedIndexChanged
        exec_Click(Nothing, Nothing)
    End Sub

End Class